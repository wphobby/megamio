<form role="search" method="get" action="<?php echo esc_url( home_url( '/' ) ); ?>">
<div class="error-form">
        <input name="s" placeholder="<?php _e( 'Search again... ', 'megamio' ); ?>" type="text">
        <button><i class="fa fa-search"></i></button>
    </div>
</form>