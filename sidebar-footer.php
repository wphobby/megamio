<?php
/**
 * The sidebar containing the footer widget area
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package megamio
 */
?>

<div class="col-lg-3 col-sm-6 col-6">
    <?php include(locate_template('template-parts/menu/menu-footer-one.php')); ?>
</div>
<div class="col-lg-3 col-sm-6 col-6">
    <?php include(locate_template('template-parts/menu/menu-footer-two.php')); ?>

</div>
<div class="col-lg-3 col-sm-6 col-6">
    <?php include(locate_template('template-parts/menu/menu-footer-three.php')); ?>

</div>
<div class="col-lg-3 col-sm-6">
    <?php if ( is_active_sidebar( 'footer-sidebar') ) : ?>
        <?php dynamic_sidebar( 'footer-sidebar' ); ?>
    <?php endif; ?>
</div>