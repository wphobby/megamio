<?php
/**
 * The template to display 404 pages (not found).
 *
 *
 * @package    megamio
 * @author     wphobby
 * @link       https://wphobby.com
 */
get_header();
?>

<section class="error-page">
    <div class="row">
            <div class="col-lg-12">
                <div class="error-item text-center pt-150 pb-200">
                    <img src="<?php echo get_template_directory_uri() . '/assets/images/Error.png'; ?>" alt="">
                    <h5><?php _e( "Error - Page not found.", 'megamio' ); ?></h5>
                    <p><?php _e( 'It seems we can’t find what you’re looking for. Perhaps searching can help or go back to ', 'megamio' ); ?><a href="<?php echo esc_url( home_url( '/' ) ); ?>"> <?php _e( "Homepage.", 'megamio' ); ?></a></p>
                    <?php get_search_form(); ?>

                </div>
            </div>
        </div>
</section>

<?php get_footer(); ?>
