<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package megamio
 */
global $megamio_options;

if ( (isset($megamio_options['footer_style'])) ) {
    if ('site-footer-default' === $megamio_options['footer_style']) {
        $footer_load = 'default';
    }else if('site-footer-center' === $megamio_options['footer_style']){
        $footer_load = 'center';
    }
}

if(isset($_GET['footer_style']) && $_GET['footer_style'] == 'none'){

}else{
    get_template_part( 'template-parts/footer/footer', $footer_load );
}


?>
<div class="wc-notification-wrapper">
</div>

<div class="modal fade woocommerce" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
    </div> <!-- modal dialog -->
</div> <!-- modal fade -->
<?php

do_action( 'megamio_after_footer' );
wp_footer();
?>
</div><!-- .site-content-container -->
</div><!-- #content -->
</body>
</html>
