<?php
/**
 * The template to display Search Results pages.
 *
 * @package    megamio
 * @author     wphobby
 * @link       https://wphobby.com
 */
get_header();
?>
    <section class="latest-posts pb-60">
        <div class="row">
                <div class="col-lg-8">
                        <h1>
                            <?php
                            /* translators: %s: search query. */
                            printf( esc_html__( 'Search Results for: %s', 'megamio' ), '<span>' . get_search_query() . '</span>' );
                            ?>
                        </h1>
                    <?php
                    // loop content with blog wrapper
                    if ( have_posts() ) :
                        while ( have_posts() ) : the_post();
                            get_template_part( 'template-parts/content', 'search' );
                        endwhile; /* End Loop */
                    else:
                        get_template_part( 'template-parts/content', 'none' );
                    endif;
                    ?>
                    <div class="readmore-btn pt-120 text-center">
                        <a href="#" class="main-btn">load more</a>
                    </div>
                </div>
                <?php get_sidebar(); ?>
            </div> <!-- row -->
    </section>
<?php
get_footer();