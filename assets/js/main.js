jQuery(function($) {

    "use strict";
    
    //===== Prealoder
    
    $(window).on('load', function(event) {
        $('.preloader').delay(500).fadeOut(500);
    });
    
    
    //===== Search 
    
    $('.search-open').on('click', function(){
        $('.search-box').addClass('open')
    });
    
    $('.search-close-btn').on('click', function(){
        $('.search-box').removeClass('open')
    });
    
    //===== Shopping Cart 
    
    $('.shopping-cart-open').on('click', function(){
        $('.shopping-cart-canvas').addClass('open')
        $('.overlay').addClass('open')
    });
    
    $('.shopping-cart-close').on('click', function(){
        $('.shopping-cart-canvas').removeClass('open')
        $('.overlay').removeClass('open')
    });
    $('.overlay').on('click', function(){
        $('.shopping-cart-canvas').removeClass('open')
        $('.overlay').removeClass('open')
    });
    
    
    //===== Side Canvas
    
    $('.canvas-menu-open').on('click', function(){
        $(".canvas-area").addClass('open');
        $(".overlay").addClass('open');
    });
    
    $('.canvas-menu-close').on('click', function(){
        $(".canvas-area").removeClass("open");
        $(".overlay").removeClass("open");
    });
    
    $('.overlay').on('click', function(){
        $(".canvas-area").removeClass("open");
        $(".overlay").removeClass("open");
    });
    
    
    var lastClicked;
    $(".dw-menu").on('click', function(e) {
        
      $(this).siblings().toggle('slow');
      $(this).removeClass('b');

        if (lastClicked != $(this)) {
            if (lastClicked != undefined) {
                if(lastClicked.hasClass('b')){
                    lastClicked.siblings().hide(1000);
                }
            }
        }

        lastClicked = $(this);
        lastClicked.addClass('b')
    });

    
    //===== LogIn Register
    
     $('.login-form-open').on('click', function(){
        $(".user-login-area").addClass('open');
        $(".overlay").addClass('open');
    });
    
    $('.login-form-close').on('click', function(){
        $(".user-login-area").removeClass("open");
        $(".overlay").removeClass("open");
    });
    
    $('.overlay').on('click', function(){
        $(".user-login-area").removeClass("open");
        $(".overlay").removeClass("open");
    });
    
    
    $('#register-btn').on('click', function(){
        $(".register").fadeIn();
        $(".login").fadeOut();
    });
    
    $('#already-account').on('click', function(){
        $(".login").fadeIn();
         $(".register").fadeOut();
    });
    
    
    //===== Sticky
    
    $(window).on('scroll',function(event) {    
        var scroll = $(window).scrollTop();
        if (scroll < 245) {
            $(".header-area").removeClass("sticky");
            $(".header-menu-area").removeClass("sticky");
        }else{
            $(".header-area").addClass("sticky");
            $(".header-menu-area").addClass("sticky");
        }
    });
    
    
    //===== Mobile Menu
    
    $('.mobile-menu-open').on('click', function(){
        $('.offcanvas-menu').addClass('open')
        $('.overlay').addClass('open')
    });
    
    $('.close-mobile-menu').on('click', function(){
        $('.offcanvas-menu').removeClass('open')
        $('.overlay').removeClass('open')
    });
    
    $('.overlay').on('click', function(){
        $('.offcanvas-menu').removeClass('open')
        $('.overlay').removeClass('open')
    });
    
    /*Variables*/
    var $offCanvasNav = $('.mobile-menu'),
        $offCanvasNavSubMenu = $offCanvasNav.find('.sub-menu');

    /*Add Toggle Button With Off Canvas Sub Menu*/
    $offCanvasNavSubMenu.parent().prepend('<span class="menu-expand"></span>');

    /*Close Off Canvas Sub Menu*/
    $offCanvasNavSubMenu.slideUp();

    /*Category Sub Menu Toggle*/
    $offCanvasNav.on('click', 'li a, li .menu-expand', function(e) {
        var $this = $(this);
        if (($this.parent().attr('class').match(/\b(menu-item-has-children|has-children|has-sub-menu)\b/)) && ($this.attr('href') === '#' || $this.hasClass('menu-expand'))) {
            e.preventDefault();
            if ($this.siblings('ul:visible').length) {
                $this.parent('li').removeClass('active');
                $this.siblings('ul').slideUp();
            } else {
                $this.parent('li').addClass('active');
                $this.closest('li').siblings('li').find('ul:visible').slideUp();
                $this.closest('li').siblings('li').removeClass('active');
                $this.siblings('ul').slideDown();
            }
        }
    });
    
    
    //===== Account Popup
    
    $(".newsletter-popup-close").on("click", function(){
        $(".newsletter-popup-area").fadeOut(500);
    });
    

    //===== Slick Slider

    /*
    function mainSlider() {
        var BasicSlider = $('.slider-active');
        var BasicSlider2 = $('.slider-active-2');
        var BasicSlider3 = $('.slider-active-3');
        var BasicSlider4 = $('.slider-active-4');
        
        BasicSlider.on('init', function (e, slick) {
            var $firstAnimatingElements = $('.single-slider:first-child').find('[data-animation]');
            doAnimations($firstAnimatingElements);
        });
        
        BasicSlider2.on('init', function (e, slick) {
            var $firstAnimatingElements = $('.single-slider:first-child').find('[data-animation]');
            doAnimations($firstAnimatingElements);
        });
        
        BasicSlider3.on('init', function (e, slick) {
            var $firstAnimatingElements = $('.single-slider:first-child').find('[data-animation]');
            doAnimations($firstAnimatingElements);
        });
        
        BasicSlider4.on('init', function (e, slick) {
            var $firstAnimatingElements = $('.single-slider:first-child').find('[data-animation]');
            doAnimations($firstAnimatingElements);
        });

        
        BasicSlider.on('beforeChange', function (e, slick, currentSlide, nextSlide) {
            var $animatingElements = $('.single-slider[data-slick-index="' + nextSlide + '"]').find('[data-animation]');
            doAnimations($animatingElements);
        });
        
        BasicSlider2.on('beforeChange', function (e, slick, currentSlide, nextSlide) {
            var $animatingElements = $('.single-slider[data-slick-index="' + nextSlide + '"]').find('[data-animation]');
            doAnimations($animatingElements);
        });
        
        BasicSlider3.on('beforeChange', function (e, slick, currentSlide, nextSlide) {
            var $animatingElements = $('.single-slider[data-slick-index="' + nextSlide + '"]').find('[data-animation]');
            doAnimations($animatingElements);
        });
        
        BasicSlider4.on('beforeChange', function (e, slick, currentSlide, nextSlide) {
            var $animatingElements = $('.single-slider[data-slick-index="' + nextSlide + '"]').find('[data-animation]');
            doAnimations($animatingElements);
        });

        
        BasicSlider.slick({
            autoplay: false,
            autoplaySpeed: 10000,
            dots: true,
            fade: true,
            arrows: false,
            responsive: [
                {
                    breakpoint: 767,
                    settings: {
                        dots: false,
                        arrows: false
                    }
                }
            ]
        });
        
        BasicSlider2.slick({
            autoplay: false,
            autoplaySpeed: 10000,
            dots: true,
            fade: true,
            arrows: false,
            responsive: [
                {
                    breakpoint: 767,
                    settings: {
                        dots: false,
                        arrows: false
                    }
                }
            ]
        });
        
        BasicSlider3.slick({
            autoplay: false,
            autoplaySpeed: 10000,
            dots: false,
            fade: true,
            arrows: true,
            prevArrow:'<span class="prev"><i class="fa fa-angle-left"></i></span>',
            nextArrow: '<span class="next"><i class="fa fa-angle-right"></i></span>',
            responsive: [
                {
                    breakpoint: 767,
                    settings: {
                        dots: false,
                        arrows: false
                    }
                }
            ]
        });
        
        BasicSlider4.slick({
            autoplay: false,
            autoplaySpeed: 10000,
            dots: true,
            fade: true,
            arrows: true,
            prevArrow:'<span class="prev"><i class="fa fa-angle-left"></i></span>',
            nextArrow: '<span class="next"><i class="fa fa-angle-right"></i></span>',
            responsive: [
                {
                    breakpoint: 767,
                    settings: {
                        dots: false,
                        arrows: false
                    }
                }
            ]
        });

        function doAnimations(elements) {
            var animationEndEvents = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
            elements.each(function () {
                var $this = $(this);
                var $animationDelay = $this.data('delay');
                var $animationType = 'animated ' + $this.data('animation');
                $this.css({
                    'animation-delay': $animationDelay,
                    '-webkit-animation-delay': $animationDelay
                });
                $this.addClass($animationType).one(animationEndEvents, function () {
                    $this.removeClass($animationType);
                });
            });
        }
    }
    mainSlider();
    */


    //===== Isotope Project 1

    $('.container').imagesLoaded(function () {
    
        var $grid = $('.grid').isotope({
            // options
            transitionDuration: '1s'
        });

        // filter items on button click
        $('.arrivals-menu ul').on('click', 'li', function () {
            var filterValue = $(this).attr('data-filter');
            $grid.isotope({
                filter: filterValue
            });
        });

        //for menu active class
        $('.arrivals-menu ul li').on('click', function (event) {
            $(this).siblings('.active').removeClass('active');
            $(this).addClass('active');
            event.preventDefault();
        });
    });

    
     //===== slick instagram active
    /*
    $('.instagram-active').slick({
        dots: false,
        infinite: true,
        arrows: false,
        speed: 600,
        slidesToShow: 6,
        slidesToScroll: 1,
        responsive: [
            {
                breakpoint: 1200,
                settings: {
                    slidesToShow: 6,
                }
            },
            {
                breakpoint: 992,
                settings: {
                    slidesToShow: 5,
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 4,
                }
            },
            {
                breakpoint: 576,
                settings: {
                    slidesToShow: 2,
                }
            },
        ]
    });
    */
    
    
    //===== Back to top
    
    // Show or hide the sticky footer button
    $(window).on('scroll', function(event) {
        if($(this).scrollTop() > 600){
            $('.back-to-top').fadeIn(200)
        } else{
            $('.back-to-top').fadeOut(200)
        }
    });
    
    //Animate the scroll to yop
    $('.back-to-top').on('click', function(event) {
        event.preventDefault();
        
        $('html, body').animate({
            scrollTop: 0,
        }, 1500);
    });
    
    
    //===== Card heard active
    
    $('.card-cont a i').on('click', function(){
        $(this).toggleClass('flaticon-favorite-heart-button');
    });
    
    
    //===== jTippy
    
    $('[data-tool="tooltip"]').jTippy({
        position: 'left',
    });
    
    
    //===== Countdown
    
    $('[data-countdown]').each(function() {
        var $this = $(this), finalDate = $(this).data('countdown');
        $this.countdown(finalDate, function(event) {
            $this.html(event.strftime('<div class="countdown d-flex justify-content-between"><div class="single-countdown"><span>%D</span><p>Days</p></div><div class="single-countdown"><span>%H</span><p>Hours</p></div><div class="single-countdown"><span>%M</span><p>Minutes</p></div><div class="single-countdown"><span>%S</span><p>Seconds</p></div></div>'));
        });
    });
    
    
    //===== Slick

    $('.trending-active').slick({
        dots: false,
        infinite: true,
        speed: 800,
        slidesToShow: 4,
        slidesToScroll: 1,
        arrows: true,
        prevArrow:'<span class="prev"><i class="fa fa-angle-left"></i></span>',
        nextArrow: '<span class="next"><i class="fa fa-angle-right"></i></span>',
        responsive: [
            {
              breakpoint: 1200,
              settings: {
                slidesToShow: 3,
              }
            },
            {
              breakpoint: 992,
              settings: {
                slidesToShow: 2,
              }
            },
            {
              breakpoint: 768,
              settings: {
                slidesToShow: 2,
                arrows: false,
              }
            },
            {
              breakpoint: 576,
              settings: {
                slidesToShow: 1,
                arrows: false,
              }
            }
        ]
    });


    
    //===== Slick product dec slider
    /*
    $('.product-dec-slider').slick({
        dots: false,
        infinite: true,
        speed: 600,
        slidesToShow: 4,
        slidesToScroll: 1,
        arrows: true,
        prevArrow:'<span class="prev"><i class="fa fa-angle-left"></i></span>',
        nextArrow:'<span class="next"><i class="fa fa-angle-right"></i></span>',
        responsive: [
            {
              breakpoint: 1200,
              settings: {
                slidesToShow: 3,
              }
            },
            {
              breakpoint: 992,
              settings: {
                slidesToShow: 3,
              }
            },
            {
              breakpoint: 768,
              settings: {
                slidesToShow: 4,
              }
            },
            {
              breakpoint: 576,
              settings: {
                slidesToShow: 3,
              }
            }
        ]
    });
    */
    
    //===== elevate Zoom Product
    
    $(".zoompro").elevateZoom({
        gallery: "gallery",
        galleryActiveClass: "active",
        zoomWindowWidth: 300,
        zoomWindowHeight: 100,
        scrollZoom: false,
        zoomType: "inner",
        cursor: "crosshair"
    });
    
    
    //===== Nice Number
    
    /*
    $('input[type="number"]').niceNumber({
        autoSize: false,
    });
    */
    
    //===== Nice Select
    
    //$('select').niceSelect();
    
    
    //===== checkout one click toggle function
    
    var checked = $('.sin-payment input:checked')
    if (checked) {
        $(checked).siblings('.payment-box').slideDown(900);
    };
    $('.sin-payment input').on('change', function() {
        $('.payment-box').slideUp(900);
        $(this).siblings('.payment-box').slideToggle(900);
    });
    
    
    //===== slider Range

    $("#slider-range").slider({
        range: true,
        min: 0,
        max: 250,
        values: [ 16, 200 ],
        slide: function( event, ui ) {
            $("#amount").val( "$" + ui.values[ 0 ] + " - $" + ui.values[ 1 ] );
        }
    });
    
    $("#amount").val("$" + $( "#slider-range").slider("values", 0) +
    " - $" + $("#slider-range").slider("values", 1 ));

    /**
     * Add class to .form-row when inputs are focused.
     */
    $('.form-row > input.input-text, .form-row > textarea').on('click', function(){
        $( this ).parent().addClass( 'focused' );

    });
    $(document).on('focus', '.form-row > input.input-text, .form-row > textarea', function(e){
        $( this ).parent().addClass( 'focused' );
    });

});
