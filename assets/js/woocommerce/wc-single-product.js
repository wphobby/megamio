jQuery(document).ready(function ($) {

    //Increase/decrease product quantity
    $(document).on('click', '.quantity .increase, .quantity .decrease', function(e){
        e.preventDefault();

        var $this = $( this ),
            $qty = $this.siblings( '.qty' ),
            step = parseInt( $qty.attr( 'step' ), 10 ),
            current = parseInt( $qty.val(), 10 ),
            min = parseInt( $qty.attr( 'min' ), 10 ),
            max = parseInt( $qty.attr( 'max' ), 10 );

        min = min ? min : 1;
        max = max ? max : current + 1;

        if ( $this.hasClass( 'decrease' ) && current > min ) {
            $qty.val( current - step );
            $qty.trigger( 'change' );
        }
        if ( $this.hasClass( 'increase' ) && current < max ) {
            $qty.val( current + step );
            $qty.trigger( 'change' );
        }

    });

    //Shop top bar filter
    $(document).on('click', '#megamio-catalog-filter', function(e){

        console.log('click catalog-filter');

        e.preventDefault();
        var $shopTopbar = $( '#un-shop-topbar' );

        $( this ).toggleClass( 'active' );
        $shopTopbar.slideToggle();
    });

});