jQuery(document).ready(function ($) {

	$(".wc-login").submit(function(e) {

		var username = $( 'input[name=username]', this ).val(),
			password = $( 'input[name=password]', this ).val(),
			remember = $( 'input[name=rememberme]', this ).is( ':checked' ),
			$button = $( '[type=submit]', this ),
			$form = $( '.wc-login' ),
			$box = $form.next( '.woocommerce-error' );

		if ( ! username ) {
			$( 'input[name=username]', this ).focus();

			return false;
		}

		if ( ! password ) {
			$( 'input[name=password]', this ).focus();

			return false;
		}

		e.preventDefault(); // avoid to execute the actual submit of the form.

		$form.find( '.woocommerce-error' ).remove();
		$button.append( '<span class="sign-in-loading"></span>' );

		$.post(
			megamio_ajaxurl,
			{
				action: 'megamio_login_authenticate',
				creds: {
					user_login: username,
					user_password: password,
					remember: remember
				}
			},
			function( response ) {
				if ( ! response.success ) {
					if ( ! $box.length ) {
						$box = $( '<div class="woocommerce-error" role="alert"/>' );

						$box.append( '<ul class="error-message" />' )
							.append( '<a class="error-message-close" href="javascript:void(0)"><i class="flaticon-close"></i></a>' );


						$box.hide().prependTo( $form );
					}

					$box.find( '.error-message' ).html( '<li>' + response.data + '</li>' );
					$box.fadeIn();
					$button.html( $button.attr( 'value' ) );
					$('.sign-in-loading').remove();
				} else {
					//$button.html( $button.data( 'signed' ) );
					window.location.reload();
				}
			}
		).done(function(){

		});


	}).on( 'click', '.woocommerce-error .error-message-close', function() {
		// Remove the error message to fix the layout issue.
		$( this ).closest( '.woocommerce-error' ).fadeOut( function() {
			$( this ).remove();
		} );

		return false;
	} );


});