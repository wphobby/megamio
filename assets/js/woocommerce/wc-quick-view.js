jQuery(document).ready(function($){

	//open the quick view panel
	$(document).on('click', '.megamio_product_quick_view_button', function(event){

		var $this = $(this);

		$this.closest('.card-image').addClass('loading');
		$('.modal-dialog').empty();

		var product_id  = $(this).data('product_id');

		$.ajax({
			url: megamio_ajaxurl,
			data: {
				"action" : "megamio_product_quick_view",
				'product_id' : product_id
			},
			success: function(results) {
				var $html = $(results);
				var $gallery = $html.find( '.woocommerce-product-gallery' );
				// Force height for images
				 $gallery.find( 'img' ).css( 'height', '580' );
				// Init flex slider.
				if ( $gallery.find( '.woocommerce-product-gallery__image' ).length > 1 ) {
					$gallery.flexslider( {
						selector      : '.woocommerce-product-gallery__wrapper > .woocommerce-product-gallery__image',
						animation     : 'slide',
						animationLoop : true,
						animationSpeed: 500,
						controlNav    : false,
						directionNav  : true,
						prevText      : '<span class="prev"><i class="fa fa-angle-left"></i></span>',
						nextText      : '<span class="next"><i class="fa fa-angle-right"></i></span>',
						slideshow     : false,
						rtl           : $( document.body ).hasClass( 'rtl' ),
						start         : function() {
							$gallery.css( 'opacity', 1 );
						},
					} );
				} else {
					$gallery.css( 'opacity', 1 );
				}

				$('.modal-dialog').html($html);
			},
			error: function(errorThrown) { console.log(errorThrown); },

		}).done(function(){
			$this.closest('.card-image').removeClass('loading');
		});


	});

	//close the quick view panel
	$('body').on('click', function(event){
		if( ($(event.target).is('.flaticon-close') || $(event.target).is('.modal')) ) {

		}
	});

});