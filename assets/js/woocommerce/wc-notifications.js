jQuery(document).ready(function ($) {

	$(window).load(function(e) {
	});

	$(document).on('added_to_cart', function(event, data) {
		display_custom_notifications();
	});

	function display_custom_notifications() {
		var html = '<div class="message-box woo-message-box success">Product was added to cart successfully</div>';
		$(html).appendTo(".wc-notification-wrapper").fadeIn('slow').animate({opacity: 1.0}, 2500).fadeOut('slow');
	    $('.added_to_cart').remove();
	}

});