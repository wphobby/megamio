jQuery(document).ready(function($){

	//New Arrivals Products Categories Tabs
	$(document).on('click', '.nav-item', function(e){

		var $this = $(this);
		var $parent = $( '.megamio-products' );
		$container = $parent.find( '.product-wrapper' );

		e.preventDefault();

		var	attr = $parent.data( 'attr' );
		var	nonce = $parent.data( 'nonce' );

		attr.category = $this.attr( 'data-filter' );

		$parent.addClass( 'loading' );


		$.post( megamio_ajaxurl, {
			action: 'megamio_load_products',
			attr  : attr,
			nonce : nonce,

		}, function( response ) {
			if ( !response.success ) {

			}

			$container.empty().html(response.data);
			$parent.removeClass( 'loading' );

		} ).fail( function() {

		} );





	});

});