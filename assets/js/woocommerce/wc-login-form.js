jQuery(document).ready(function ($) {

	// Login and Register
	$(document).on('click', '.account-tab-link', function(event){

		var that = $(this),
			target = that.attr('href');

		that.parent().siblings().find('.account-tab-link').removeClass('current');
		that.addClass('current');

		$('.account-forms').find($(target)).siblings().stop().fadeOut(function(){
			$('.account-forms').find($(target)).fadeIn();
		});

		//$(target).siblings().stop().fadeOut(function(){
		//	$(target).fadeIn();
		//});

		return false;


	});

	$(document).on('focus', '.form-row > span > input.input-text', function(e){
		$( this ).parent().prev('label').addClass( 'focused' );
	});

	$(document).on('blur', '.form-row > span > input.input-text', function(e){
		if ( $( this ).val() == '' ) {
			$( this ).parent().prev('label').removeClass( 'focused' );
		}
	});

	$(document).find( '.form-row > span > input.input-text' ).each( function() {
		if ( $( this ).val() != '' ) {
			$( this ).parent().prev('label').addClass( 'focused' );
		}
	});
});