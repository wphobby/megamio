<?php
/**
 * The header of all pages.
 *
 * Displays all of the <head> section and everything before <main id="main">
 *
 *
 * @package    megamio
 * @author     wphobby
 * @link       https://wphobby.com
 */
global $megamio_options;
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
    <head>
        <meta charset="<?php bloginfo('charset'); ?>">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="profile" href="http://gmpg.org/xfn/11">
        <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">

        <?php wp_head(); ?>
    </head>

<body <?php body_class(); ?>>
<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please upgrade your browser to improve your experience.</p>
<![endif]-->
<?php do_action( 'megamio_before_header' ); ?>

<?php
if ( (isset($megamio_options['header_style'])) ) {
    if ('header-default' === $megamio_options['header_style']) {
        $header_load = 'template-parts/header/header-default.php';
    }else if('header-center' === $megamio_options['header_style'] ){
        $header_load = 'template-parts/header/header-center.php';
    }
}else{
    $header_load = 'template-parts/header/header-default.php';
}

if(isset($_GET['header_style']) && $_GET['header_style'] == 'center'){
    $header_load = 'template-parts/header/header-center.php';
}

$header_transparency_class = "";
if ( $megamio_options['header_transparent'] && ('transparent' == get_post_meta( get_the_ID(), 'header_background', true ))) {
    $header_transparency_class = "header-transparent";
}

include(locate_template($header_load));

do_action( 'megamio_after_header' );
?>

<div id="content" class="site-content">
    <div class="site-content-container <?php echo esc_attr( apply_filters( 'megamio_content_container_class', '' ) ) ?>">

