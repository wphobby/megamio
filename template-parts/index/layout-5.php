<?php
global $megamio_options;
?>
<!--====== MASONRY BLOG PART START ======-->

<section class="masonry-blog-area pt-30 pb-60">
    <div class="container">
        <div class="row grid">
            <?php
            // loop content with blog wrapper
            if ( have_posts() ) :
                while ( have_posts() ) : the_post();
                    echo '<div class="col-lg-4 col-md-6 grid-item">';
                    get_template_part( 'template-parts/post/layout', '5' );
                    echo '</div>';
                endwhile; /* End Loop */
                // show pagination.
                get_template_part( 'template-parts/pagination/pagination' );
            else:
                get_template_part( 'template-parts/content', 'none' );
            endif;
            ?>
        </div> <!-- row -->

        <div class="row">
            <div class="col-lg-12">
                <div class="readmore-btn pt-35 text-center">
                    <a href="#" class="main-btn">load more</a>
                </div>
            </div>
        </div> <!-- row -->
    </div> <!-- container -->
</section>

<!--====== MASONRY BLOG PART ENDS ======-->
