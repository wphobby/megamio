<?php
/**
* Template part for displaying blog layout 1
*
* @link    https://developer.wordpress.org/themes/basics/template-hierarchy/
*
* @package megamio
*/
?>

<section class="blog-page pt-15">
        <?php
        // loop content with blog wrapper
        if ( have_posts() ) :
            while ( have_posts() ) : the_post();
                get_template_part( 'template-parts/post/layout', '1' );
            endwhile; /* End Loop */
        else:
            get_template_part( 'template-parts/content', 'none' );
        endif;
        // show pagination.
        get_template_part( 'template-parts/pagination/pagination' );
        ?>
</section>