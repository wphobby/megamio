<?php
global $megamio_options;
?>

<!--====== NEWS & TRENDS PART START ======-->

<section class="news-trends pt-50 pb-55">
    <div class="container">
        <div class="title-bar border-bottom">
            <div class="row">
                <div class="col-lg-6 col-8">
                    <div class="section-title">
                        <h4>News & Trends</h4>
                    </div>
                </div>
                <div class="col-lg-6 col-4">
                    <div class="view-more text-right">
                        <a href="#" class="view-btn">View All</a>
                    </div> <!-- view more -->
                </div>
            </div> <!-- row -->
        </div> <!-- title bar -->
            <?php
            // loop content with blog wrapper
            if ( have_posts() ) :
            ?>
               <div class="row">
               <?php
               while ( have_posts() ) : the_post();
                    get_template_part( 'template-parts/post/layout', '3' );
               endwhile; /* End Loop */
               ?>
               </div> <!-- row -->
               <?php
                // show pagination.
                get_template_part( 'template-parts/pagination/pagination' );
            else:
                get_template_part( 'template-parts/content', 'none' );
            endif;
            ?>
    </div> <!-- container -->
</section>

<!--====== NEWS & TRENDS PART ENDS ======-->