<?php
global $megamio_options;
?>

<section class="latest-posts pb-55">
    <div class="container">
        <div class="row">
            <div class="col-lg-8">
                <div class="title-bar border-bottom pt-55">
                    <div class="row">
                        <div class="col-lg-6 col-8">
                            <div class="section-title">
                                <h4>Latest Posts</h4>
                            </div>
                        </div>
                        <div class="col-lg-6 col-4">
                            <div class="view-more text-right">
                                <a href="#" class="view-btn">View All</a>
                            </div> <!-- view more -->
                        </div>
                    </div> <!-- row -->
                </div> <!-- title bar -->
                <div class="row">
                    <?php
                    // loop content with blog wrapper
                    if ( have_posts() ) :
                        while ( have_posts() ) : the_post();
                            get_template_part( 'template-parts/post/layout', '2' );
                        endwhile; /* End Loop */
                        // show pagination.
                        get_template_part( 'template-parts/pagination/pagination' );
                    else:
                        get_template_part( 'template-parts/content', 'none' );
                    endif;
                    ?>
                </div> <!-- row -->
            </div>
            <?php get_sidebar(); ?>
        </div> <!-- row -->
    </div> <!-- container -->
</section>
