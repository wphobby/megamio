<?php
/**
 * Template for displaying login panel.
 *
 * @package megamio
 */

if ( ! class_exists( 'WooCommerce' ) ) {
   return;
}
?>

<!--====== USER LOGIN PART START ======-->

<div class="user-login-wrapper">
    <div class="user-login-area">
        <div class="login-header clearfix">
            <div class="login-title">
                <h4 class="title"><?php esc_html_e( 'Sign in', 'megamio' ) ?></h4>
            </div>
            <div class="login-close">
                <a class="login-form-close" href="javascript:void(0)"><i class="flaticon-close"></i></a>
            </div>
        </div> <!-- login header -->
        <div class="login-body pt-30">
            <div class="login-reg login wc-login">
                <form method="post">
                    <?php do_action( 'woocommerce_login_form_start' ); ?>
                    <div class="single-input-form">
                        <input placeholder="Username" name="username" type="text">
                    </div>
                    <div class="single-input-form">
                        <input placeholder="Password" name="password" type="password">
                    </div>
                    <?php do_action( 'woocommerce_login_form' ); ?>
                    <div class="single-checkout-form">
                        <label for="check">
                            <input id="check" name="check" type="checkbox">
                            <span class="label-text"><?php esc_html_e( 'Remember Me', 'megamio' ) ?></span>
                        </label>
                    </div>
                    <div class="single-input-form">
                        <?php
                        $nonce_field = wp_nonce_field( 'woocommerce-login', 'woocommerce-login-nonce', true, false );
                        echo preg_replace( '|\s(id)="[^"]+"|i', '', $nonce_field );
                        ?>
                        <button class="main-btn main-btn-2" name="login" type="submit" data-signed="<?php esc_attr_e( 'Signed In', 'megamio' ); ?>"><?php esc_html_e( 'Sign in', 'megamio' ) ?></button>
                    </div>
                    <?php do_action( 'woocommerce_login_form_end' ); ?>

                </form>
                <?php if ( get_option( 'woocommerce_enable_myaccount_registration' ) === 'yes' ) : ?>

                    <div class="single-input-form">
                        <button id="register-btn" class="main-btn" type="submit"><?php esc_html_e( 'Register', 'megamio' ) ?></button>
                    </div>

                <?php endif; ?>
                <div class="login-account text-center">
                    <a href="<?php echo esc_url( wp_lostpassword_url() ); ?>"><?php esc_html_e( 'Lost Password', 'megamio' ) ?>?</a>
                </div>
            </div>

            <div class="login-reg register">
                <form method="post">
                    <?php do_action( 'woocommerce_register_form_start' ); ?>
                    <div class="single-input-form">
                        <input placeholder="Email Address" name="email" type="text">
                    </div>
                    <?php if ( 'no' === get_option( 'woocommerce_registration_generate_password' ) ) : ?>
                    <div class="single-input-form">
                        <input placeholder="Password" name="password" type="password">
                    </div>
                    <?php endif; ?>
                    <?php do_action( 'woocommerce_register_form' ); ?>
                    <div class="single-input-form">
                        <?php
                        $nonce_field = wp_nonce_field( 'woocommerce-register', 'woocommerce-register-nonce', true, false );
                        echo preg_replace( '|\s(id)="[^"]+"|i', '', $nonce_field );
                        ?>
                        <button class="main-btn main-btn-2" name="register" type="submit"><?php esc_html_e( 'Sign up', 'megamio' ) ?></button>
                    </div>
                    <?php do_action( 'woocommerce_register_form_end' ); ?>
                </form>
                <div class="login-account text-center">
                    <a id="already-account" href="javascript:void(0)"><?php esc_html_e( 'Already has an account', 'megamio' ) ?>?</a>
                </div>
            </div>
        </div>  <!-- login body -->
    </div> <!-- canvas -->
</div>

<!--====== USER LOGIN PART ENDS ======-->



