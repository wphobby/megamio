<!--====== FOOTER PART START ======-->

<footer class="footer-area">
    <div class="container">
        <div class="footer-widgets pt-40 pb-90">
            <div class="row">
                <?php get_sidebar('footer'); ?>
            </div> <!-- row -->
        </div> <!-- footer widgets -->

        <div class="footer-copyright pt-10 pb-25">
            <div class="row">
                <div class="col-lg-6">
                    <div class="copyright text-center text-lg-left pt-15">
                        <p>&copy; 2019 <span>Megamio</span>. Design by WPHobby</p>
                    </div>
                </div>
                <div class="col-lg-6">
                    <?php include(locate_template('template-parts/menu/menu-footer-bottom.php')); ?>
                </div>
            </div> <!-- row -->
        </div> <!-- footer copyright -->
    </div> <!-- container -->
</footer>

<!--====== FOOTER PART ENDS ======-->