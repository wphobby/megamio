<?php
/**
 * Template for displaying full screen search form.
 *
 * @package megamio
 */

?>

<!--====== SEARCH BOX PART START ======-->

<div class="search-box">
    <div class="search-header">
        <div class="container mt-60">
            <div class="row">
                <div class="col-6">
                    <h5 class="search-title">Search</h5> <!-- search title -->
                </div>
                <div class="col-6">
                    <div class="search-close text-right">
                        <button class="search-close-btn">Close <span></span><span></span></button>
                    </div> <!-- search close -->
                </div>
            </div> <!-- row -->
        </div> <!-- container -->
    </div> <!-- search header -->
    <div class="search-body">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="search-form">
                        <form action="<?php echo home_url('/'); ?>" method="get">
                            <input type="text" name="s" placeholder="Search for Products" value="<?php the_search_query(); ?>">
                            <button><i class="flaticon-search"></i></button>
                        </form>
                    </div>
                </div>
            </div>  <!-- row -->
        </div> <!-- container -->
    </div> <!-- search body -->
    <div class="search-footer">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="search-footer-content">
                        <h4 class="mb-25">Quick Links</h4>
                        <p>SS2019 Dresses Accessories Footwear Sweatshirt</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!--====== SEARCH BOX PART ENDS ======-->
