<!--====== FOOTER PART START ======-->

<footer class="footer-area-2 pt-110 pb-150">
    <div class="container">
        <div class="row">
            <div class="col-lg-4">
                    <?php
                    wp_nav_menu( array(
                        'theme_location'  => 'footer_bottom',
                        'menu_class'      => 'footer-menu-2',
                        'container'       => 'div',
                        'container_class' => 'footer-content text-center text-lg-left pt-30',
                    ) );
                    ?>
            </div>
            <div class="col-lg-4">
                <div class="footer-content text-center pt-40">
                    <a href="<?php echo esc_url( home_url( '/' ) ); ?>">
                        <?php
                        $custom_logo_id = get_theme_mod('custom_logo');
                        if($custom_logo_id) {
                            $custom_logo_image = wp_get_attachment_image_src($custom_logo_id, 'full');
                            ?>
                            <img src="<?php echo $custom_logo_image[0]; ?>" alt="Logo"/>
                            <?php
                        }else{
                            bloginfo('name');
                        }
                        ?>
                    </a>
                    <p class="mt-30">&copy; 2019 <a href="#">WPHobby</a>. All Rights Resevered</p>
                </div> <!-- footer content -->
            </div>
            <div class="col-lg-4">
                <div class="footer-content text-center d-sm-flex justify-content-center justify-content--lg-end pt-30">
                    <span>Follow Us On Social</span>
                    <ul class="social">
                        <li><a href="#"><i class="flaticon-facebook-letter-logo"></i></a></li>
                        <li><a href="#"><i class="flaticon-twitter"></i></a></li>
                        <li><a href="#"><i class="flaticon-pinterest-logo"></i></a></li>
                        <li><a href="#"><i class="flaticon-vimeo"></i></a></li>
                        <li><a href="#"><i class="flaticon-youtube"></i></a></li>
                        <li><a href="#"><i class="flaticon-instagram-logo"></i></a></li>
                    </ul>
                </div>
            </div>
        </div> <!-- row -->
    </div> <!-- container -->
</footer>

<!--====== FOOTER PART ENDS ======-->