<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package megamio
 */
?>
<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <?php if ( 'none' != get_post_meta( get_the_ID(), 'page_title_display', true ) ) : ?>
    <div id="page-title-bar" class="page-title-bar">
        <div class="container">
            <div class="wrap w-100 d-flex align-items-center">
                <div class="page-title-bar-inner d-flex flex-column align-items-center w-100">
                        <div class="breadcrumb">
                            <?php if (function_exists('bcn_display')): ?>
                                <?php bcn_display(); ?>
                            <?php else: ?>
                                <?php the_title( '<h4 class="entry-title">', '</h4>' ); ?>
                            <?php endif; ?>
                        </div>
                </div>
            </div>
        </div>
    </div>

    <header class="entry-header">
       <?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
       <?php if ( $subtitle = get_post_meta( get_the_ID(), '_subtitle', true ) ) : ?>
             <h4 class="entry-subtitle"><?php echo wp_kses_post( $subtitle ); ?></h4>
       <?php endif; ?>
    </header><!-- .entry-header -->
    <?php endif; ?>



    <div class="entry-content">
        <?php
        the_content();

        wp_link_pages( array(
            'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'megamio' ),
            'after'  => '</div>',
        ) );
        ?>
    </div><!-- .entry-content -->
</div><!-- #post-<?php the_ID(); ?> -->


