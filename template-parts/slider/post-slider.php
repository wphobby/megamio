<?php
/**
 * Template for displaying post slider
 *
 * @package megamio
 */

global $megamio_options;
?>

<section class="slider-area">
    <div class="container">
        <div class="slider-active">
        <?php
        // options
        $args = array(
            'posts_per_page'      => 3,
            'post_type'           => 'post',
            'ignore_sticky_posts' => true,
        );

        $query_slider_posts = new WP_Query( $args );
        while ( $query_slider_posts->have_posts() ) :
            $query_slider_posts->the_post();
            $thumbnail = '';
            if ( has_post_thumbnail() ) {
                $thumbnail = 'style="background-image: url(' . get_the_post_thumbnail_url( get_the_ID() ) . ')"';
            }
            ?>
                <div class="single-slider bg_cover d-flex align-items-center" <?php echo $thumbnail;?> >
                    <div class="slider-content text-center">
                      <div class="row justify-content-center">
                         <div class="col-lg-8">
                                <h4 class="slider-title" data-animation="fadeInUp" data-delay="0.3s"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
                                <p data-animation="fadeInUp" data-delay="1s"><?php echo get_the_time( get_option('date_format') ); ?>   -  <span><?php echo get_the_author(); ?></span> </p>
                         </div>
                      </div> <!-- row -->
                    </div> <!-- slider content -->
                </div> <!-- single slider -->
            <?php
        endwhile;
        wp_reset_postdata();
        ?>
        </div> <!-- single active -->
    </div>
</section>