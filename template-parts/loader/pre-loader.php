<?php
/**
 * Preloader before header.
 *
 * @package    megamio
 * @author     wphobby
 * @link       https://wphobby.com
 */
?>
<!--====== PRELOADER PART START ======-->

<div class="preloader">
    <div class="loader">
        <!-- Preloader Elements -->
        <div class="spinner"></div>
        <div class="descript">
            <h5>Loading..</h5>
        </div>
    </div>
</div>

<!--====== PRELOADER PART ENDS ======-->
