<?php
/**
 * Script to show pagination.
 *
 * @package megamio
 */

global $wp_query;
$big = 9999999999;

// if only have one page don't show pagination.
if ( $wp_query->max_num_pages <= 1 ) {
	return;
}
?>
<div class="row justify-content-center">
	<div class="col-lg-10">
		<nav>
			<ul class="pagination justify-content-center pt-85">
			<?php
			   $pages = paginate_links(
					array(
						'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
						'format' => '?paged=%#%',
						'current' => max( 1, get_query_var('paged') ),
						'total' => $wp_query->max_num_pages,
						'type'  => 'array',
						'prev_text'    => esc_html__( 'Previous', 'megamio' ),
						'next_text'    => esc_html__( 'Next', 'megamio' ),
					)
				);

			   foreach ( $pages as $page ) {
				   $paged = ( get_query_var('paged') == 0 ) ? 1 : get_query_var('paged');
				   echo '<li class="page-item">'.$page.'</li>';
			   }
			?>
			</ul>
		</nav>
	</div>
</div> <!-- row -->