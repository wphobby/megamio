<?php
/**
 * Template part for displaying full width single post layout
 *
 * @link    https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package megamio
 */
global $megamio_options;
?>
<?php while ( have_posts() ) : the_post(); ?>
    <div class="row">
        <div class="col-lg-12">
            <div class="blog-single">
                <?php if( has_post_thumbnail() ):?>
                    <div class="blog-image">
                        <?php the_post_thumbnail('megamio-blog-list'); ?>
                    </div>
                <?php endif; ?>
                <div class="blog-single-content pt-90">
                    <div class="row justify-content-center">
                        <div class="col-lg-10">
                            <div class="blog-content">
                                <div class="blog-main-content">
                                    <h2 class="blog-title"><?php the_title(); ?></h2>
                                    <ul class="blog-date mt-10">
                                        <li><a href="#"><?php echo get_the_author(); ?></a></li>
                                        <li><a href="#"><?php echo get_the_time( get_option('date_format') ); ?></a></li>
                                        <li><a href="#"><?php foreach((get_the_category()) as $category) { echo $category->cat_name . ' '; } ?></a></li>
                                    </ul>
                                    <?php the_content(); ?>
                                </div> <!-- blog content -->

                                <?php
                                megamio_content_nav();

                                // If comments are open or we have at least one comment, load up the comment template
                                if ( comments_open() || '0' != get_comments_number() )
                                    comments_template();
                                ?>
                            </div> <!-- blog content -->
                        </div>
                    </div> <!-- row -->
                </div>
            </div> <!-- blog single -->
        </div>
    </div> <!-- row -->
<?php endwhile; ?>
