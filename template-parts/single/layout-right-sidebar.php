<?php
/**
 * Template part for displaying right sidebar single post layout
 *
 * @link    https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package megamio
 */
global $megamio_options;
?>
<?php while ( have_posts() ) : the_post(); ?>
<div class="row ">
    <div class="col-lg-8">
        <div class="details-content pt-60 pb-55">
                    <?php if( has_post_thumbnail() ):?>
                        <div class="details-thum mb-35">
                            <?php the_post_thumbnail(array(770, 550, true)); ?>
                        </div>
                    <?php endif; ?>
                    <span>
                    <?php the_category(', '); ?>
                        -
                        <?php echo get_the_time( get_option('date_format') ); ?>
                    </span>
                    <h2 class="post-title mt-45"><?php the_title(); ?></h2>
                    <?php the_content(); ?>


                    <div class="meta pt-40 pb-15">
                        <div class="tag-social">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="tag mt-15">
                                        <ul class="d-flex">
                                            <?php
                                            $tags = get_the_tags();
                                            if(isset($tags) && !empty($tags)) {
                                                foreach ((get_the_tags()) as $tag) {
                                                    echo '<li><a href="' . get_tag_link($tag->term_id) . '">#' . $tag->name . '</a></li>';
                                                }
                                            }
                                            ?>
                                        </ul>
                                    </div> <!-- tag -->
                                </div>
                                <div class="col-md-6">
                                    <div class="social mt-15">
                                        <ul class="d-flex justify-content-start justify-content-md-end">
                                            <li><a href="#"><i class="flaticon-google-plus-logo"></i></a></li>
                                            <li><a href="#"><i class="flaticon-pinterest-logo"></i></a></li>
                                            <li><a href="#"><i class="flaticon-twitter"></i></a></li>
                                            <li><a href="#"><i class="flaticon-friends"></i></a></li>
                                            <li><a href="#"><i class="flaticon-paper-plane"></i></a></li>
                                            <li><a href="#"><i class="flaticon-streaming"></i></a></li>
                                        </ul>
                                    </div> <!-- social -->
                                </div>
                            </div> <!-- row -->
                        </div> <!-- tag social -->
                        <?php
                        megamio_content_nav();
                        ?>
                    </div> <!-- meta -->

                    <?php
                    // Show author box if enabled.
                    if ( true === $megamio_options['single_enable_author_box'] ) {
                        get_template_part( 'template-parts/author/author', 'box' );
                    }
                    ?>

                </div> <!-- details content -->
        <?php
        // If comments are open or we have at least one comment, load up the comment template
        if ( comments_open() || '0' != get_comments_number() )
            comments_template();
        ?>    </div>
    <?php get_sidebar(); ?>
</div> <!-- row -->
<?php endwhile; ?>