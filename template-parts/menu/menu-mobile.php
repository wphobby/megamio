<?php
/**
 * Displays mobile menu
 *
 * @package megamio
 */
?>

<!--====== MOBILE MENU PART START ======-->

<div class="offcanvas-menu-wrapper">
    <div class="offcanvas-menu">
        <a class="close-mobile-menu" href="javascript:void(0)"><i class="flaticon-close"></i></a>

        <div class="mobile-language-current">
            <div class="language">
                <a class="language-current" href="#"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/language/en.png" alt="">English <i class="fa fa-angle-down"></i></a>
                <ul class="language-sub">
                    <li><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/language/nl.png" alt="">Nederlands</a></li>
                    <li><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/language/la.png" alt="">Latin</a></li>
                    <li><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/language/nl.png" alt="">Nederlands</a></li>
                    <li><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/language/la.png" alt="">Latin</a></li>
                </ul>
            </div> <!-- language -->
            <div class="language current">
                <a class="language-current" href="#"> $USD</a>
                <ul class="language-sub">
                    <li><a href="#">URO</a></li>
                    <li><a href="#">YEN</a></li>
                    <li><a href="#">POUND</a></li>
                </ul>
            </div> <!-- current -->
        </div>

        <div class="mobile-menu pt-30">
            <nav>
                <?php
                $walker = new Megamio_Nav_Walker;
                wp_nav_menu(array(
                    'theme_location'  => 'off_canvas',
                    'fallback_cb'     => false,
                    'container'       => false,
                    'walker' 		  => $walker
                ));
                ?>
            </nav>
        </div>
    </div>
    <div class="overlay"></div>
</div>

<!--====== MOBILE MENU PART ENDS ======-->