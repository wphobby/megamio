<?php
/**
 * Displays footer menu section three
 *
 * @package megamio
 */
wp_nav_menu( array(
    'theme_location' => 'footer_section_three',
    'container'      => 'div',
    'container_class' => 'footer-link mt-50',
) );
?>



