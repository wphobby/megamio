<?php
/**
 * Displays Social Medias
 *
 * @package megamio
 */

global $megamio_options;
//Social media icons
$social_icons = $megamio_options['social_icons'];
?>

<div class="header-social pt-15 d-none d-sm-block">
    <?php
    if ( ! empty( $social_icons ) && is_array( $social_icons ) ) :
        ?>

        <ul class="d-flex justify-content-center justify-content-lg-start">
            <?php
            // Loop through each of the social links.
            foreach ( $social_icons as $social_icon ) {
                if ( ! empty( $social_icon['social_url'] ) ) :
                    ?>

                    <li>
                        <a href="<?php echo esc_url( $social_icon['social_url'] ); ?>">
                            <i class="<?php echo esc_attr( $social_icon['social_icon'] ); ?>"></i>
                        </a>
                    </li>

                    <?php
                endif;
            }
            ?>
        </ul>

    <?php endif; ?>
</div> <!-- header social -->
