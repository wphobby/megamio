<?php
/**
 * Displays footer bottom menu
 *
 * @package megamio
 */
wp_nav_menu( array(
    'theme_location' => 'footer_bottom',
    'container'      => 'div',
    'container_class' => 'footer-menu text-center text-lg-right pt-15',
) );
?>



