<?php
/**
 * Displays footer menu section two
 *
 * @package megamio
 */
wp_nav_menu( array(
    'theme_location' => 'footer_section_two',
    'container'      => 'div',
    'container_class' => 'footer-link mt-50',
) );
?>



