<?php
/**
 * Displays primary menu
 *
 * @package megamio
 */

global $megamio_options;
if ( (isset($megamio_options['header_style'])) ) {
    if ('header-default' === $megamio_options['header_style']) {
        $menu_style = '';
    }else if('header-center' === $megamio_options['header_style']){
        $menu_style = 'text-right';
    }
}
?>
<div class="header-menu d-none d-lg-block">
    <div class="main-menu <?php echo $menu_style; ?>">
        <nav id="mobile-menu">
            <?php
               $walker = new Megamio_Primary_Nav_Walker;
               wp_nav_menu(array(
                  'theme_location'  => 'primary',
                  'fallback_cb'     => false,
                  'container'       => false,
                  'walker' 		    => $walker
               ));
            ?>
        </nav>
    </div>
    <div class="mobile-menu"></div>
</div> <!-- header menu -->




