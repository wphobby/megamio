<?php
/**
 * Displays footer menu section one
 *
 * @package megamio
 */
wp_nav_menu( array(
    'theme_location' => 'footer_section_one',
    'container'      => 'div',
    'container_class' => 'footer-link mt-50',
) );
?>



