<?php
/**
 * Displays off canvas menu
 *
 * @package megamio
 */
?>

<!--====== CANVAS MENU PART START ======-->

<div class="canvas-wrapper">
  <div class="canvas-area">
    <div class="canvas">
        <div class="canvas-close text-right">
            <a class="canvas-menu-close" href="javascript:void(0)"><i class="flaticon-close"></i></a>
        </div>
        <div class="canvas-top-bar clearfix pt-15">
            <ul>
                <li><a class="active" href="#">Eng</a></li>
                <li><a href="#">fra</a></li>
                <li><a href="#">ger</a></li>
            </ul>
            <ul class="many pl-45">
                <li><a class="active" href="#">USD</a></li>
                <li><a href="#">Euro</a></li>
                <li><a href="#">pround</a></li>
            </ul>
        </div> <!-- canvas top bar -->

        <div class="canvas-menu pt-50">
            <?php
            $walker = new Megamio_Nav_Walker;
            wp_nav_menu(array(
                'theme_location'  => 'off_canvas',
                'fallback_cb'     => false,
                'container'       => false,
                'walker' 		  => $walker
            ));
            ?>
        </div> <!-- canvas menu -->

        <div class="canvas-social pt-50">
            <ul>
                <li><a href="#"><i class="flaticon-facebook-letter-logo"></i></a></li>
                <li><a href="#"><i class="flaticon-twitter"></i></a></li>
                <li><a href="#"><i class="flaticon-pinterest-logo"></i></a></li>
                <li><a href="#"><i class="flaticon-instagram-logo"></i></a></li>
                <li><a href="#"><i class="flaticon-vimeo"></i></a></li>
                <li><a href="#"><i class="flaticon-youtube"></i></a></li>
            </ul>
        </div> <!-- canvas social -->

        <div class="canvas-copyright pt-30 pb-50">
            <p>© Megamio 2019. All Rights Reseverd</p>
            <p>Design by WPHobby</p>
        </div>  <!-- canvas Copyright -->
    </div> <!-- canvas -->
</div>
  <div class="overlay"></div>
</div>

<!--====== CANVAS MENU PART ENDS ======-->
