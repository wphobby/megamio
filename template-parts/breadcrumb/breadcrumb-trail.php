<?php
/**
 * Shows breadcrumb
 *
 * @package megamio
 */
?>
<section class="page-banner gray-bg">
		<div class="row">
			<div class="col-lg-12">
				<div class="page-breadcrumb text-center pt-60 pb-60">
					<h2>blog sidebar</h2>
					<nav>
						<ol class="breadcrumb justify-content-center mt-20">
							<li class="breadcrumb-item"><a href="#">Home</a></li>
							<li class="breadcrumb-item"><a href="#">Blog</a></li>
							<li class="breadcrumb-item active" aria-current="page"><?php the_title(); ?></li>
						</ol>
					</nav>
				</div> <!-- page breadcrumb -->
			</div>
		</div> <!-- row -->
</section>


