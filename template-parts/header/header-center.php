<?php
/**
 * Header to show when style is menu under.
 *
 * @package    megamio
 * @author     wphobby
 * @link       https://wphobby.com
 */
?>
<!--====== HEADER PART START ======-->

<header class="header-area-2">
    <div class="header-top pb-15">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="header-info pt-15 text-center text-md-left">
                        <p>Free shipping all orders over $60</p>
                    </div> <!-- header info -->
                </div>
                <div class="col-md-6">
                    <div class="header-social pt-15 text-center text-md-right">
                        <ul>
                            <li><a href="#"><i class="flaticon-facebook-letter-logo"></i></a></li>
                            <li><a href="#"><i class="flaticon-twitter"></i></a></li>
                            <li><a href="#"><i class="flaticon-pinterest-logo"></i></a></li>
                            <li><a href="#"><i class="flaticon-vimeo"></i></a></li>
                            <li><a href="#"><i class="flaticon-youtube"></i></a></li>
                        </ul>
                    </div> <!-- header social -->
                </div>
            </div> <!-- row -->
        </div> <!-- container -->
    </div> <!-- header top -->

    <div class="header-logo-area d-none d-lg-block">
        <div class="container">
            <div class="header-logo">
                <div class="row align-items-center">
                    <div class="col-md-4">
                        <div class="Header-logo-left">
                            <div class="language">
                                <a class="language-current" href="#"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/language/en.png" alt="">English <i class="fa fa-angle-down"></i></a>
                                <ul class="language-sub">
                                    <li><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/language/nl.png" alt="">Nederlands</a></li>
                                    <li><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/language/la.png" alt="">Latin</a></li>
                                    <li><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/language/nl.png" alt="">Nederlands</a></li>
                                    <li><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/language/la.png" alt="">Latin</a></li>
                                </ul>
                            </div> <!-- language -->
                            <div class="language current">
                                <a class="language-current" href="#"> $USD</a>
                                <u class="language-sub">
                                    <li><a href="#">URO</a></li>
                                    <li><a href="#">YEN</a></li>
                                    <li><a href="#">POUND</a></li>
                                </u>
                            </div> <!-- current -->
                        </div> <!-- Header logo left -->
                    </div>
                    <div class="col-md-4">
                        <div class="logo-area text-center">
                            <a href="<?php echo esc_url( home_url( '/' ) ); ?>">
                                <?php
                                $custom_logo_id = get_theme_mod('custom_logo');
                                if($custom_logo_id) {
                                    $custom_logo_image = wp_get_attachment_image_src($custom_logo_id, 'full');
                                    ?>
                                    <img src="<?php echo $custom_logo_image[0]; ?>" alt="Logo"/>
                                    <?php
                                }else{
                                    ?>
                                    <img src="<?php echo get_template_directory_uri() . '/assets/images/logo.svg'; ?>" alt="Logo"/>
                                    <?php
                                }
                                ?>
                            </a>
                        </div> <!-- logo area -->
                    </div>
                    <div class="col-md-4">
                        <div class="offset-menu-wrapper text-right">
                            <ul>
                                <li>
                                    <div class="menu-search d-none d-sm-block">
                                        <a class="search-open" href="javascript:void(0)"><i class="flaticon-search"></i></a>
                                    </div>
                                </li>
                                <li>
                                    <div class="menu-user d-none d-sm-block">
                                        <a class="login-form-open" href="javascript:void(0)"><i class="flaticon-avatar"></i></a>
                                    </div>
                                </li>
                                <li>
                                    <div class="menu-cart d-none d-sm-block">
                                        <a class="shopping-cart-open" href="javascript:void(0)">
                                            <i class="flaticon-paper-bag"></i>
                                            <span class="shopping_bag_items_number"><?php echo esc_html(WC()->cart->get_cart_contents_count()); ?></span>
                                        </a>
                                    </div>
                                </li>
                                <li class="d-none d-lg-inline-block">
                                    <div class="menu-bar">
                                        <a class="canvas-menu-open" href="javascript:void(0)"><i class="flaticon-menu"></i></a>
                                    </div>
                                </li>
                                <li class="d-lg-none">
                                    <div class="menu-bar">
                                        <a class="mobile-menu-open" href="javascript:void(0)"><i class="flaticon-menu"></i></a>
                                    </div>
                                </li>
                            </ul>
                        </div> <!-- offset menu wrapper -->
                    </div>
                </div> <!-- row -->
            </div> <!-- header logo -->
        </div> <!-- container -->
    </div> <!-- header-logo-area -->


    <div class="header-menu-area">
        <div class="container position-relative">
            <div class="row">
                <div class="col-lg-3">
                    <div class="logo">
                        <a href="<?php echo esc_url( home_url( '/' ) ); ?>">
                            <?php
                            $custom_logo_id = get_theme_mod('custom_logo');
                            if($custom_logo_id) {
                                $custom_logo_image = wp_get_attachment_image_src($custom_logo_id, 'full');
                                ?>
                                <img src="<?php echo $custom_logo_image[0]; ?>" alt="Logo"/>
                                <?php
                            }else{
                                ?>
                                <img src="<?php echo get_template_directory_uri() . '/assets/images/logo.svg'; ?>" alt="Logo"/>
                                <?php
                            }
                            ?>
                        </a>
                    </div> <!-- logo -->
                </div>
                <div class="col-lg-6 static header-center-wrapper">
                    <?php get_template_part( 'template-parts/menu/menu', 'primary' ); ?>
                </div>
                <div class="col-lg-3">
                    <div class="offset-menu-wrapper text-right">
                        <ul>
                            <li>
                                <div class="menu-search d-none d-sm-block">
                                    <a class="search-open" href="javascript:void(0)"><i class="flaticon-search"></i></a>
                                </div>
                            </li>
                            <li>
                                <div class="menu-user d-none d-sm-block">
                                    <a class="login-form-open" href="javascript:void(0)"><i class="flaticon-avatar"></i></a>
                                </div>
                            </li>
                            <li>
                                <div class="menu-cart d-none d-sm-block">
                                    <a class="shopping-cart-open" href="javascript:void(0)">
                                        <i class="flaticon-paper-bag"></i>
                                        <span class="shopping_bag_items_number"><?php echo esc_html(WC()->cart->get_cart_contents_count()); ?></span>
                                    </a>
                                </div>
                            </li>
                            <li class="d-none d-lg-inline-block">
                                <div class="menu-bar">
                                    <a class="canvas-menu-open" href="javascript:void(0)"><i class="flaticon-menu"></i></a>
                                </div>
                            </li>
                            <li class="d-lg-none">
                                <div class="menu-bar">
                                    <a class="mobile-menu-open" href="javascript:void(0)"><i class="flaticon-menu"></i></a>
                                </div>
                            </li>
                        </ul>
                    </div> <!-- offset menu wrapper -->
                </div>
            </div> <!-- row -->
        </div> <!-- container -->
    </div> <!-- container -->
</header>

<!--====== HEADER PART ENDS ======-->
