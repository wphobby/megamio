<?php
/**
 * Header to show when style is default.
 *
 * @package    megamio
 * @author     wphobby
 * @link       https://wphobby.com
 */
?>
<!--====== HEADER PART START ======-->

<header class="header-area <?php echo $header_transparency_class; ?>">
    <div class="megamio-header-container position-relative container">
        <div class="row">
            <div class="col-lg-2">
                <div class="logo">
                    <a href="<?php echo esc_url( home_url( '/' ) ); ?>">
                        <?php
                        $custom_logo_id = get_theme_mod('custom_logo');
                        if($custom_logo_id) {
                            $custom_logo_image = wp_get_attachment_image_src($custom_logo_id, 'full');
                            ?>
                            <img src="<?php echo $custom_logo_image[0]; ?>" alt="Logo"/>
                            <?php
                        }else{
                            ?>
                            <img src="<?php echo get_template_directory_uri() . '/assets/images/logo.svg'; ?>" alt="Logo"/>
                            <?php
                        }
                        ?>
                    </a>
                </div> <!-- logo -->
            </div>
            <div class="col-lg-7 static">
                <?php get_template_part( 'template-parts/menu/menu', 'primary' ); ?>
            </div>
            <div class="col-lg-3">
                <div class="offset-menu-wrapper text-right">
                    <ul>
                        <li>
                            <div class="menu-search d-none d-sm-block">
                                <a class="search-open" href="javascript:void(0)"><i class="flaticon-search"></i></a>
                            </div>
                        </li>
                        <li>
                            <div class="menu-user d-none d-sm-block">
                                <?php if ( is_user_logged_in() ) : ?>
                                <a href="<?php echo esc_url( wc_get_account_endpoint_url( 'dashboard' ) ); ?>"><i class="flaticon-avatar"></i></a>
                                <ul class="user-dropdown-menu">
                                    <?php foreach ( wc_get_account_menu_items() as $endpoint => $label ) : ?>
                                        <li class="account-link--<?php echo esc_attr( $endpoint ); ?>">
                                            <a href="<?php echo esc_url( wc_get_account_endpoint_url( $endpoint ) ); ?>" class="underline-hover"><?php echo esc_html( $label ); ?></a>
                                        </li>
                                    <?php endforeach; ?>
                                </ul>
                                <?php else : ?>
                                <a class="login-form-open" href="javascript:void(0)"><i class="flaticon-avatar"></i></a>
                                <?php endif; ?>
                            </div>
                        </li>
                        <li>
                            <div class="menu-cart d-none d-sm-block">
                                <a class="shopping-cart-open" href="javascript:void(0)">
                                    <i class="flaticon-paper-bag"></i>
                                    <span class="shopping_bag_items_number"><?php echo esc_html(WC()->cart->get_cart_contents_count()); ?></span>
                                </a>
                            </div>
                        </li>
                        <li class="d-none d-lg-inline-block">
                            <div class="menu-bar">
                                <a class="canvas-menu-open" href="javascript:void(0)"><i class="flaticon-menu"></i></a>
                            </div>
                        </li>
                        <li class="d-lg-none">
                            <div class="menu-bar">
                                <a class="mobile-menu-open" href="javascript:void(0)"><i class="flaticon-menu"></i></a>
                            </div>
                        </li>
                    </ul>
                </div> <!-- offset menu wrapper -->
            </div>
        </div> <!-- row -->
    </div> <!-- container -->
</header>


<!--====== HEADER PART ENDS ======-->

