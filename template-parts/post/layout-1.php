<?php
/**
 * Template part for displaying blog layout 1 posts
 *
 * @link    https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package megamio
 */
?>

<div class="row justify-content-center">
    <div class="col-lg-10">
        <div class="single-blog-post mt-65">
            <div class="blog-image">
                <?php if( has_post_thumbnail() ):?>
                        <?php the_post_thumbnail('megamio-blog-list'); ?>
                <?php endif; ?>
            </div> <!-- blog image -->
            <div class="blog-content mt-35">
                <h2 class="blog-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
                <ul class="blog-date mt-10">
                    <li><a href="#"><?php echo get_the_author(); ?></a></li>
                    <li><a href="#"><?php echo get_the_time( get_option('date_format') ); ?></a></li>
                    <li><a href="#"><?php foreach((get_the_category()) as $category) { echo $category->cat_name . ' '; } ?></a></li>
                </ul>
                <p class="mt-25"><?php the_excerpt(); ?></p>

                <div class="blog-meta mt-35">
                    <div class="row align-items-center">
                        <div class="col-sm-4 col-6 order-1 order-sm-0">
                            <ul class="blog-social mt-10 text-left">
                                <li><a href="#"><i class="fa fa-facebook-f"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fa fa-pinterest-p"></i></a></li>
                            </ul>
                        </div>
                        <div class="col-sm-4 col-12 order-0 order-sm-0">
                            <div class="blog-more mt-10 text-left text-sm-center">
                                <a href="<?php the_permalink(); ?>">Read More</a>
                            </div>
                        </div>
                        <div class="col-sm-4 col-6 order-2 order-sm-2">
                            <ul class="blog-like mt-10 text-right">
                                <li><a href="#"><i class="fa fa-heart-o"></i> 30</a></li>
                                <li><a href="#"><i class="fa fa-comments"></i> 16</a></li>
                            </ul>
                        </div>
                    </div> <!-- row -->
                </div>
            </div> <!-- blog content -->
        </div> <!-- single Blog -->
    </div>
</div> <!-- row -->
