<?php
/**
 * Template part for displaying blog layout 2 posts
 *
 * @link    https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package megamio
 */
?>
<div class="col-md-6">
    <div class="news-one mt-30">
        <?php if( has_post_thumbnail() ):?>
        <div class="one-image">
            <?php the_post_thumbnail(array(450, 270, true)); ?>
        </div>
        <?php endif; ?>
        <div class="one-content mt-25">
            <h5><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h5>
            <p class="mt-10"><?php the_excerpt(); ?></p>
            <ul class="clearfix mt-25">
                <li class="date"><a href="#"><?php foreach((get_the_category()) as $category) { echo $category->cat_name . ' '; } ?> - <?php echo get_the_time( get_option('date_format') ); ?></a></li>
            </ul>
        </div>
    </div> <!-- news one -->
</div>