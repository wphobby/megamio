<?php
/**
 * Template part for displaying blog layout 5 posts
 *
 * @link    https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package megamio
 */
?>
<div class="single-blig mt-30">
    <?php if( has_post_thumbnail() ):?>
        <div class="blog-image">
            <?php the_post_thumbnail(array(370, 450)); ?>
        </div>
    <?php endif; ?>
    <div class="blog-content">
        <ul>
            <li><a href="#"><?php echo get_the_time( get_option('date_format') ); ?></a></li>
            <li><a href="#"><?php echo esc_html(' - in ','megamio'); the_category( ', ' ); ?></a></li>
        </ul>
        <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
        <?php the_excerpt(); ?>
        <a href="<?php the_permalink(); ?>">READ MORE</a>
    </div>
</div> <!-- single blog -->