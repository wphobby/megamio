<?php
/**
 * Template part for displaying blog layout 4 posts
 *
 * @link    https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package megamio
 */
?>
<div class="col-lg-3 col-md-6">
    <div class="news-fore mt-30">
        <?php if( has_post_thumbnail() ):?>
            <div class="news-fore-image">
                <?php the_post_thumbnail(array(303, 200, true)); ?>
                <span>journal</span>
            </div>
        <?php endif; ?>
        <div class="news-fore-content pt-25">
            <h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
            <p class="mt-10"><?php the_excerpt(); ?></p>
            <div class="author-name clearfix mt-20">
                <div class="name">
                    <ul>
                        <li><a href="#"><?php get_the_author(); ?></a></li>
                        <li><a href="#"><?php echo get_the_time( get_option('date_format') ); ?></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div> <!-- news fore -->
</div>
