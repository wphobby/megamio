<?php
/**
 * Displays Author bio on single post
 *
 * @package megamio
 */

$author_id         = get_the_author_meta( 'ID' );
$author_avatar     = get_avatar( $author_id, 'thumbnail' );
$author_post_link  = get_the_author_posts_link();
$author_bio        = get_the_author_meta( 'description' );
$author_url        = get_the_author_meta( 'user_url' );
$author_post_count = count_user_posts( $author_id );

?>
<div class="post-author media pt-30 pb-60">
    <div class="author-thumb pt-30">
      <?php echo $author_avatar; ?>
    </div>
    <div class="author-content media-body align-self-center pl-30 pt-25">
        <h6 class="author-title mb-10"><a href="<?php echo $author_url;?>">Donec libero</a></h6>
        <p class="mb-15"><?php echo $author_bio; ?></p>
        <a href="<?php echo $author_url;?>">READ MORE</a>
    </div>
</div> <!-- post author -->
