<?php
/**
 * Template part for displaying results in search pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package megamio
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <div class="news-three pt-15">
        <div class="row">
            <div class="col-md-5">
                <?php if( has_post_thumbnail() ):?>
                    <div class="news-three-image mt-15">
                        <?php the_post_thumbnail(array(303, 250, true)); ?>
                        <span>journal</span>
                    </div>
                <?php endif; ?>
            </div>
            <div class="col-md-7">
                <div class="news-three-content mt-15">
                    <ul>
                        <li><a href="#"><?php get_the_author(); ?></a></li>
                        <li><a href="#"><?php echo get_the_time( get_option('date_format') ); ?></a></li>
                    </ul>
                    <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                    <?php the_excerpt(); ?>
                    <div class="more-share clearfix mt-10">
                        <div class="more">
                            <a href="<?php the_permalink(); ?>">READ MORE</a>
                        </div>
                        <div class="share-like">
                            <ul>
                                <li><a href="#"><i class="flaticon-heart"></i> 12</a></li>
                                <li><a href="#"><i class="flaticon-comment-white-oval-bubble"></i> 12</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div> <!-- row -->
    </div> <!-- news three -->
</article><!-- #post-<?php the_ID(); ?> -->
