<?php
/**
 * Registering meta boxes
 *
 * @package megamio
 */

/**
 * Registering meta boxes
 *
 * @param array $meta_boxes Default meta boxes. By default, there are no meta boxes.
 *
 * @return array All registered meta boxes
 */
function megamio_register_meta_boxes( $meta_boxes ) {

	// Display Settings.
	$meta_boxes[] = array(
		'id'       => 'display-settings',
		'title'    => esc_html__( 'Display Settings', 'megamio' ),
		'pages'    => array( 'page' ),
		'context'  => 'normal',
		'priority' => 'high',
		'fields'   => array(
			array(
				'name' => esc_html__( 'Page Header', 'megamio' ),
				'id'   => 'heading_page_header',
				'type' => 'heading',
			),
			array(
				'name'    => esc_html__( 'Header Background', 'megamio' ),
				'id'      => 'header_background',
				'type'    => 'select',
				'options' => array(
					''            => esc_html__( 'Default', 'megamio' ),
					'light'       => esc_html__( 'Light', 'megamio' ),
					'transparent' => esc_html__( 'Transparent', 'megamio' ),
				),
			),
			array(
				'name'    => esc_html__( 'Page Title Display', 'megamio' ),
				'id'      => 'page_title_display',
				'type'    => 'select',
				'options' => array(
					''      => esc_attr__( 'Default', 'megamio' ),
					'none'  => esc_attr__( 'Hide Page Title', 'megamio' ),
				),
			),
			array(
				'name'    => esc_html__( 'Page Title Color', 'megamio' ),
				'id'      => 'page_title_color',
				'class'   => 'page-title-color hidden',
				'type'    => 'radio',
				'std'     => 'light',
				'options' => array(
					'dark'  => esc_attr__( 'Text Dark', 'megamio' ),
					'light' => esc_attr__( 'Text Light', 'megamio' ),
				),
			),
			array(
				'name'    => esc_html__( 'Page Header Height', 'megamio' ),
				'id'      => 'page_header_height',
				'type'    => 'select',
				'options' => array(
					''       => esc_attr__( 'Default', 'megamio' ),
					'full'   => esc_attr__( 'Full height', 'megamio' ),
					'manual' => esc_attr__( 'Manual', 'megamio' ),
				),
			),

			array(
				'name' => esc_html__( 'Layout', 'megamio' ),
				'id'   => 'heading_site_header',
				'type' => 'heading',
			),
			array(
				'name'    => esc_html__( 'Page Layout', 'megamio' ),
				'id'      => 'page_layout',
				'type'    => 'select',
				'options' => array(
					''            => esc_html__( 'Default', 'megamio' ),
					'boxed'        => esc_html__( 'Boxed', 'megamio' ),
					'full-width'       => esc_html__( 'Full Width', 'megamio' ),
				),
			),

		),
	);

	return $meta_boxes;
}

add_filter( 'rwmb_meta_boxes', 'megamio_register_meta_boxes' );

/**
 * Enqueue scripts for meta boxes.
 *
 * @param string $hook Admin page slugs.
 */
function megamio_meta_boxes_scripts( $hook ) {
	if ( in_array( $hook, array( 'post.php', 'post-new.php' ) ) ) {
		wp_enqueue_script( 'megamio-meta-boxes', get_template_directory_uri() . '/assets//js/admin/meta-boxes.js', array( 'jquery' ), '', true );
	}
}

add_action( 'admin_enqueue_scripts', 'megamio_meta_boxes_scripts' );
