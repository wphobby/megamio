<?php
/**
 * The megamio Theme Functions start here.
 *
 * @package    megamio
 * @author     wphobby
 * @link       https://wphobby.com
 */

/**
* Define Theme Options
*/
global $megamio_options;

/*
* Load kirki framework and customizer
*/
require get_template_directory() . '/inc/kirki/kirki.php';
require get_template_directory() . '/inc/customizer/kirki-customizer.php';

/**
* Setup Theme Functions.
* Enqueue styles, register widget regions, etc.
*/
require get_template_directory() . '/inc/functions/function-conditionals.php';
require get_template_directory() . '/inc/functions/function-setup.php';
require get_template_directory() . '/inc/functions/function-template.php';
require get_template_directory() . '/inc/functions/function-vc.php';


/**
 *TGM Plugin Activation
 */
require get_template_directory() . '/inc/tgm-plugin-activation/class-tgm-plugin-activation.php';
require get_template_directory() . '/inc/tgm-plugin-activation/required_plugins.php';

/**
 * Load custom menu
 */
require get_template_directory().'/inc/custom-menu/custom_menu.php';


/*
 * Include Demo Import
 */
require get_template_directory().'/inc/demo/demo-import.php';

/**
 * Megamio ShortCodes.
 */
require get_template_directory() . '/inc/shortcodes/slider.php';
require get_template_directory() . '/inc/shortcodes/featured_box.php';
require get_template_directory() . '/inc/shortcodes/new_arrivals.php';
require get_template_directory() . '/inc/shortcodes/banner.php';
require get_template_directory() . '/inc/shortcodes/banner_countdown.php';
require get_template_directory() . '/inc/shortcodes/brand.php';
require get_template_directory() . '/inc/shortcodes/newsletter.php';
require get_template_directory() . '/inc/shortcodes/hot_deal.php';
require get_template_directory() . '/inc/shortcodes/blog_posts.php';
require get_template_directory() . '/inc/shortcodes/wc_trending_items.php';
require get_template_directory() . '/inc/shortcodes/wc_product_categories_grid.php';

/**
 * Widget for showing recent post
 */
require get_template_directory() . '/inc/widgets/class-widget-newsletter.php';
require get_template_directory() . '/inc/widgets/class-widget-product-cat.php';
require get_template_directory() . '/inc/widgets/class-widget-product-tag.php';
require get_template_directory() . '/inc/widgets/class-widget-product-sort-by.php';
require get_template_directory() . '/inc/widgets/class-widget-product-price-filter.php';

/**
 * WooCommerce functions
 */
if ( is_woocommerce_activated() ) {
    require get_template_directory() . '/inc/woocommerce/wc-cart.php';
    require get_template_directory() . '/inc/woocommerce/wc-quick-view.php';
    require get_template_directory() . '/inc/woocommerce/wc-single-product.php';
    require get_template_directory() . '/inc/woocommerce/wc-template-hooks.php';
}

/**
 * Admin functions
 */
if(is_admin()){
    require get_template_directory() . '/inc/admin/meta-boxes.php';
}
?>
