<?php
/******************************************************************************/
/*************************** Visual Composer **********************************/
/******************************************************************************/

if (class_exists('WPBakeryVisualComposerAbstract')) {

    add_action( 'init', 'visual_composer_stuff' );
    function visual_composer_stuff() {

        //disable update
        // Vc_Manager::getInstance()->disableUpdater(true);


        //enable vc on post types
        if(function_exists('vc_set_default_editor_post_types')) vc_set_default_editor_post_types( array('post','page','product','portfolio') );

        // Modify and remove existing shortcodes from VC
        include_once(get_template_directory() . '/inc/shortcodes/visual-composer/custom_vc.php');


        // VC Templates
        $vc_templates_dir = get_template_directory() . '/inc/shortcodes/visual-composer/vc-templates/';
        vc_set_shortcodes_templates_dir($vc_templates_dir);

        // Add new shortcodes to VC
        include_once(get_template_directory() . '/inc/shortcodes/visual-composer/slider.php');
        include_once(get_template_directory() . '/inc/shortcodes/visual-composer/featured_box.php');
        include_once(get_template_directory() . '/inc/shortcodes/visual-composer/new_arrivals.php');
        include_once(get_template_directory() . '/inc/shortcodes/visual-composer/banner.php');
        include_once(get_template_directory() . '/inc/shortcodes/visual-composer/banner_countdown.php');
        include_once(get_template_directory() . '/inc/shortcodes/visual-composer/brand.php');
        include_once(get_template_directory() . '/inc/shortcodes/visual-composer/newsletter.php');
        include_once(get_template_directory() . '/inc/shortcodes/visual-composer/hot_deal.php');
        include_once(get_template_directory() . '/inc/shortcodes/visual-composer/blog_posts.php');
        include_once(get_template_directory() . '/inc/shortcodes/visual-composer/wc_trending_items.php');
        include_once(get_template_directory() . '/inc/shortcodes/visual-composer/wc_product_categories_grid.php');
    }

}

function megamio_vcSetAsTheme() {
    vc_manager()->disableUpdater(true);
    vc_set_as_theme();
}
add_action( 'vc_before_init', 'megamio_vcSetAsTheme' );

if (class_exists('WooCommerce')) {
   /**
   * Init
   */
   function megamio_vc_addons_init() {
      //woocommerce_before_shop_loop
      remove_action( 'woocommerce_before_shop_loop', 'woocommerce_result_count', 20 );
      remove_action( 'woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 30 );

      //Pagination.
      remove_action( 'woocommerce_after_shop_loop', 'woocommerce_pagination' );

      //woocommerce_after_shop_loop_item_title
      remove_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_price', 10 );
   }

   add_action( 'after_setup_theme', 'megamio_vc_addons_init', 20 );
}