<?php
function megamio_setup() {

    /* add woocommerce support */
    add_theme_support( 'woocommerce' );
    add_theme_support( 'wc-product-gallery-slider' );

    if ( megamio_theme_option( 'single_product_image_zoom' ) ) {
        add_theme_support( 'wc-product-gallery-zoom' );
    }

    if ( megamio_theme_option( 'single_product_image_lightbox' ) ) {
        add_theme_support('wc-product-gallery-lightbox');
    }

    add_theme_support( 'woocommerce', array(

        // Product grid theme settings
        'product_grid'        => array(
            'default_rows'    => get_option('woocommerce_catalog_rows', 5),
            'min_rows'        => 2,
            'max_rows'        => '',

            'default_columns' => get_option('woocommerce_catalog_columns', 5),
            'min_columns'     => 1,
            'max_columns'     => 6,
        ),
    ) );

    /* add title tag support */
    add_theme_support( 'title-tag' );

    /* load theme languages */
    load_theme_textdomain( 'megamio', get_template_directory() . '/languages' );

    /* Add default posts and comments RSS feed links to head */
    add_theme_support( 'automatic-feed-links' );

    /* Add excerpt to pages */
    add_post_type_support( 'page', 'excerpt' );

    /* Add support for post thumbnails */
    add_theme_support( 'post-thumbnails' );
    add_image_size( 'megamio-blog-list', 1170, 500, true);
    add_image_size( 'megamio-blog-list-medium', 370, 210, true);



    /* Add support for Selective Widget refresh */
    add_theme_support( 'customize-selective-refresh-widgets' );

    /** Add sensei support */
    add_theme_support( 'sensei' );

    /* Add support for HTML5 */
    add_theme_support( 'html5', array(
        'search-form',
        'comment-form',
        'comment-list',
        'gallery',
        'caption',
        'widgets',
    ) );

    /**
     * Add support for core custom logo.
     *
     * @link https://codex.wordpress.org/Theme_Logo
     */
    add_theme_support( 'custom-logo', array(
        'height'      => 250,
        'width'       => 250,
        'flex-width'  => true,
        'flex-height' => true,
    ) );

    // Set up the WordPress core custom background feature.
    add_theme_support( 'custom-background', apply_filters( 'megamio_custom_background_args', array(
        'default-color' => 'ffffff',
        'default-image' => '',
    ) ) );

    add_editor_style();

    /*  Register menus. */
    register_nav_menus( array(
        'primary' => __( 'Main Menu', 'megamio' ),
        'primary_mobile' => __( 'Main Menu - Mobile', 'megamio' ),
        'off_canvas' => __( 'Off Canvas Menu', 'megamio' ),
        'footer_bottom' => __( 'Footer Bottom Menu', 'megamio' ),
        'footer_section_one' => __( 'Footer Section One Menu', 'megamio' ),
        'footer_section_two' => __( 'Footer Section Two Menu', 'megamio' ),
        'footer_section_three' => __( 'Footer Section Three Menu', 'megamio' ),
    ) );
}
add_action( 'after_setup_theme', 'megamio_setup' );

/* Setup Theme Widgets */
function megamio_widgets_init() {

    //Sidebar Widget
    register_sidebar( array(
        'name'          => esc_html__( 'Sidebar', 'megamio' ),
        'id'            => 'sidebar-1',
        'description'   => esc_html__( 'Add widgets here.', 'megamio' ),
        'before_widget' => '<div class="col-lg-12 col-md-6"><section id="%1$s" class="widget %2$s">',
        'after_widget'  => '</section></div>',
        'before_title'  => '<h2 class="widget-title">',
        'after_title'   => '</h2>',
    ) );

    //Footer widget area 1
    register_sidebar( array(
        'name'          => esc_html__( 'Footer', 'megamio' ),
        'id'            => 'footer-sidebar',
        'description'   => esc_html__( 'Add widgets here to show on footer.', 'megamio' ),
        'before_widget' => '',
        'after_widget'  => '',
        'before_title'  => '',
        'after_title'   => '',
    ) );

    //catalog widget area
    register_sidebar( array(
        'name'          => __( 'Shop Sidebar', 'megamio' ),
        'id'            => 'catalog-widget-area',
        'before_widget' => '<div id="%1$s" class="widget mt-40 %2$s">',
        'after_widget'  => '</div>',
        'before_title'  => '<h4 class="widget-title">',
        'after_title'   => '</h4>',
    ) );

    //catalog widget area
    register_sidebar( array(
        'name'          => __( 'Catalog Filter', 'megamio' ),
        'id'            => 'catalog-filter',
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget'  => '</div>',
        'before_title'  => '<h4 class="widget-title">',
        'after_title'   => '</h4>',
    ) );
}
add_action( 'widgets_init', 'megamio_widgets_init' );


/**
 * Setup megamio Styles and Scripts
 */
function megamio_scripts() {
    $uri = get_template_directory_uri();
    $theme = wp_get_theme( get_template() );
    $version = $theme->get( 'Version' );

    // Enqueue theme styles  (Loaded on request)

    wp_enqueue_style('megamio-bootstrap', $uri . '/assets/css/bootstrap.min.css', NULL, $version, 'all' );
    wp_enqueue_style('megamio-font-awesome', $uri . '/assets/css/font-awesome.min.css', NULL, $version, 'all' );
    wp_enqueue_style('megamio-animate', $uri . '/assets/css/animate.css', NULL, $version, 'all' );
    wp_enqueue_style('megamio-slick', $uri . '/assets/css/slick.css', NULL, $version, 'all' );
    wp_enqueue_style('megamio-meanmenu', $uri . '/assets/css/meanmenu.css', NULL, $version, 'all' );
    wp_enqueue_style('megamio-jTippy', $uri . '/assets/css/jTippy.min.css', NULL, $version, 'all' );
    wp_enqueue_style('megamio-flaticon', $uri . '/assets/css/flaticon.css', NULL, $version, 'all' );
    wp_enqueue_style('megamio-default', $uri . '/assets/css/default.css', NULL, $version, 'all' );
    wp_enqueue_style('megamio-style', $uri . '/assets/css/style.css', NULL, $version, 'all' );
    wp_enqueue_style('megamio-responsive', $uri . '/assets/css/responsive.css', NULL, $version, 'all' );


    // Enqueue theme scripts
    wp_enqueue_script( 'megamio-modernizr', $uri .'/assets/js/vendor/modernizr-3.6.0.min.js', array('jquery'), '3.6.0', false );
    wp_enqueue_script( 'megamio-bootstrap-js', $uri .'/assets/js/bootstrap.min.js', array('jquery'), '4.2.1', false );
    wp_enqueue_script( 'megamio-slick-js', $uri .'/assets/js/slick.min.js', array('jquery'), '4.2.1', false );
    wp_enqueue_script( 'megamio-isotope-js', $uri .'/assets/js/isotope.pkgd.min.js', array('imagesloaded'), '3.0.6', false );
    wp_enqueue_script( 'megamio-imagesloaded-js', $uri .'/assets/js/imagesloaded.pkgd.min.js', array('jquery'), '4.1.4', false );
    wp_enqueue_script( 'megamio-meanmenu-js', $uri .'/assets/js/jquery.meanmenu.min.js', array('jquery'), '', false );
    wp_enqueue_script( 'megamio-jTippy-js', $uri .'/assets/js/jTippy.min.js', array('jquery'), '', false );
    wp_enqueue_script( 'megamio-elevateZoom-js', $uri .'/assets/js/jquery.elevateZoom.min.js', array('jquery'), '', false );
    wp_enqueue_script( 'megamio-nice-select-js', $uri .'/assets/js/jquery.nice-select.min.js', array('jquery'), '', false );
    wp_enqueue_script( 'megamio-nice-number-js', $uri .'/assets/js/jquery.nice-number.min.js', array('jquery'), '', false );
    wp_enqueue_script( 'megamio-countdown-js', $uri .'/assets/js/jquery.countdown.min.js', array('jquery'), '', false );
    wp_enqueue_script( 'megamio-jquery-ui-js', $uri .'/assets/js/jquery-ui.min.js', array('jquery'), '', false );
    wp_enqueue_script( 'megamio-main-js', $uri .'/assets/js/main.js', array('jquery'), '', false );


    if ( wp_script_is( 'flexslider', 'registered' ) ) {
        wp_enqueue_script( 'flexslider' );
    }

    if(is_woocommerce_activated()){
        wp_enqueue_script( 'megamio-wc-quick-view-js', $uri .'/assets/js/woocommerce/wc-quick-view.js', array('jquery'), '', false );
        wp_enqueue_script( 'megamio-wc-load-products-js', $uri .'/assets/js/woocommerce/wc-load-products.js', array('jquery'), '', false );
        wp_enqueue_script( 'megamio-wc-notification-js', $uri .'/assets/js/woocommerce/wc-notifications.js', array('jquery'), '', false );
        wp_enqueue_script( 'megamio-wc-login-authenticate-js', $uri .'/assets/js/woocommerce/wc-login-authenticate.js', array('jquery'), '', false );
        wp_enqueue_script( 'megamio-wc-login-form-js', $uri .'/assets/js/woocommerce/wc-login-form.js', array('jquery'), '', false );
        wp_enqueue_script( 'megamio-wc-single-product-js', $uri .'/assets/js/woocommerce/wc-single-product.js', array('jquery'), '', false );

    }

    if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
        wp_enqueue_script( 'comment-reply' );
    }

}
add_action( 'wp_enqueue_scripts', 'megamio_scripts', 100 );

/**
 * Define Ajax Url
 */

add_action('wp_head','megamio_ajaxurl');
function megamio_ajaxurl() {

    $ajax_url = admin_url('admin-ajax.php', 'relative');
    if ( class_exists('SitePress') ) {
        $my_current_lang = apply_filters( 'wpml_current_language', NULL );
        if ( $my_current_lang ) {
            $ajax_url = add_query_arg( 'wpml_lang', $my_current_lang, $ajax_url );
        }}
    ?>
    <script type="text/javascript">
        var megamio_ajaxurl = '<?php echo $ajax_url; ?>';
    </script>
    <?php
}

/**
 * Load theme options
 */
function megamio_customizer_styles(){

    global $megamio_options;

    /* Header Style */
    $megamio_options['header_style'] 							    = megamio_theme_option('header_style', 'header-default');
    $megamio_options['header_transparent']                          = megamio_theme_option('header_transparent', true);


    /* Social Media */
    $megamio_options['social_icons'] 							    = megamio_theme_option('social_icons_lists', array());


    /* Blog Layout */
    $megamio_options['layout_blog'] 							    = megamio_theme_option('layout_blog', 'layout-1');
    $megamio_options['enable_blog_sidebar'] 					    = megamio_theme_option('enable_blog_sidebar', 1);

    /* Single Post */
    $megamio_options['single_post_layout'] 					        = megamio_theme_option('single_post_layout', 'full-width');
    $megamio_options['single_enable_author_box'] 					= megamio_theme_option('single_enable_author_box', true);

    /* Breadcrumbs */
    $megamio_options['enable_breadcrumbs'] 						    = megamio_theme_option('enable_breadcrumbs', 1);

    /* Footer */
    $megamio_options['footer_style'] 							    = megamio_theme_option('footer_style', 'site-footer-default');
    $megamio_options['enable_scroll_to_top']                        = megamio_theme_option('enable_scroll_to_top', true);

    /* Post Slider */
    $megamio_options['enabled_post_slider']                         = megamio_theme_option('enabled_post_slider', true);

    /* Shop Page */
    $megamio_options['display_shop_sidebar']                        = megamio_theme_option('display_shop_sidebar', true);

    /* Single Product */
    $megamio_options['single_product_socials'] 					    = megamio_theme_option('single_product_socials', array());
    $megamio_options['single_product_layout'] 					    = megamio_theme_option('single_product_layout', '1');

    /* Product Columns */
    $megamio_options['vc_product_columns'] 					        = megamio_theme_option('vc_product_columns', '4');


}
add_action( 'wp_loaded', 'megamio_customizer_styles', 99 );

// Admin area
function megamio_admin_styles() {
    $uri = get_template_directory_uri();
    $theme = wp_get_theme(get_template());
    $version = $theme->get( 'Version' );

    if ( is_admin() ) {

        if (class_exists('WPBakeryVisualComposerAbstract')) {
            wp_enqueue_style('megamio_visual_composer', $uri.'/assets/css/admin/visual-composer.css', NULL, $version, 'all');
        }
    }
}
add_action( 'admin_enqueue_scripts', 'megamio_admin_styles' );


/**
 * Get theme options from theme settings
 */
if ( ! function_exists ('megamio_theme_option') ) {
    function megamio_theme_option( $name, $default = "" ) {
        return get_theme_mod( $name, $default );
    } //function
}

/**
 * Set the content width based on the theme's design and stylesheet.
 */
if ( ! isset( $content_width ) ) {
    $content_width = 1200;
}

/**
 * Authenticate a user, confirming the login credentials if valid.
 */
function megamio_login_authenticate() {
    $creds = $_POST['creds'];

    $user = wp_signon( $creds );

    if ( is_wp_error( $user ) ) {
        wp_send_json_error( $user->get_error_message() );
    } else {
        wp_send_json_success();
    }
}
add_action( 'wp_ajax_nopriv_megamio_login_authenticate', 'megamio_login_authenticate' );