<?php
/**
 * Hooking Wordpress Theme Functions
 *
 * @package megamio
 */

/**
 * Hooking pre loader
 *
 */
function megamio_add_pre_loader() {
    // Show preloader if enabled.
    if ( true === get_theme_mod( 'enabled_pre_loader') ) :
        get_template_part( 'template-parts/loader/pre', 'loader' );
    endif;
}

add_action( 'megamio_before_header', 'megamio_add_pre_loader', 1 );

/**
 * Adds the necessary template to load on footer.
 *
 */
function megamio_add_footer_extras() {


    // Load mobile menu.
    get_template_part( 'template-parts/menu/menu', 'mobile' );

    // Load search form.
    get_template_part( 'template-parts/footer/search', 'form' );

    // Load Woocommerce Mini Cart.
    if(function_exists('woocommerce_mini_cart')){
        woocommerce_mini_cart();
    }

    // Load off canvas menu.
    get_template_part( 'template-parts/menu/menu', 'offcanvas' );

    // Load login form.
    get_template_part( 'template-parts/footer/login', 'form' );

    // Show back to top if enabled.
    ?>
        <a href="#" class="back-to-top"><i class="fa fa-angle-up"></i></a>
        <?php

}
add_action( 'megamio_after_footer', 'megamio_add_footer_extras' );

/**
 * Filter to Change the Length of Excerpts
 *
 */
function megamio_excerpt_length( $length ) {
    // Don't change anything inside admin
    if ( is_admin() ) {
        return $length;
    }
    // Set excerpt length to 20 words
    return 50;
}
// "999" priority makes this run last of all the functions hooked to this filter, meaning it overrides them
add_filter( 'excerpt_length', 'megamio_excerpt_length', 999 );

/**
 *
 * Display navigation to next/previous pages when applicable
 */
function megamio_content_nav(){
    global $wp_query, $post;

    // Don't print empty markup on single pages if there's nowhere to navigate.
    if ( is_single() ) {
        $previous = ( is_attachment() ) ? get_post( $post->post_parent ) : get_adjacent_post( false, '', true );
        $next = get_adjacent_post( false, '', false );

        if ( ! $next && ! $previous )
            return;
    }

    if ( is_single() ) : // navigation links for single posts ?>
        <div class="blog-next-prev pb-40 mt-45 d-flex justify-content-between">
            <div class="prev-post">
                <?php previous_post_link( '%link', ' <i class="flaticon-left-arrow-1"></i>'.__( "Older", "megamio" ).'<p> %title </p>' ); ?>
            </div>
            <div class="next-post text-right">
                <?php next_post_link( '%link', __( "Newer", "megamio" ) .'<i class="flaticon-right-arrow-1"></i>  <p> %title </p>' ); ?>
            </div>
        </div> <!-- row -->
<?php endif;
}

/**
 *
 * List post comments
 */
function megamio_comment( $comment, $args, $depth ) {
    $GLOBALS['comment'] = $comment;
?>

    <li id="comment-<?php comment_ID(); ?>" <?php if( $depth > 1 ) { comment_class('comment-replay'); } ?>>

            <div class="single-comment">

                <div class="comment-thumb">
                    <?php echo get_avatar( $comment, 70 ); ?>
                </div><!-- .comment-image -->

                <div class="comment-content">
                    <?php printf( __( '%s', 'megamio' ), sprintf( '<h6 class="comment-name">%s</h6>', get_comment_author_link() ) ); ?>
                    <sapn>
                        <time datetime="<?php comment_time( 'c' ); ?>">
                            <?php printf( __( '- %1$s at %2$s', 'megamio' ), get_comment_date(), get_comment_time() ); ?>
                        </time>
                    </sapn>
                    <p class="mt-10"><?php comment_text(); ?></p>
                    <?php
                    comment_reply_link( array_merge( $args, array(
                        'add_below' => 'div-comment',
                        'depth'     => $depth,
                        'max_depth' => $args['max_depth'],
                        'before'    => '',
                        'after'     => '',
                    ) ) );
                    ?>
                </div><!-- .comment-content -->
            </div><!-- .single-comment -->
    </li>
    <?php
}

function megamio_move_comment_field_to_bottom( $fields ) {
    $comment_field = $fields['comment'];
    unset( $fields['comment'] );
    $fields['comment'] = $comment_field;
    return $fields;
}

add_filter( 'comment_form_fields', 'megamio_move_comment_field_to_bottom' );

function megamio_comment_form_hide_cookies_consent( $fields ) {
    unset( $fields['cookies'] );
    return $fields;
}
add_filter( 'comment_form_fields', 'megamio_comment_form_hide_cookies_consent' );

// define the comment_form_submit_button callback
function filter_comment_form_submit_button( $submit_button, $args ) {
    // make filter magic happen here...
    $submit_before = '<div class="row"><div class="col-md-12"><div class="single-comment-form">';
    $submit_after = '</button></div></div></div>';
    $submit_button = str_replace('class="submit"','class="main-btn main-btn-2"', $submit_button);
    return $submit_before . $submit_button . $submit_after;
};

// add the filter
add_filter( 'comment_form_submit_button', 'filter_comment_form_submit_button', 10, 2 );

/* Tagcloud, change the font size */
function megamio_custom_tag_cloud_widget($args) {
    $args['largest'] = 10; //largest tag
    $args['smallest'] = 10; //smallest tag
    $args['unit'] = 'px'; //tag font unit
    return $args;
}
add_filter( 'widget_tag_cloud_args', 'megamio_custom_tag_cloud_widget', 999);

/*Hide categories from WordPress category widget*/
function megamio_exclude_widget_categories($args){
    $exclude = "1,8,14,10,15,24,17";
    $args["exclude"] = $exclude;
    return $args;
}
add_filter("widget_categories_args","megamio_exclude_widget_categories");

/* Set post count for per page */
function megamio_set_posts_per_page( $query ) {
    $count = 0;
    //Get layout_blog
    $layout_blog = 'layout-1';
    if(isset($megamio_options['layout_blog'])){
        $layout_blog = $megamio_options['layout_blog'];
    }
    if(isset($_GET['layout_blog'])){
        $layout_blog = $_GET['layout_blog'];
    }
    switch ($layout_blog){
        case "layout-3":
            $count = 9;
            break;
        case "layout-4":
            $count = 8;
            break;
    }
    if ( !is_admin() && $query->is_main_query() && $count > 0 ) {
        $query->set( 'posts_per_page', $count );
    }
}
add_action( 'pre_get_posts', 'megamio_set_posts_per_page' );

/**
 * Change the content container class for different page layout.
 *
 * @param string $class Container class.
 *
 * @return string
 */
function content_container_class( $class ) {
    if('full-width' == get_post_meta( get_the_ID(), 'page_layout', true )){
        return '';
    }elseif( is_account_page() || is_product() ){
        return 'container';
    }
    return $class;
}
add_filter( 'megamio_content_container_class', 'content_container_class');

/* Show product loop columns class */
function megamio_loop_columns_class() {

    $products_per_column = get_option('woocommerce_catalog_columns', 4);

    if (isset($_GET["products_per_row"])) {
       $products_per_column = $_GET["products_per_row"];
    }

    switch ( $products_per_column ){
        case 2:
            $column_class = 'col-md-6';
            break;
        case 3:
            $column_class = 'col-lg-4 col-md-6';
            break;
        case 4:
            $column_class = 'col-lg-3 col-md-6';
            break;
        case 5:
            $column_class = 'col-md-1-5 col-md-6';
            break;
        case 6:
            $column_class = 'col-lg-2 col-md-6';
            break;
    }

    echo $column_class;
}
?>