<?php

/******************************************************************************/
/* WooCommerce Template Hooks *********************************************/
/******************************************************************************/

if ( is_woocommerce_activated() ) {

	if ( in_array( megamio_theme_option( 'single_product_layout' ), array( '2', '3', '4', '5', '6' ) ) ) {
		add_filter( 'woocommerce_single_product_flexslider_enabled', '__return_false' );
		add_filter( 'woocommerce_single_product_zoom_enabled', '__return_false' );
	}

	if ( in_array( megamio_theme_option( 'single_product_layout' ), array( '2', '3', '4', '5', '6' ) ) ) {
		add_filter('woocommerce_gallery_image_size', 'gallery_image_size');
	}

	/**
	 * Product Image Size.
	 */
	function gallery_image_size() {
		return 'woocommerce_single';
	}


	// Add image to empty cart message.
	add_filter( 'wc_empty_cart_message', 'megamio_empty_cart_message' );

	/**
	 * Display cart empty image.
	 */
	function megamio_empty_cart_message( $message ) {
		$message = '<img src="' . esc_url( get_theme_file_uri( 'assets/images/shopping-bag.svg' ) ) . '" width="150" alt="' . esc_attr__( 'Shopping Cart is empty', 'megamio' ) . '">' . $message;

		return $message;
	}

	if ( megamio_theme_option( 'shop_page_header' ) ) {
		add_action( 'megamio_after_header', 'megamio_page_header');
		add_action( 'woocommerce_archive_description', 'woocommerce_breadcrumb', 20 );
	}

	/**
	 * Products page header section.
	 */
	function megamio_page_header() {
		if ( ! is_shop() && ! is_product_taxonomy() ) {
			return;
		}

	?>
		<div id="page-title-bar" class="page-title-bar">
			<div class="container">
				<div class="wrap w-100 d-flex align-items-center">
					<div class="page-title-bar-inner d-flex flex-column align-items-center w-100">
						<div class="breadcrumb">
							<?php if (function_exists('bcn_display')): ?>
								<?php bcn_display(); ?>
							<?php else: ?>
								<?php the_title( '<h4 class="entry-title">', '</h4>' ); ?>
							<?php endif; ?>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php
	}

}