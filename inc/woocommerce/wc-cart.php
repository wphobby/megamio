<?php
//================================================================================
// Update local storage with cart counter each time
//================================================================================

add_filter('woocommerce_add_to_cart_fragments', 'megamio_shopping_bag_items_number');
function megamio_shopping_bag_items_number( $fragments )
{
global $woocommerce;
ob_start(); ?>
    <span class="shopping_bag_items_number"><?php echo esc_html(WC()->cart->get_cart_contents_count()); ?></span>
<?php
$fragments['.shopping_bag_items_number'] = ob_get_clean();
return $fragments;
}


add_filter('woocommerce_add_to_cart_fragments', 'megamio_mini_cart');
function megamio_mini_cart( $fragments )
{

    global $woocommerce;
    ob_start(); ?>
    <script type="text/javascript">
        jQuery(function($) {
            $('.shopping-cart-close').on('click', function(){
                $('.shopping-cart-canvas').removeClass('open')
                $('.overlay').removeClass('open')
            });
        });
    </script>
        <?php woocommerce_mini_cart(); ?>
    <?php
    $fragments['.shopping-cart-canvas'] = ob_get_clean();
    return $fragments;
}
