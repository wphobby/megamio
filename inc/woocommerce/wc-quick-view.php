<?php

/******************************************************************************/
/* WooCommerce Product Quick View *********************************************/
/******************************************************************************/

if ( is_woocommerce_activated() ) {

	// Enqueue wc-add-to-cart-variation

	function megamio_product_quick_view_scripts() {	
		wp_enqueue_script('wc-add-to-cart-variation');
	}
	add_action( 'wp_enqueue_scripts', 'megamio_product_quick_view_scripts' );

	
	// Load The Product

	function megamio_product_quick_view_fn() {		
		if (!isset( $_REQUEST['product_id'])) {
			die();
		}

		if( isset($_GET['wpml_lang']) )	{
			do_action( 'wpml_switch_language',  $_GET[ 'wpml_lang' ] );
		}

		$product_id = intval($_REQUEST['product_id']);
		// wp_query for the product
		wp('p='.$product_id.'&post_type=product');
		ob_start();
		get_template_part( 'woocommerce/quick-view' );
		echo ob_get_clean();
		die();
	}
	add_action( 'wp_ajax_megamio_product_quick_view', 'megamio_product_quick_view_fn');
	add_action( 'wp_ajax_nopriv_megamio_product_quick_view', 'megamio_product_quick_view_fn');

	
	// Show Quick View Button
	function megamio_product_quick_view_button() {
		global $product;
		echo '<a href="#" id="product_id_' . $product->get_id() . '" class="megamio_product_quick_view_button" data-toggle="modal" data-target="#exampleModalCenter" data-tool="tooltip" data-product_id="'.$product->get_id().'" title="Quick View"><i class="flaticon-eye"></i></a>';
	}
	add_action( 'woocommerce_after_shop_loop_item_title', 'megamio_product_quick_view_button', 5 );
	
}