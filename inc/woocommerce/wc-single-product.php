<?php

/******************************************************************************/
/* WooCommerce Single Product *********************************************/
/******************************************************************************/

if ( is_woocommerce_activated() ) {

	//Display single product toolbar
	function single_product_toolbar()
	{
		$args = array(
			'delimiter' => '<span class="circle"></span>',
		);
		?>

		<div class="product-toolbar clearfix pt-100">
			<?php

			woocommerce_breadcrumb($args);

			the_post_navigation(
				array(
					'screen_reader_text' => esc_html__('Product navigation', 'megamio'),
					'prev_text' => _x('<i class="fa fa-angle-left"></i><span class="screen-reader-text">%title</span>', 'Previous post link', 'megamio'),
					'next_text' => _x('<span class="screen-reader-text">%title</span><i class="fa fa-angle-right"></i>', 'Next post link', 'megamio'),
				)
			);

			?>
		</div>

		<?php
	}

	add_action('woocommerce_before_main_content', 'single_product_toolbar', 5);

	//==============================================================================
    //	Custom WooCommerce related products
    //==============================================================================
	function megamio_output_related() {
		   echo '<section class="related-products mt-90">
	            <div class="container">';
				$atts = array(
					'columns'		 => 4,
					'posts_per_page' => 4,
					'orderby'        => 'rand'
				);
				woocommerce_related_products($atts); // Display 4 products in rows of 4
				echo '</div> <!-- container --></section>';
	}
}