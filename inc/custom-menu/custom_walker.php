<?php
/**
 * Custom Walker
 *
 * @access      public
 * @since       1.0 
 * @return      void
*/
class Megamio_Nav_Walker extends Walker_Nav_Menu
{
	// Displays start of a level. E.g '<ul>'
	// @see Walker::start_lvl()
	function start_lvl(&$output, $depth=0, $args=array()) {
			$output .= '';
	}

	// Displays end of a level. E.g '</ul>'
	// @see Walker::end_lvl()
	function end_lvl(&$output, $depth=0, $args=array()) {
            $output .= '';
	}

	// Displays start of an element. E.g '<li> Item Name'
	// @see Walker::start_el()
	function start_el(&$output, $item, $depth=0, $args=array(), $id = 0) {

		if($depth == 0){
			if($args->walker->has_children){
				$output .= '<li class="menu-item-has-children"><a href="#" class="dw-menu">'.$item->title.'</a>
                        <ul class="sub-menu">';
			}else{
				$output .= '<li><a href="'.$item->url.'" class="dw-menu">'.$item->title.'</a>
                        <ul>';
			}


		}

		if($depth == 1){
             $output .= '<li><a href="'.$item->url.'">'.$item->title.'</a>';
		}

	}

	// Displays end of an element. E.g '</li>'
	// @see Walker::end_el()
	function end_el(&$output, $item, $depth=0, $args=array()) {
		if($depth == 0){
			$output .= "</ul></li>";
		}

		if($depth == 1){
            $output .= '</li>';
		}

	}
}

class Megamio_Primary_Nav_Walker extends Walker_Nav_Menu
{
	// Displays start of a level. E.g '<ul>'
	// @see Walker::start_lvl()
	function start_lvl(&$output, $depth=0, $args=array()) {
		$output .= '';
	}

	// Displays end of a level. E.g '</ul>'
	// @see Walker::end_lvl()
	function end_lvl(&$output, $depth=0, $args=array()) {
		if($depth == 0){
			$output .= "</ul></li>";
		}
		if($depth == 1){
			$output .= "</ul></li>";
		}
		if($depth == 2){
			$output .= "</li>";
		}
	}

	// Displays start of an element. E.g '<li> Item Name'
	// @see Walker::start_el()
	function start_el(&$output, $item, $depth=0, $args=array(), $id = 0) {

		if($depth == 0){
			if( $item->megamenu == 'enabled' ){
				$output .= '<li class="static"><a href="'.$item->url.'">'.$item->title.'</a>
                        <ul class="megamenu clearfix">';
			}
			else if($args->walker->has_children){
				$output .= '<li><a href="'.$item->url.'">'.$item->title.'</a>
                        <ul class="dropdown">';
			}else{
				$output .= '<li><a href="'.$item->url.'">'.$item->title.'</a>';
			}
		}

		if($depth == 1){
			if(!empty($item->background_url)){
				$output .= '<li class="mega-banner d-none d-lg-block"><a href="'.$item->url.'"><img src="'.$item->background_url.'" alt=""></a>';
			}
			else if($args->walker->has_children){
				$output .= '<li class="mega-title"><a href="'.$item->url.'">'.$item->title.'</a>
                        <ul>';
			}else{
				$output .= '<li><a href="'.$item->url.'">'.$item->title.'</a>';
			}
		}

		if($depth == 2){
			$output .= '<li><a href="'.$item->url.'">'.$item->title.'</a>';
		}

	}

	// Displays end of an element. E.g '</li>'
	// @see Walker::end_el()
	function end_el(&$output, $item, $depth=0, $args=array()) {

		if($depth == 1){
			$output .= '</li>';
		}

	}
}