<?php
/**
 * Kirki Customizer Options File
 *
 * @package megamio
 */

/**
 * Configuration for Kirki Framework
 */
function megamio_kirki_configuration() {
	return array(
		'url_path' => get_template_directory_uri() . '/inc/kirki/',
	);
}

add_filter( 'kirki/config', 'megamio_kirki_configuration' );

/**
 * megamio Kirki Config
 */
Kirki::add_config( 'megamio_config', array(
	'capability'  => 'edit_theme_options',
	'option_type' => 'theme_mod',
) );

/**
 * megamio Kirki Theme Options Panel
 */
Kirki::add_panel( 'megamio_theme_options', array(
	'priority' => 10,
	'title'    => esc_html__( 'Theme Options', 'megamio' ),
) );


/**
 * megamio Header Options section
 */
Kirki::add_section( 'megamio_section_header', array(
	'title'    => esc_html__( 'Header', 'megamio' ),
	'panel'    => 'megamio_theme_options',
	'priority' => 25,
) );

/**
 * megamio Layout Options section
 */
Kirki::add_section( 'megamio_section_layout', array(
	'title'    => esc_html__( 'Layout', 'megamio' ),
	'panel'    => 'megamio_theme_options',
	'priority' => 25,
) );


/**
 * megamio Footer Options section
 */
Kirki::add_section( 'megamio_section_footer', array(
	'title'    => esc_html__( 'Footer', 'megamio' ),
	'panel'    => 'megamio_theme_options',
	'priority' => 25,
) );

/**
 * megamio Preloader Options section
 */
Kirki::add_section( 'megamio_section_loader', array(
	'title'    => esc_html__( 'Loader', 'megamio' ),
	'panel'    => 'megamio_theme_options',
	'priority' => 25,
) );

/**
 * Shop Options section
 */
Kirki::add_section( 'megamio_section_shop', array(
	'title'    => esc_html__( 'Shop', 'megamio' ),
	'panel'    => 'megamio_theme_options',
	'priority' => 25,
) );


/**
 * megamio Post Slider Options section
 */
Kirki::add_section( 'megamio_section_post_slider', array(
	'title'    => esc_html__( 'Post Slider', 'megamio' ),
	'panel'    => 'megamio_theme_options',
	'priority' => 25,
) );

/**
 * megamio Breadcrumb Options section
 */
Kirki::add_section( 'megamio_section_breadcrumb', array(
	'title'    => esc_html__( 'Breadcrumb', 'megamio' ),
	'panel'    => 'megamio_theme_options',
	'priority' => 25,
) );

/**
 * Single Post Options section
 */
Kirki::add_section( 'megamio_section_single', array(
	'title'    => esc_html__( 'Single Post', 'megamio' ),
	'panel'    => 'megamio_theme_options',
	'priority' => 25,
) );

/**
 * Social Options section
 */
Kirki::add_section( 'megamio_section_social', array(
	'title'    => esc_html__( 'Social', 'megamio' ),
	'panel'    => 'megamio_theme_options',
	'priority' => 25,
) );

/**
 * Color Options section
 */
Kirki::add_section( 'megamio_section_color', array(
	'title'    => esc_html__( 'Color', 'megamio' ),
	'panel'    => 'megamio_theme_options',
	'priority' => 25,
) );

/**
 * Color Options section
 */
Kirki::add_section( 'megamio_section_single_product', array(
	'title'    => esc_html__( 'Single Product', 'megamio' ),
	'panel'    => 'woocommerce',
	'priority' => 25,
) );





/**
 * Add the required kirki customizer options files
 */

require get_template_directory() . '/inc/customizer/options/options-layout.php';
require get_template_directory() . '/inc/customizer/options/options-header.php';
require get_template_directory() . '/inc/customizer/options/options-footer.php';
require get_template_directory() . '/inc/customizer/options/options-social.php';
require get_template_directory() . '/inc/customizer/options/options-color.php';
require get_template_directory() . '/inc/customizer/options/options-single.php';
require get_template_directory() . '/inc/customizer/options/options-post-slider.php';
require get_template_directory() . '/inc/customizer/options/options-loader.php';
require get_template_directory() . '/inc/customizer/options/options-shop.php';
require get_template_directory() . '/inc/customizer/options/options-breadcrumb.php';
require get_template_directory() . '/inc/customizer/options/options-single-product.php';