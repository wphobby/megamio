<?php
/**
 * Customizer options for Pre Loader
 *
 * @package megamio
 */

// Enable/Disable Shop Sidebar
Kirki::add_field(
    'megamio_config', array(
        'type'     => 'toggle',
        'settings' => 'display_shop_sidebar',
        'label'    => esc_html__( 'Display Sidebar on Shop Page', 'megamio' ),
        'section'  => 'megamio_section_shop',
        'default'  => true,
    )
);

// Enable/Disable Page Header
Kirki::add_field(
    'megamio_config', array(
        'type'     => 'radio',
        'settings' => 'shop_page_header',
        'label'   => esc_html__( 'Shop Page Header Style', 'megamio' ),
        'section'  => 'megamio_section_shop',
        'default' => 'minimal',
        'choices' => array(
            ''         => esc_attr__( 'Not display page header', 'megamio' ),
            'minimal'  => esc_attr__( 'Minimal Style (text only)', 'megamio' ),
        ),
    )
);