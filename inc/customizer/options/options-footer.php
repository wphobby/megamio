<?php
/**
 * Customizer options for footer
 *
 * @package megamio
 */

// Enable/Disable footer Top Bar.
Kirki::add_field(
    'megamio_config', array(
        'type'     => 'toggle',
        'settings' => 'enable_scroll_to_top',
        'label'    => esc_html__( 'Enable Scroll To Top', 'megamio' ),
        'section'  => 'megamio_section_footer',
        'default'  => true,
    )
);

// Select footer style.
Kirki::add_field(
    'megamio_config', array(
        'type'     => 'radio-image',
        'settings' => 'footer_style',
        'label'    => esc_html__( 'Footer Layout', 'megamio' ),
        'section'  => 'megamio_section_footer',
        'default'  => 'site-footer-default',
        'choices'  => array(
            'site-footer-default'        => get_template_directory_uri() . '/assets/images/footer/footer-center.png',
            'site-footer-center'     => get_template_directory_uri() . '/assets/images/footer/footer-left.png',
        ),
    )
);
