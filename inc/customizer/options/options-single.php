<?php
/**
 * Customizer options for single post
 *
 * @package megamio
 */

/* Single Post Layout */
Kirki::add_field(
	'megamio_config', array(
		'type'     => 'radio-image',
		'settings' => 'single_post_layout',
		'label'    => esc_html__( 'Single Post Layout', 'megamio' ),
		'section'  => 'megamio_section_single',
		'default'  => 'full-width',
		'choices'     => array(
			'full-width'        => get_template_directory_uri() . '/assets/images/header/header-center.png',
			'right-sidebar'     => get_template_directory_uri() . '/assets/images/header/header-left.png'
		),
	)
);

// Enable/Disable Author Box.
Kirki::add_field(
	'megamio_config', array(
		'type'     => 'toggle',
		'settings' => 'single_enable_author_box',
		'label'    => esc_html__( 'Enable Author Box', 'megamio' ),
		'section'  => 'megamio_section_single',
		'default'  => true,
	)
);