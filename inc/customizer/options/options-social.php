<?php
/**
 * Social Media Options
 *
 * @package megamio
 */

// Option to add the social links via repeater field.
Kirki::add_field( 'megamio_config', array(
	'type'        => 'repeater',
	'label'       => esc_html__( 'Add Social Profile', 'megamio' ),
	'description' => esc_html__( 'Drag & Drop items to re-arrange the order', 'megamio' ),
	'section'     => 'megamio_section_social',
	'settings'    => 'social_icons_lists',
	'row_label'   => array(
		'type'  => 'field',
		'value' => esc_html__( 'Social Profile', 'megamio' ),
		'field' => 'social_icon',
	),
	'fields'      => array(
		'social_icon' => array(
			'label'   => esc_html__( 'Social Icon', 'megamio' ),
			'type'    => 'select',
			'default' => 'flaticon-google-plus-logo',
			'choices' => array(
				'flaticon-google-plus-logo'     => esc_html__( 'Google Plus', 'megamio' ),
				'flaticon-pinterest-logo'     => esc_html__( 'Pinterest', 'megamio' ),
				'flaticon-twitter'     => esc_html__( 'Twitter', 'megamio' ),
				'flaticon-friends'     => esc_html__( 'Friends', 'megamio' ),
				'flaticon-paper-plane'     => esc_html__( 'Paper Plane', 'megamio' ),
				'flaticon-streaming'     => esc_html__( 'Streaming', 'megamio' ),
			),
		),
		'social_url'  => array(
			'type'    => 'text',
			'label'   => esc_html__( 'Social Link URL', 'megamio' ),
			'default' => '',
		),
	),
) );
