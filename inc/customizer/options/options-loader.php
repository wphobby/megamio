<?php
/**
 * Customizer options for Pre Loader
 *
 * @package megamio
 */

// Enable/Disable Pre-Loader.
Kirki::add_field(
    'megamio_config', array(
        'type'     => 'toggle',
        'settings' => 'enabled_pre_loader',
        'label'    => esc_html__( 'Enable Preloader', 'megamio' ),
        'section'  => 'megamio_section_loader',
        'default'  => true,
    )
);