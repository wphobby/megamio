<?php
/**
 * megamio Header Options
 *
 * @package megamio
 */

// Select header style.
Kirki::add_field(
	'megamio_config', array(
		'type'     => 'radio-image',
		'settings' => 'header_style',
		'label'    => esc_html__( 'Header Layout', 'megamio' ),
		'section'  => 'megamio_section_header',
		'default'  => 'header-default',
		'choices'  => array(
			'header-default'   => get_template_directory_uri() . '/assets/images/header/header-center.png',
			'header-center'     => get_template_directory_uri() . '/assets/images/header/header-left.png',
		),
	)
);

// Enable/Disable breadcrumbs.
Kirki::add_field(
	'megamio_config', array(
		'type'     => 'toggle',
		'settings' => 'header_transparent',
		'label'    => esc_html__( 'Header Transparent', 'megamio' ),
		'section'  => 'megamio_section_header',
		'default'  => true,
	)
);
