<?php
/**
 * Customizer options for single product
 *
 * @package megamio
 */

/* Single Product Layout */
Kirki::add_field(
	'megamio_config', array(
		'type'     => 'select',
		'settings' => 'single_product_layout',
		'label'    => esc_html__( 'Single Product Layout', 'megamio' ),
		'section'  => 'megamio_section_single_product',
		'default'  => '1',
		'choices'  => array(
			'1' => esc_html__( 'Layout 1', 'megamio' ),
			'2' => esc_html__( 'Layout 2', 'megamio' ),
			'3' => esc_html__( 'Layout 3', 'megamio' ),
			'4' => esc_html__( 'Layout 4', 'megamio' ),
			'5' => esc_html__( 'Layout 5', 'megamio' ),
			'6' => esc_html__( 'Layout 6', 'megamio' ),
		),
	)
);


/* Single Product Layout */
Kirki::add_field(
	'megamio_config', array(
		'type'        => 'toggle',
		'settings'    => 'single_product_image_lightbox',
		'label'       => esc_html__( 'Product Image Lightbox', 'megamio' ),
		'description' => esc_html__( 'Opens your images against a dark backdrop', 'megamio' ),
		'section'     => 'megamio_section_single_product',
		'default'     => true,
	)
);

/* Single Product Layout */
Kirki::add_field(
	'megamio_config', array(
		'type'        => 'toggle',
		'settings'    => 'single_product_image_zoom',
		'label'       => esc_html__( 'Product Image Zoom', 'megamio' ),
		'description' => esc_html__( 'Zooms in where your cursor is on the image', 'megamio' ),
		'section'     => 'megamio_section_single_product',
		'default'     => true,
	)
);

/* Single Product Social Share */
Kirki::add_field(
	'megamio_config', array(
		'type'     => 'multicheck',
		'settings' => 'single_product_socials',
		'label'    => esc_html__( 'Socials Share', 'megamio' ),
		'section'  => 'megamio_section_single_product',
		'default'         => array( 'facebook', 'twitter', 'pinterest' ),
		'choices'         => array(
			'facebook'  => esc_html__( 'Facebook', 'megamio' ),
			'twitter'   => esc_html__( 'Twitter', 'megamio' ),
			'google'    => esc_html__( 'Google Plus', 'megamio' ),
			'tumblr'    => esc_html__( 'Tumblr', 'megamio' ),
			'pinterest' => esc_html__( 'Pinterest', 'megamio' ),
			'linkedin'  => esc_html__( 'Linkedin', 'megamio' ),
		),
	)
);

