<?php
/**
 * megamio available Layout Options
 *
 * @package megamio
 */

/* Blog Archive */
Kirki::add_field(
    'megamio_config', array(
        'type'     => 'radio-image',
        'settings' => 'layout_blog',
        'label'    => esc_html__( 'Blog Layout', 'megamio' ),
        'section'  => 'megamio_section_layout',
        'default'  => 'layout-1',
        'choices'     => array(
            'layout-1'        => get_template_directory_uri() . '/assets/images/layout/blog_layout_1.png',
            'layout-2'        => get_template_directory_uri() . '/assets/images/layout/blog_layout_2.png',
            'layout-3'        => get_template_directory_uri() . '/assets/images/layout/blog_layout_3.png',
            'layout-4'        => get_template_directory_uri() . '/assets/images/layout/blog_layout_4.png',
            'layout-5'        => get_template_directory_uri() . '/assets/images/layout/blog_layout_5.png'

        ),
    )
);