<?php
/**
 * Customizer option for typography
 *
 * @package megamio
 */

// Body typography.
Kirki::add_field(
	'megamio_config', array(
		'type'     => 'typography',
		'settings' => 'megamio_typography_body',
		'label'    => esc_attr__( 'Body', 'megamio' ),
		'section'  => 'megamio_section_typography',
		'default'  => array(
			'font-family' => 'Roboto',
			'variant'     => 'regular',
		),
		'output'   => array(
			array(
				'element' => 'body',
			),
		),
		'choices'  => array(
			'fonts'   => array(
				'google' => array(
					'Poppins',
					'Roboto',
					'Open Sans',
				),
			),
			'variant' => array(
				'400',
				'700',
			),
		),
	)
);

// Heading.
Kirki::add_field(
	'megamio_config', array(
		'type'     => 'typography',
		'settings' => 'megamio_typography_heading',
		'label'    => esc_attr__( 'Heading', 'megamio' ),
		'section'  => 'megamio_section_typography',
		'default'  => array(
			'font-family' => 'Catamaran',
			'variant'     => '700',
		),
		'output'   => array(
			array(
				'element' => array( 'h1, h2, h3, h4, h5, h6' ),
			),
		),
		'choices'  => array(
			'fonts'   => array(
				'google' => array(
					'Catamaran',
					'Playfair Display',
				),
			),
			'variant' => array(
				'700',
			),
		),
	)
);
