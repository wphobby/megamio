<?php
/**
 * megamio option for breadcrumb
 *
 * @package megamio
 */

// Enable/Disable breadcrumbs.
Kirki::add_field(
	'megamio_config', array(
		'type'     => 'toggle',
		'settings' => 'enable_breadcrumbs',
		'label'    => esc_html__( 'Enable Breadcrumbs', 'megamio' ),
		'section'  => 'megamio_section_breadcrumb',
		'default'  => true,
	)
);
