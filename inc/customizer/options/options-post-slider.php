<?php
/**
 * megamio post slider Option
 *
 * @package megamio
 */

// Enable Slider.
Kirki::add_field(
	'megamio_config', array(
		'type'     => 'toggle',
		'settings' => 'enabled_post_slider',
		'label'    => esc_html__( 'Enable post slider', 'megamio' ),
		'section'  => 'megamio_section_post_slider',
		'default'  => false,
	)
);

// Post slider.
// Category select.
Kirki::add_field(
	'megamio_config', array(
		'type'            => 'radio-buttonset',
		'settings'        => 'megamio_post_slide_category',
		'label'           => esc_html__( 'Category Select', 'megamio' ),
		'section'         => 'megamio_section_post_slider',
		'default'         => 'latest-post',
		'choices'         => array(
			'latest-post' => esc_attr__( 'Latest Post', 'megamio' ),
			'category'    => esc_attr__( 'Category', 'megamio' ),
		),
		'active_callback' => array(
			array(
				'setting'  => 'enabled_post_slider',
				'operator' => '==',
				'value'    => 1,
			),
		),
	)
);

$category_choices = array();
$categories_lists = get_categories();
foreach ( $categories_lists as $categories => $category ) {
	$category_choices[ $category->term_id ] = $category->name;
}

Kirki::add_field(
	'megamio_config', array(
		'type'            => 'select',
		'settings'        => 'megamio_post_slide_category_select',
		'label'           => esc_html__( 'Categories', 'megamio' ),
		'section'         => 'megamio_section_post_slider',
		'default'         => 'uncategorized',
		'choices'         => $category_choices,
		'active_callback' => array(
			array(
				'setting'  => 'megamio_post_slide_category',
				'operator' => '==',
				'value'    => 'category',
			),
			array(
				'setting'  => 'enabled_post_slider',
				'operator' => '==',
				'value'    => 1,
			),
		),
	)
);
