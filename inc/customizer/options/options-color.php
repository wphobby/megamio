<?php
/**
 * megamio color option
 *
 * @package megamio
 */

// Primary color option.
Kirki::add_field(
	'megamio_config', array(
		'type'     => 'color',
		'settings' => 'megamio_color_primary',
		'label'    => esc_html__( 'Primary Color', 'megamio' ),
		'section'  => 'megamio_section_color',
		'default'  => '#de7b85',
		'output'   => array(
			array(
				'element'  => array(
					'a:hover',
					'a:focus',
					'a:active',
				),
				'function' => 'css',
				'property' => 'color',
			),
			array(
				'element'  => array(
					'.button:hover',
					'button:hover',
					'input[type="button"]:hover',
					'input[type="reset"]:hover',
					'input[type="submit"]:hover',
				),
				'function' => 'css',
				'property' => 'background-color',
			),
			array(
				'element'  => array(
					'.entry-footer .tags-links a:hover',
				),
				'function' => 'css',
				'property' => 'border-color',
			),
		),
	)
);
