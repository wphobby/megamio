<?php
/**
 * Displays Newsletter Widget
 *
 * @package    Megamio
 * @subpackage Widgets
 * @since      1.0.0
 */

/**
 * Class to show newsletter widget
 */
class Megamio_Widget_Newsletter extends WP_Widget {

	/**
	 * Set up newsletter widget
	 */
	public function __construct() {
		$widget_ops = array(
			'classname'                   => 'tg_widget_recent_posts',
			'description'                 => esc_html__( 'Shows recent posts with an image', 'megamio' ),
			'customize_selective_refresh' => true,
		);
		parent::__construct( 'mg-newsletter', esc_html__( 'MG: Newsletter', 'megamio' ), $widget_ops );
		$this->alt_option_name = 'mg_widget_newsletter';
	}

	/**
	 * Output the html markup
	 *
	 * @param array $args     display arguments.
	 * @param array $instance settings for the current.
	 *
	 * @return void
	 */
	public function widget( $args, $instance ) {
		if ( ! isset( $args ['widget_id'] ) ) {
			$args['widget_id'] = $this->id;
		}
		$title  = ( ! empty( $instance['title'] ) ) ? $instance['title'] : esc_html__( 'Newsletter', 'megamio' );
		$title  = apply_filters( 'widget_title', $title, $instance, $this->id_base );
		$description  = ( ! empty( $instance['description'] ) ) ? $instance['description'] : esc_html__( 'Subscribe and get a 10% off your next order', 'megamio' );
		?>
		<?php echo $args['before_widget']; // WPCS: XSS OK. ?>
		<div class="footer-newsletter mt-50">
				<h6 class="newsletter-title"><?php echo $title; ?></h6>
				<p><?php echo $description; ?></p>
				<div class="newsletter-form pt-35">
					<form action="#">
						<input type="text" placeholder="Newsletter">
						<button><i class="flaticon-paper-plane"></i></button>
					</form>
				</div>
			</div> <!-- row -->
		<?php
		echo $args['after_widget']; // WPCS: XSS OK.
	}

	/**
	 * Update widget settings
	 *
	 * @param array $new_instance new settings.
	 * @param array $old_instance old settings.
	 *
	 * @return array updated settings to save.
	 */
	public function update( $new_instance, $old_instance ) {
		$instance              = $old_instance;
		$instance['title']     = sanitize_text_field( $new_instance['title'] );
		$instance['description']     = sanitize_text_field( $new_instance['description'] );

		return $instance;
	}

	/**
	 * Shows the form in the back-end.
	 *
	 * @param array $instance settings.
	 */
	public function form( $instance ) {
		$title     = isset( $instance['title'] ) ? esc_attr( $instance['title'] ) : '';
		$description    = isset( $instance['description'] ) ? esc_attr( $instance['description'] ) : '';
	    ?>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php esc_html_e( 'Title:', 'megamio' ); ?></label>
			<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
		</p>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'description' ) ); ?>"><?php esc_html_e( 'Title:', 'megamio' ); ?></label>
			<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'description' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'description' ) ); ?>" type="text" value="<?php echo esc_attr( $description ); ?>" />
		</p>
		<?php
	}
}

register_widget( 'Megamio_Widget_Newsletter' );
