<?php
function megamio_import_files() {
  return array(

      array(
          'import_file_name'           => __( 'Fashion Demo', 'megamio' ),
          'local_import_file'            => get_stylesheet_directory() . '/inc/demo/default/all_content.xml',
          'local_import_widget_file'     => get_stylesheet_directory() . '/inc/demo/default/widgets.wie',
          'local_import_customizer_file' => get_stylesheet_directory() . '/inc/demo/default/megamio-export.dat',
          'import_preview_image_url'     => get_template_directory_uri() .'/assets/images/screenshot/screenshot-1.png',
          'import_notice'                => __( 'After you import this demo, you will have setup all content.', 'megamio' ),
      ),

      array(
          'import_file_name'           => __( 'Furniture Demo', 'megamio' ),
          'local_import_file'            => get_stylesheet_directory() . '/inc/demo/default/all_content.xml',
          'local_import_widget_file'     => get_stylesheet_directory() . '/inc/demo/default/widgets.wie',
          'local_import_customizer_file' => get_stylesheet_directory() . '/inc/demo/default/megamio-export.dat',
          'import_preview_image_url'     => get_template_directory_uri() .'/assets/images/screenshot/screenshot-2.png',
          'import_notice'                => __( 'After you import this demo, you will have setup all content.', 'megamio' ),
      ),

      array(
          'import_file_name'           => __( 'Bags Demo', 'megamio' ),
          'local_import_file'            => get_stylesheet_directory() . '/inc/demo/default/all_content.xml',
          'local_import_widget_file'     => get_stylesheet_directory() . '/inc/demo/default/widgets.wie',
          'local_import_customizer_file' => get_stylesheet_directory() . '/inc/demo/default/megamio-export.dat',
          'import_preview_image_url'     => get_template_directory_uri() .'/assets/images/screenshot/screenshot-3.png',
          'import_notice'                => __( 'After you import this demo, you will have setup all content.', 'megamio' ),
      ),

      array(
          'import_file_name'           => __( 'Shoes Demo', 'megamio' ),
          'local_import_file'            => get_stylesheet_directory() . '/inc/demo/default/all_content.xml',
          'local_import_widget_file'     => get_stylesheet_directory() . '/inc/demo/default/widgets.wie',
          'local_import_customizer_file' => get_stylesheet_directory() . '/inc/demo/default/megamio-export.dat',
          'import_preview_image_url'     => get_template_directory_uri() .'/assets/images/screenshot/screenshot-4.png',
          'import_notice'                => __( 'After you import this demo, you will have setup all content.', 'megamio' ),
      ),

      array(
          'import_file_name'           => __( 'Watch Demo', 'megamio' ),
          'local_import_file'            => get_stylesheet_directory() . '/inc/demo/default/all_content.xml',
          'local_import_widget_file'     => get_stylesheet_directory() . '/inc/demo/default/widgets.wie',
          'local_import_customizer_file' => get_stylesheet_directory() . '/inc/demo/default/megamio-export.dat',
          'import_preview_image_url'     => get_template_directory_uri() .'/assets/images/screenshot/screenshot-5.png',
          'import_notice'                => __( 'After you import this demo, you will have setup all content.', 'megamio' ),
      ),

    );


////////////////////////////////////////////////////////////////////
 
}
add_filter( 'pt-ocdi/import_files', 'megamio_import_files' );


function megamio_after_import_setup() {
    // Assign menus to their locations.
    $header_menu = get_term_by( 'name', 'Primary Menu', 'nav_menu' );
    $off_canvas_menu = get_term_by( 'name', 'Off Canvas', 'nav_menu' );
    $footer_menu_one = get_term_by( 'name', 'Footer Menu One', 'nav_menu' );
    $footer_menu_two = get_term_by( 'name', 'Footer Menu Two', 'nav_menu' );
    $footer_menu_three = get_term_by( 'name', 'Footer Menu Three', 'nav_menu' );
    $footer_menu_bottom = get_term_by( 'name', 'Footer Bottom Menu', 'nav_menu' );


    set_theme_mod( 'nav_menu_locations' , array(
            'primary' => $header_menu->term_id,
            'primary_mobile' => $header_menu->term_id,
            'off_canvas' => $off_canvas_menu->term_id,
            'footer_section_one' => $footer_menu_one->term_id,
            'footer_section_two' => $footer_menu_two->term_id,
            'footer_section_three' => $footer_menu_three->term_id,
            'footer_bottom' => $footer_menu_bottom->term_id,
        )
    );

    // Assign front page and posts page (blog page).
    $front_page_id = get_page_by_title( 'Home' );
    $blog_page_id  = get_page_by_title( 'Blog' );

    update_option( 'show_on_front', 'page' );
    update_option( 'page_on_front', $front_page_id->ID );
    update_option( 'page_for_posts', $blog_page_id->ID );

}
add_action( 'pt-ocdi/after_import', 'megamio_after_import_setup' );