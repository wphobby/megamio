<?php

// [new_arrivals]

if (class_exists('WooCommerce')) {

   /**
     * New Arrivals Product Grid
     *
     * @param array $atts
     *
     * @return string
     */
   function megamio_new_arrivals( $atts, $content ) {
        $atts = shortcode_atts(
            array(
                'title'     => '',
                'limit'     => 8,
                'columns'   => 4,
                'orderby'   => 'title',
                'order'     => 'ASC',
                'filter'    => 'category',
                'tabs'      => '',
                'category'  => '',
                'el_class'  => '',
                'top_style' => 'style-1',
            ), $atts
        );

        if ( ! is_woocommerce_activated() ) {
            return;
        }

        $css_class = array(
            'new-arrivals pt-90 pb-90 megamio-products-grid megamio-products',
            $atts['el_class']
        );

        $output = array();
        $filter = array();
        $type   = 'products';

        $attr = array(
            'limit'   => intval( $atts['limit'] ),
            'columns' => intval( $atts['columns'] ),
            'orderby' => $atts['orderby'],
            'order'   => $atts['order'],
        );

        // set product columns
        set_theme_mod('vc_product_columns', $atts['columns']);

       if ( $atts['filter'] == 'category' ) {
            if ( $atts['category'] ) {
                $cats = explode( ',', $atts['category'] );

                foreach ( $cats as $cat ) {
                    $cat      = get_term_by( 'slug', $cat, 'product_cat' );
                    $filter[] = sprintf( '<li class="nav-item" data-filter="%s">
                                <a id="%s-tab" data-toggle="tab" href="#%s" role="tab" aria-controls="%s" aria-selected="false" class="">%s</a>
                               </li>', esc_html( $cat->name ), esc_html( $cat->name ), esc_html( $cat->name ), esc_html( $cat->name ), esc_attr( $cat->name ) );
                }
            }

        }

        $link = '';

        $atts['page'] = 1;

        $filter_html = '';

        if( $atts['top_style'] == 'style-1'){
            $filter_html = '<div class="row">
                        <div class="col-lg-12">
                           <div class="section-title text-center">
                              <h3>'.$atts['title'].'</h3>
                           </div> <!-- section title -->
                        </div>
                      </div>
                     <div class="row"><div class="col-lg-12"><div class="arrivals-menu mt-10">
                        <ul class="nav justify-content-center" id="myTab" role="tablist">'
                . implode( "\n", $filter ) .
                '</ul></div></div></div>';
            }else if($atts['top_style'] == 'style-2'){
            $filter_html = '<div class="container"><div class="row">
                        <div class="col-lg-4">
                           <div class="section-title">
                              <h3>'.$atts['title'].'</h3>
                           </div> <!-- section title -->
                        </div>
                       <div class="col-lg-8"><div class="arrivals-menu">
                        <ul class="nav justify-content-center justify-content-lg-end" id="myTab" role="tablist">'
                . implode( "\n", $filter ) .
                '</ul></div></div></div></div>';
        }

        $output[] = '<div class="product-header">';
        $output[] = $filter_html;
        $output[] = $link;
        $output[] = '</div>';
        $output[] = '<div class="product-wrapper">';
        $output[] = get_wc_products( $atts, $type );
        $output[] = '</div>';

        return sprintf(
            '<div class="%s" data-attr="%s" data-nonce="%s">%s</div>',
            esc_attr( implode( ' ', $css_class ) ),
            esc_attr( json_encode( $attr ) ),
            esc_attr( wp_create_nonce( 'megamio_get_products' ) ),
            implode( '', $output )
        );
    }

   add_shortcode('new_arrivals', 'megamio_new_arrivals');


    /**
     * Ajax load products
     */
    function ajax_load_products() {
        check_ajax_referer( 'megamio_get_products', 'nonce' );

        $attr = $_POST['attr'];

        $attr['load_more'] = isset( $_POST['load_more'] ) ? $_POST['load_more'] : true;
        $attr['page']      = isset( $_POST['page'] ) ? $_POST['page'] : 1;

        $type = isset( $_POST['type'] ) ? $_POST['type'] : '';

        $products = get_wc_products( $attr, $type );

        wp_send_json_success($products);

    }

    add_action( 'wp_ajax_nopriv_megamio_load_products', 'ajax_load_products');
    add_action( 'wp_ajax_megamio_load_products', 'ajax_load_products');

   /**
     * @param $atts
     * @param $type
     *
     * @return string
     */
   function get_wc_products( $atts, $type = 'products' ) {
        if ( ! class_exists( 'WC_Shortcode_Products' ) ) {
            return '';
        }


        $attr = array(
            'limit'    => intval( $atts['limit'] ),
            'columns'  => intval( $atts['columns'] ),
            'page'     => $atts['page'],
            'category' => $atts['category'],
            'paginate' => true,
            'orderby'  => $atts['orderby'],
            'order'    => $atts['order'],
        );

        $current_page = absint( empty( $_GET['product-page'] ) ? 1 : $_GET['product-page'] );

        if ( isset( $attr['page'] ) ) {
            $_GET['product-page'] = $attr['page'];
        }

        $shortcode = new WC_Shortcode_Products( $attr, $type );

        $args = $shortcode->get_query_args();
        $html = $shortcode->get_content();

        $products   = new WP_Query( $args );
        $total_page = $products->max_num_pages;

        if ( isset( $attr['page'] ) ) {
            $_GET['product-page'] = $current_page;
        }

        return '<div class="container">'.$html.'</div>';
    }


}