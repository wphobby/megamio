<?php

// [newsletter]

function megamio_newsletter($params = array(), $content = null) {
    extract(shortcode_atts(array(
        'full_height' 				=> 'yes',
        'custom_height'	 			=> '',
        'title' 					=> '',
        'sub_title' 				=> '',
        'button_text'               => '',
        'bg_image'					=> '',
    ), $params));

    if (is_numeric($bg_image))
    {
        $bg_image = wp_get_attachment_url($bg_image);
    } else {
        $bg_image = "";
    }


    $megamio_newsletter = '<section class="newsletter-area">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-xl-10 col-md-11">
                    <div class="newsletter-content pt-50 pb-100">
                        <div class="row align-items-center">
                            <div class="col-lg-5">
                                <div class="newsletter-discount text-center text-lg-left mt-45">
                                    <h3>'.$title.'</h3>
                                    <p>'.$sub_title.'</p>
                                </div> <!-- newsletter discount -->
                            </div>
                            <div class="col-lg-7">
                                <div class="newsletter-form mt-50">
                                    <form action="#">
                                        <input type="text" placeholder="Enter your email...">
                                        <button type="button">'.$button_text.'</button>
                                    </form>
                                </div>
                            </div>
                        </div> <!-- row -->
                    </div> <!-- newsletter content -->
                </div>
            </div> <!-- row -->
        </div> <!-- container -->
    </section>
';

    return $megamio_newsletter;
}

add_shortcode('newsletter', 'megamio_newsletter');