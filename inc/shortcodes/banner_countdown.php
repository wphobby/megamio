<?php

// [banner_countdown]

function megamio_banner_countdown($params = array(), $content = null) {
    extract(shortcode_atts(array(
        'full_height' 				=> 'yes',
        'custom_height'	 			=> '',
        'title' 					=> '',
        'product_name' 				=> '',
        'product_url'               => '#',
        'button_text'               => 'shop now',
        'date'                      => '2019/07/01',
        'description' 				=> '',
        'button_text'               => '',
        'bg_image'					=> '',
        'text_align'				=> 'left'
    ), $params));

    if (is_numeric($bg_image))
    {
        $bg_image = wp_get_attachment_url($bg_image);
    } else {
        $bg_image = "";
    }


    $megamio_banner_countdown = '<section class="banner_countdown bg_cover pt-60 pb-60" style="background-image: url('.$bg_image.')">
        <div class="container">
            <div class="row ">
                <div class="col-lg-6">
                    <div class="banner_static-content">
                        <h4 class="banner-title">'.$title.'</h4>
                        <h2 class="banner-sale" >'.$product_name.'</h2>
                        <div class="banner-price">
                           <span class="regular-price">$89.90</span>
                           <span class="sale-price">$65.90</span>
                        </div>
                        <div class="countdown-area">
                          <div data-countdown="'.$date.'"></div>
                       </div>
                        <a class="shop-now-button" href="'.$product_url.'">'.$button_text.'</a>
                    </div> <!-- banner_static content -->
                </div>
            </div> <!-- row -->
        </div> <!-- container -->
    </section>
';

    return $megamio_banner_countdown;
}

add_shortcode('banner_countdown', 'megamio_banner_countdown');