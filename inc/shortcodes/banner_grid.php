<?php

// [banner_grid]

function megamio_banner_grid($params = array(), $content = null) {
    extract(shortcode_atts(array(
        'full_height' 				=> 'yes',
        'custom_height'	 			=> '',
        'title' 					=> '',
        'sub_title' 				=> '',
        'description' 				=> '',
        'button_text'               => '',
        'bg_image'					=> '',
        'text_align'				=> 'left'
    ), $params));

    if (is_numeric($bg_image))
    {
        $bg_image = wp_get_attachment_url($bg_image);
    } else {
        $bg_image = "";
    }


    $megamio_banner = '<section class="banner_static-area">
        <div class="container">
            <div class="banner_static pt-60 pb-100">
                <div class="row align-items-center">
                    <div class="col-lg-6">
                        <div class="banner_static-content mt-35">
                            <h4 class="banner-title">'.$sub_title.'</h4>
                            <h2 class="banner-sale mb-20" >'.$title.'</h2>
                            <p class="mb-45">'.$description.'</p>
                            <a class="main-btn" href="#">'.$button_text.'</a>
                        </div> <!-- banner_static contant -->
                    </div>
                    <div class="col-lg-6">
                        <div class="banner_static-image mt-40 text-center">
                            <img src="'.$bg_image.'" alt="banner">
                        </div> <!-- banner_static image -->
                    </div>
                </div> <!-- row -->
            </div> <!-- banner_static -->
        </div> <!-- container -->
    </section>';

    return $megamio_banner_grid;
}

add_shortcode('banner_grid', 'megamio_banner_grid');