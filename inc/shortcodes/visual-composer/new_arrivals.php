<?php

/*
**	New Arrivals
*/

$taxonomy     = 'product_cat';
$orderby      = 'name';
$show_count   = 0;      // 1 for yes, 0 for no
$pad_counts   = 0;      // 1 for yes, 0 for no
$hierarchical = 1;      // 1 for yes, 0 for no
$title        = '';
$empty        = 0;

$args = array(
    'taxonomy'     => $taxonomy,
    'orderby'      => $orderby,
    'show_count'   => $show_count,
    'pad_counts'   => $pad_counts,
    'hierarchical' => $hierarchical,
    'title_li'     => $title,
    'hide_empty'   => $empty
);

$all_categories = get_categories( $args );
$cat_list = array();

foreach ($all_categories as $cat) {
    $cat_list[] = array('label' => $cat->name, 'value' => $cat->name);
}


// [new_arrivals]

vc_map(array(
    "name"			=> "New Arrivals",
    "description"	=> "New Arrivals",
    "base"			=> "new_arrivals",
    "category"      => esc_html__( "WPHobby", 'megamio' ),
    "class"			=> "",
    "icon"			=> get_template_directory_uri() . "/assets/images/visual_composer/new_arrivals_main.png",
    "params" 	=> array(

        array(
            "type"			=> "textfield",
            "holder"		=> "div",
            "class" 		=> "hide_in_vc_editor",
            "admin_label" 	=> false,
            "heading"		=> "Title",
            "param_name"	=> "title",
            "value"			=> "",
        ),

        array(
            'heading'     => esc_html__( 'Number Of Products', 'megamio' ),
            'description' => esc_html__( 'Total number of products you want to show. Set -1 to show all products', 'megamio' ),
            'param_name'  => 'limit',
            'type'        => 'textfield',
            'value'       => 8,
        ),
        array(
            'heading'     => esc_html__( 'Columns', 'megamio' ),
            'description' => esc_html__( 'Display products in how many columns', 'megamio' ),
            'param_name'  => 'columns',
            'type'        => 'dropdown',
            'std'         => 4,
            'value'       => array(
                esc_html__( '3 Columns', 'megamio' ) => 3,
                esc_html__( '4 Columns', 'megamio' ) => 4,
                esc_html__( '6 Columns', 'megamio' ) => 6,
            ),
        ),

        array(
            'heading'     => esc_html__( 'Top Style', 'megamio' ),
            'description' => esc_html__( 'Top section display style', 'megamio' ),
            'param_name'  => 'top_style',
            'type'        => 'dropdown',
            'std'         => 1,
            'value'       => array(
                esc_html__( 'Style_1', 'megamio' ) => 'style-1',
                esc_html__( 'Style_2', 'megamio' ) => 'style-2',
            ),
        ),

       array(
            "type" 			=> "autocomplete",
            "heading" 		=> "Categories",
            "param_name" 	=> 'category',
            "settings" 		=> array(
                "multiple" 	=> true,
                "sortable" 	=> true,
                "values"	=> $cat_list,
            ),
            "save_always" => true
       ),
    )
));