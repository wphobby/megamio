<?php

/*
**	Banner Countdown
*/

vc_map( array(
    "name"			=> "Banner Countdown",
    "description"	=> "Banner Countdown",
    "base"			=> "banner_countdown",
    "category"      => esc_html__( "WPHobby", 'megamio' ),
    "class"			=> "",
    "icon"			=> get_template_directory_uri() . "/assets/images/visual_composer/banner_countdown.png",
    "params" => array(
        // add params same as with any other content element

        array(
            "type"			=> "dropdown",
            "holder"		=> "div",
            "class" 		=> "hide_in_vc_editor",
            "admin_label" 	=> false,
            "heading"		=> "Height",
            "param_name"	=> "full_height",
            "value"			=> array('Full Height' => 'yes', 'Custom Height' => 'no'),
        ),

        array(
            "type"			=> "textfield",
            "holder"		=> "div",
            "class" 		=> "hide_in_vc_editor",
            "admin_label" 	=> false,
            "heading"		=> "Custom Height",
            "param_name"	=> "custom_height",
            "value"			=> "",
            "dependency"	=> array(
                "element" 	=> "full_height",
                "value"		=> array('no'),
            ),
        ),

        array(
            "type"			=> "textfield",
            "holder"		=> "div",
            "class" 		=> "hide_in_vc_editor",
            "admin_label" 	=> false,
            "heading"		=> "Title",
            "param_name"	=> "title",
            "value"			=> "",
        ),

        array(
            "type"			=> "textfield",
            "holder"		=> "div",
            "class" 		=> "hide_in_vc_editor",
            "admin_label" 	=> false,
            "heading"		=> "Product Name",
            "param_name"	=> "product_name",
            "value"			=> "",
        ),

        array(
            "type"			=> "textfield",
            "holder"		=> "div",
            "class" 		=> "hide_in_vc_editor",
            "admin_label" 	=> false,
            "heading"		=> "Product URL",
            "param_name"	=> "product_url",
            "value"			=> "#",
        ),

        array(
            "type"			=> "textfield",
            "holder"		=> "div",
            "class" 		=> "hide_in_vc_editor",
            "admin_label" 	=> false,
            "heading"		=> "Button Text",
            "param_name"	=> "button_text",
            "value"			=> "shop now",
        ),

        array(
            "type"			=> "textfield",
            "holder"		=> "div",
            "class" 		=> "hide_in_vc_editor",
            "admin_label" 	=> true,
            "heading"		=> "Date",
            'description'   => esc_html__( 'Enter the date in format: YYYY/MM/DD', 'megamio' ),
            "param_name"	=> "date",
            "value"			=> "2019/07/01",
        ),


        array(
            "type"			=> "textarea",
            "holder"		=> "div",
            "class" 		=> "hide_in_vc_editor",
            "admin_label" 	=> false,
            "heading"		=> "Description",
            "param_name"	=> "description",
            "value"			=> "",
        ),

        array(
            "type"			=> "textfield",
            "holder"		=> "div",
            "class" 		=> "hide_in_vc_editor",
            "admin_label" 	=> false,
            "heading"		=> "Button Text",
            "param_name"	=> "button_text",
            "value"			=> "",
        ),

        array(
            "type"			=> "attach_image",
            "holder"		=> "div",
            "class" 		=> "hide_in_vc_editor",
            "admin_label" 	=> false,
            "heading"		=> "Background Image",
            "param_name"	=> "bg_image",
            "value"			=> "",
        ),


    )
));