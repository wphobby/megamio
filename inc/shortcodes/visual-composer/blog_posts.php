<?php

/*
**	Blog Posts
*/

$args = array(
    'type'                     => 'post',
    'child_of'                 => 0,
    'parent'                   => '',
    'orderby'                  => 'name',
    'order'                    => 'ASC',
    'hide_empty'               => 1,
    'hierarchical'             => 1,
    'exclude'                  => '',
    'include'                  => '',
    'number'                   => '',
    'taxonomy'                 => 'category',
    'pad_counts'               => false
);

$categories = get_categories($args);

$output_categories = array();

$output_categories["All"] = "";

foreach($categories as $category) {
    $output_categories[htmlspecialchars_decode($category->name)] = $category->slug;
}

vc_map( array(
    "name"			=> "Blog Posts",
    "description"	=> "Blog Posts",
    "base"			=> "blog_posts",
    "category"      => esc_html__( "WPHobby", 'megamio' ),
    "class"			=> "",
    "icon"			=> get_template_directory_uri() . "/assets/images/visual_composer/blog_posts.png",
    "params" => array(
        // add params same as with any other content element

        array(
            "type"			=> "dropdown",
            "holder"		=> "div",
            "class" 		=> "hide_in_vc_editor",
            "admin_label" 	=> false,
            "heading"		=> "Height",
            "param_name"	=> "full_height",
            "value"			=> array('Full Height' => 'yes', 'Custom Height' => 'no'),
        ),

        array(
            "type"			=> "textfield",
            "holder"		=> "div",
            "class" 		=> "hide_in_vc_editor",
            "admin_label" 	=> false,
            "heading"		=> "Custom Height",
            "param_name"	=> "custom_height",
            "value"			=> "",
            "dependency"	=> array(
                "element" 	=> "full_height",
                "value"		=> array('no'),
            ),
        ),

        array(
            "type"			=> "textfield",
            "holder"		=> "div",
            "class" 		=> "hide_in_vc_editor",
            "admin_label" 	=> false,
            "heading"		=> "Title",
            "param_name"	=> "title",
            "value"			=> "",
        ),

        array(
            "type"			=> "textfield",
            "holder"		=> "div",
            "class" 		=> "hide_in_vc_editor",
            "admin_label" 	=> false,
            "heading"		=> "Blog Text",
            "param_name"	=> "blog_text",
            "value"			=> "",
        ),

        array(
            "type"			=> "textfield",
            "holder"		=> "div",
            "class" 		=> "hide_in_vc_editor",
            "admin_label" 	=> false,
            "heading"		=> "Blog URL",
            "param_name"	=> "blog_url",
            "value"			=> "",
        ),

        array(
            "type" => "dropdown",
            "holder" => "div",
            "class" => "hide_in_vc_editor",
            "admin_label" => true,
            "heading" => "Category",
            "param_name" => "category",
            "value" => $output_categories
        ),


    )
));