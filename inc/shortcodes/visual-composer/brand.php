<?php

/*
**	Brand
*/

//Register "container" content element. It will hold all your inner (child) content elements
vc_map( array(
    "name"			=> "Brand",
    "description"	=> "Brand",
    "base"			=> "brand",
    "category"      => esc_html__( "WPHobby", 'megamio' ),
    "class"			=> "",
    "icon"			=> get_template_directory_uri() . "/assets/images/visual_composer/brand.png",
    "as_parent" => array('only' => 'brand_inner'),
    "content_element" => true,
    "params" => array(
        // add params same as with any other content element
        array(
            "type"			=> "textfield",
            "holder"		=> "div",
            "class" 		=> "hide_in_vc_editor",
            "admin_label" 	=> false,
            "heading"		=> "Title",
            "param_name"	=> "title",
            "value"			=> "",
        ),
    ),
    "js_view" => 'VcColumnView'
));

vc_map( array(
    "name" => 'Brand Inner',
    "base" => "brand_inner",
    "as_child" => array('only' => 'brand'), // Use only|except attributes to limit parent (separate multiple values with comma)
    "icon"	=> get_template_directory_uri() . "/assets/images/visual_composer/brand_inner.png",
    "params" => array(
        // add params same as with any other content element
        array(
            "type"			=> "attach_image",
            "holder"		=> "div",
            "class" 		=> "hide_in_vc_editor",
            "admin_label" 	=> false,
            "heading"		=> "Background Image",
            "param_name"	=> "bg_image",
            "value"			=> "",
        ),
    )
) );
//Your "container" content element should extend WPBakeryShortCodesContainer class to inherit all required functionality
if ( class_exists( 'WPBakeryShortCodesContainer' ) ) {
    class WPBakeryShortCode_Brand extends WPBakeryShortCodesContainer {
    }
}
if ( class_exists( 'WPBakeryShortCode' ) ) {
    class WPBakeryShortCode_Brand_Inner extends WPBakeryShortCode {
    }
}