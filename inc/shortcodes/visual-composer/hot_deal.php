<?php

/*
**	Hot Deal
*/

vc_map( array(
    "name"			=> "Hot Deal",
    "description"	=> "Hot Deal",
    "base"			=> "hot_deal",
    "category"      => esc_html__( "WPHobby", 'megamio' ),
    "class"			=> "",
    "icon"			=> get_template_directory_uri() . "/assets/images/visual_composer/hot_deal.png",
    "params" => array(
        // add params same as with any other content element

        array(
            "type"			=> "dropdown",
            "holder"		=> "div",
            "class" 		=> "hide_in_vc_editor",
            "admin_label" 	=> false,
            "heading"		=> "Height",
            "param_name"	=> "full_height",
            "value"			=> array('Full Height' => 'yes', 'Custom Height' => 'no'),
        ),

        array(
            "type"			=> "textfield",
            "holder"		=> "div",
            "class" 		=> "hide_in_vc_editor",
            "admin_label" 	=> false,
            "heading"		=> "Custom Height",
            "param_name"	=> "custom_height",
            "value"			=> "",
            "dependency"	=> array(
                "element" 	=> "full_height",
                "value"		=> array('no'),
            ),
        ),

        array(
            "type"			=> "textfield",
            "holder"		=> "div",
            "class" 		=> "hide_in_vc_editor",
            "admin_label" 	=> false,
            "heading"		=> "Title",
            "param_name"	=> "title",
            "value"			=> "",
        ),

        array(
            "type"			=> "textfield",
            "holder"		=> "div",
            "class" 		=> "hide_in_vc_editor",
            "admin_label" 	=> false,
            "heading"		=> "Sub Title",
            "param_name"	=> "sub_title",
            "value"			=> "",
        ),


        array(
            "type"			=> "textarea",
            "holder"		=> "div",
            "class" 		=> "hide_in_vc_editor",
            "admin_label" 	=> false,
            "heading"		=> "Price",
            "param_name"	=> "price",
            "value"			=> "",
        ),

        array(
            "type"			=> "textfield",
            "holder"		=> "div",
            "class" 		=> "hide_in_vc_editor",
            "admin_label" 	=> false,
            "heading"		=> "Button Text",
            "param_name"	=> "button_text",
            "value"			=> "",
        ),

        array(
            "type"			=> "textfield",
            "holder"		=> "div",
            "class" 		=> "hide_in_vc_editor",
            "admin_label" 	=> false,
            "heading"		=> "Button URL",
            "param_name"	=> "button_url",
            "value"			=> "",
        ),

        array(
            "type"			=> "attach_image",
            "holder"		=> "div",
            "class" 		=> "hide_in_vc_editor",
            "admin_label" 	=> false,
            "heading"		=> "Background Image",
            "param_name"	=> "bg_image",
            "value"			=> "",
        ),


    )
));