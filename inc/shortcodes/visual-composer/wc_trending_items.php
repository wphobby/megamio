<?php

$taxonomy     = 'product_cat';
$orderby      = 'name';
$show_count   = 0;      // 1 for yes, 0 for no
$pad_counts   = 0;      // 1 for yes, 0 for no
$hierarchical = 1;      // 1 for yes, 0 for no
$title        = '';
$empty        = 0;

$args = array(
    'taxonomy'     => $taxonomy,
    'orderby'      => $orderby,
    'show_count'   => $show_count,
    'pad_counts'   => $pad_counts,
    'hierarchical' => $hierarchical,
    'title_li'     => $title,
    'hide_empty'   => $empty
);

$all_categories = get_categories( $args );
$cat_list = array();

foreach ($all_categories as $cat) {
    $cat_list[] = array('label' => $cat->name, 'value' => $cat->name);
}


// [trending_items]

vc_map(array(
    "name"			=> "Trending Items",
    "category"      => esc_html__( "WPHobby", 'megamio' ),
    "description"	=> "",
    "base"			=> "trending_items",
    "class"			=> "",
    "icon"			=> get_template_directory_uri() . "/assets/images/visual_composer/categories-grid.png",
    "params" 	=> array(

        array(
            "type" => "textfield",
            "holder" => "div",
            "class" => "hide_in_vc_editor",
            "admin_label" => true,
            "heading" => "Number of Posts",
            "param_name" => "posts",
            "description" => "Number of posts to be displayed in the slider."
        ),

        array(
            "type" => "dropdown",
            "holder" => "div",
            "class" => "hide_in_vc_editor",
            "admin_label" => true,
            "heading" => "Category",
            "param_name" => "category",
            "value" => $cat_list
        ),
    )
));