<?php

/*
**	SLIDER
*/

//Register "container" content element. It will hold all your inner (child) content elements
vc_map( array(
    "name"			=> "Featured_Box",
    "description"	=> "Featured_Box",
    "base"			=> "featured_box",
    "category"      => esc_html__( "WPHobby", 'megamio' ),
    "class"			=> "",
    "icon"			=> get_template_directory_uri() . "/assets/images/visual_composer/featured_box.png",
    "as_parent" => array('only' => 'featured_box_item'),
    "content_element" => true,
    "params" => array(
        // add params same as with any other content element

        array(
            "type"			=> "dropdown",
            "holder"		=> "div",
            "class" 		=> "hide_in_vc_editor",
            "admin_label" 	=> false,
            "heading"		=> "Height",
            "param_name"	=> "full_height",
            "value"			=> array('Full Height' => 'yes', 'Custom Height' => 'no'),
        ),

        array(
            "type"			=> "textfield",
            "holder"		=> "div",
            "class" 		=> "hide_in_vc_editor",
            "admin_label" 	=> false,
            "heading"		=> "Custom Height",
            "param_name"	=> "custom_height",
            "value"			=> "",
            "dependency"	=> array(
                "element" 	=> "full_height",
                "value"		=> array('no'),
            ),
        ),

    ),
    "js_view" => 'VcColumnView'
));

vc_map( array(
    "name" => 'Featured Box Item',
    "base" => "featured_box_item",
    "as_child" => array('only' => 'featured_box'), // Use only|except attributes to limit parent (separate multiple values with comma)
    "icon"	=> get_template_directory_uri() . "/assets/images/visual_composer/featured_box_item.png",
    "params" => array(
        // add params same as with any other content element

        array(
            "type"			=> "textfield",
            "holder"		=> "div",
            "class" 		=> "hide_in_vc_editor",
            "admin_label" 	=> false,
            "heading"		=> "Title",
            "param_name"	=> "title",
            "value"			=> "",
        ),


        array(
            "type"			=> "textarea",
            "holder"		=> "div",
            "class" 		=> "hide_in_vc_editor",
            "admin_label" 	=> false,
            "heading"		=> "Description",
            "param_name"	=> "description",
            "value"			=> "",
        ),


        array(
            "type"			=> "textfield",
            "holder"		=> "div",
            "class" 		=> "hide_in_vc_editor",
            "admin_label" 	=> false,
            "heading"		=> "Button Text",
            "param_name"	=> "button_text",
            "value"			=> "",
        ),

        array(
            "type"			=> "textfield",
            "holder"		=> "div",
            "class" 		=> "hide_in_vc_editor",
            "admin_label" 	=> false,
            "heading"		=> "Button URL",
            "param_name"	=> "button_url",
            "value"			=> "",
        ),

        array(
            "type"			=> "attach_image",
            "holder"		=> "div",
            "class" 		=> "hide_in_vc_editor",
            "admin_label" 	=> false,
            "heading"		=> "Product Image",
            "param_name"	=> "product_image",
            "value"			=> "",
        ),

        array(
            "type"			=> "dropdown",
            "holder"		=> "div",
            "class" 		=> "hide_in_vc_editor",
            "admin_label" 	=> false,
            "heading"		=> "Text Align",
            "param_name"	=> "text_align",
            "value"			=> array(
                "Left"		=> "left",
                "Center"	=> "center",
                "Right"		=> "right",
            ),
            "std"			=> "",
        ),
    )
) );
//Your "container" content element should extend WPBakeryShortCodesContainer class to inherit all required functionality
if ( class_exists( 'WPBakeryShortCodesContainer' ) ) {
    class WPBakeryShortCode_Featured_Box extends WPBakeryShortCodesContainer {
    }
}
if ( class_exists( 'WPBakeryShortCode' ) ) {
    class WPBakeryShortCode_Featured_Box_Item extends WPBakeryShortCode {
    }
}

