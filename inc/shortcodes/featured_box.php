<?php

// [featured_box]

function megamio_featured_box($params = array(), $content = null) {
    extract(shortcode_atts(array(
        'full_height' 				=> 'yes',
        'custom_height'	 			=> '',
    ), $params));

    $megamio_featured_box = '<section class="products-design pt-100">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-10">
		              '.do_shortcode($content).'</div>
            </div> <!-- row -->
        </div> <!-- container -->
    </section>';

    return $megamio_featured_box;
}

add_shortcode('featured_box', 'megamio_featured_box');

function megamio_featured_box_item($params = array(), $content = null) {
    extract(shortcode_atts(array(
        'title' 					=> '',
        'description' 				=> '',
        'button_text' 				=> 'shop now',
        'button_url' 				=> '#',
        'product_image'					=> '',
        'text_align'				=> 'right',
    ), $params));

    if (is_numeric($product_image))
    {
        $product_image = wp_get_attachment_url($product_image);
    } else {
        $product_image = '';
    }


    if($text_align == 'left'){
        $text_align_class= 'order-lg-first';
        $image_align_class= 'order-lg-last';
        $text_class = '';
    }else if($text_align == 'right'){
        $text_align_class= '';
        $image_align_class= '';
        $text_class = 'pl-55';

    }

    $megamio_featured_box_item = '<div class="products">
                        <div class="row no-gutters align-items-center">
                            <div class="col-lg-6 '.$image_align_class.'">
                                <div class="products-image">
                                    <img src="'.$product_image.'" alt="products design">
                                </div>
                            </div>
                            <div class="col-lg-6 '.$text_align_class.'">
                                <div class="products-cont '.$text_class.'">
                                    <h2 class="mb-20">'.$title.'</h2>
                                    <p class="mb-40">'.$description.'</p>
                                    <a class="main-btn" href="'.$button_url.'">'.$button_text.'</a>
                                </div>
                            </div>
                        </div> <!-- row -->
                    </div> <!-- products -->';

    return $megamio_featured_box_item;
}

add_shortcode('featured_box_item', 'megamio_featured_box_item');