<?php

// [slider]

function megamio_slider($params = array(), $content = null) {
    extract(shortcode_atts(array(
        'full_height' 				=> 'yes',
        'custom_height'	 			=> '',
        'hide_arrows'				=> '',
        'hide_bullets'				=> '',
        'color_navigation_bullets' 	=> '#000;',
        'color_navigation_arrows'	=> '#000;',
        'custom_autoplay_speed'		=> 10
    ), $params));

    $megamio_slider = '<section class="slider-area"><div class="slider-active">
		              '.do_shortcode($content).'</div> <!-- slider active --></section>';

    return $megamio_slider;
}

add_shortcode('slider', 'megamio_slider');

function megamio_image_slide($params = array(), $content = null) {
    extract(shortcode_atts(array(
        'title' 					=> '',
        'title_font_size'			=> '64px',
        'title_line_height'			=> '',
        'title_font_family'			=> 'primary_font',
        'description' 				=> '',
        'description_font_size' 	=> '21px',
        'description_line_height'	=> '',
        'description_font_family'	=> 'primary_font',
        'text_color'				=> '#000000',
        'button_text' 				=> '',
        'button_url'				=> '',
        'link_whole_slide'			=> '',
        'button_color'				=> '#000000',
        'button_text_color'			=>'#FFFFFF',
        'bg_color'					=> '#CCCCCC',
        'bg_image'					=> '',
        'text_align'				=> 'left',
        'active'                    => ''

    ), $params));

    if (is_numeric($bg_image))
    {
        $bg_image = wp_get_attachment_url($bg_image);
    } else {
        $bg_image = "";
    }


    $megamio_image_slide = '<div class="single-slider bg_cover d-flex align-items-center" style="background-image: url('.$bg_image.')">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-8">
                            <div class="slider-cont">
                                <h4 data-animation="fadeInUp" data-delay="0.5s">Spring - Summer 2019</h4>
                                <h2 class="mb-20" data-animation="fadeInUp" data-delay="1s"><span>Sale</span> 35% Off</h2>
                                <a class="main-btn" href="#" data-animation="fadeInUp" data-delay="1.5s">shop now</a>
                            </div> <!-- slider cont -->
                        </div>
                    </div> <!-- row -->
                </div> <!-- container -->
            </div> <!-- single slider -->';

    return $megamio_image_slide;
}

add_shortcode('image_slide', 'megamio_image_slide');