<?php

// [banner]

function megamio_banner($params = array(), $content = null) {
    extract(shortcode_atts(array(
        'full_height' 				=> 'yes',
        'custom_height'	 			=> '',
        'title' 					=> '',
        'sub_title' 				=> '',
        'button_text'               => '',
        'button_url'                => '',
        'bg_image'					=> '',
        'bg_color'					=> '',
        'text_align'				=> 'left'
    ), $params));

    if (is_numeric($bg_image))
    {
        $bg_image = wp_get_attachment_url($bg_image);
    } else {
        $bg_image = "";
    }


    if($text_align == 'left'){
        $text_content_align = '';
    }else if($text_align == 'right'){
        $text_content_align = 'justify-content-end';
    }

    $megamio_banner = '<section class="banner_static-area">
        <div class="container">
            <div class="banner_static-3 bg_cover pt-100 pb-100" style="background-image: url('.$bg_image.')">
                <div class="row '.$text_content_align.'">
                    <div class="col-xl-5 col-lg-6">
                        <div class="banner_static-content-3 text-center">
                            <h2 class="banner-title-3 mb-25">'.$title.'</h2>
                            <p class="mb-30">'.$sub_title.'</p>
                            <a href="'.$button_url.'" class="main-btn">'.$button_text.'</a>
                        </div> <!-- banner_static content 3 -->
                    </div>
                </div>
            </div> <!-- banner_static -->
        </div> <!-- container -->
    </section>';

    return $megamio_banner;
}

add_shortcode('banner', 'megamio_banner');