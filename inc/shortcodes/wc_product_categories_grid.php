<?php

if (class_exists('WooCommerce')) {

    function megamio_product_categories( $atts ) {

        extract( shortcode_atts( array(
            'product_categories_selection'	=> 'auto',
            'ids'							=> '',
            'number'     					=> 12,
            'order'      					=> 'asc',
            'hide_empty'				 	=> 0,
            'show_count'					=> 0,
            'parent'     					=> '0'
        ), $atts ) );

        if ( isset( $atts[ 'ids' ] ) ) {
            $ids = explode( ',', $atts[ 'ids' ] );
            $ids = array_map( 'trim', $ids );
        } else {
            $ids = array();
        }

        $hide_empty = ( $hide_empty == true && $hide_empty == 1 ) ? 1 : 0;

        if ($product_categories_selection == "auto") {

            $args = array(
                'orderby'    => 'title',
                'order'      => $order,
                'hide_empty' => $hide_empty,
                'number'	 => $number,
                'pad_counts' => true,
            );

            if ($parent == 0) {
                $args['parent']= 0;
            }


        } else {

            $args = array(
                'orderby'    => 'include',
                'hide_empty' => $hide_empty,
                'include'    => $ids,
                'pad_counts' => true,
            );

            $parent = 999;

        }

        $product_categories = get_terms( 'product_cat', $args );

        if ( $parent == "0" ) {
            $product_categories = wp_list_filter( $product_categories, array( 'parent' => $parent ) );
        }

        if ( $hide_empty ) {
            foreach ( $product_categories as $key => $category ) {
                if ( $category->count == 0 ) {
                    unset( $product_categories[ $key ] );
                }
            }
        }

        ob_start();

        $cat_counter = 0;

        $cat_number = count($product_categories);

        if ( $product_categories ) {

            foreach ( $product_categories as $category ) {

                $thumbnail_id = get_term_meta( $category->term_id, 'thumbnail_id', true );
                $image = wp_get_attachment_url( $thumbnail_id );
                $cat_counter++;

                switch ($cat_number) {
                    case 1:
                        $cat_class = "one_cat_" . $cat_counter;
                        break;
                    case 2:
                        $cat_class = "two_cat_" . $cat_counter;
                        break;
                    case 3:
                        $cat_class = "three_cat_" . $cat_counter;
                        break;
                    case 4:
                        $cat_class = "four_cat_" . $cat_counter;
                        break;
                    case 5:
                        $cat_class = "five_cat_" . $cat_counter;
                        break;
                    default:
                        $cat_class = "more_than_6";
                }
                ?>

                <div class="category_grid category_<?php echo $cat_class; ?>">
                    <div class="single-collection-bag mt-10">
                        <img src="<?php echo esc_url($image); ?>" alt="collection">
                        <div class="bag-content">
                           <h4><?php echo esc_html($category->name); ?></h4>
                           <a href="<?php echo get_term_link( $category->slug, 'product_cat' ); ?>">Shop Now</a>
                        </div>
                    </div>
                </div>

                <?php

            }

        }

        woocommerce_reset_loop();

        return '<section class="collection-area">
        <div class="container-fluid">
            <div class="row justify-content-center">
               <div class="container">
                 <div class="row">' . ob_get_clean() . '</div> <!-- row -->
               </div>
            </div>
        </div> <!-- container -->
        </section>';
    }

    add_shortcode("product_categories_grid", "megamio_product_categories");

}