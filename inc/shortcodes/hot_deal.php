<?php

// [Hot Deal]

function megamio_hot_deal($params = array(), $content = null) {
    extract(shortcode_atts(array(
        'full_height' 				=> 'yes',
        'custom_height'	 			=> '',
        'title' 					=> '',
        'sub_title' 				=> '',
        'price' 				    => '',
        'button_text'               => '',
        'button_url'               => '',
        'bg_image'					=> '',
        'text_align'				=> 'left'
    ), $params));

    if (is_numeric($bg_image))
    {
        $bg_image = wp_get_attachment_url($bg_image);
    } else {
        $bg_image = "";
    }


    $megamio_hot_deal = '<section class="hot_deal bg_cover pt-95 pb-100" style="background-image: url('.$bg_image.')">
        <div class="container">
            <div class="row ">
                <div class="col-lg-6 col-md-10">
                    <div class="hot_content">
                        <h5 class="hot_content-title">'.$sub_title.'</h5>
                        <h3 class="hot-title mt-30">'.$title.'</h3>
                        <span class="hot-price">'.$price.'</span>
                        <ul class="hot-rating mt-15">
                            <li><i class="fa fa-star"></i></li>
                            <li><i class="fa fa-star"></i></li>
                            <li><i class="fa fa-star"></i></li>
                            <li><i class="fa fa-star"></i></li>
                            <li><i class="fa fa-star"></i></li>
                        </ul>
                        <a href="'.$button_url.'" class="main-btn mt-50">'.$button_text.'</a>
                    </div> <!-- hot content -->
                </div>
            </div> <!-- row -->
        </div> <!-- container -->
    </section>';

    return $megamio_hot_deal;
}

add_shortcode('hot_deal', 'megamio_hot_deal');