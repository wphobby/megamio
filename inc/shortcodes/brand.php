<?php

// [brand]

function megamio_brand($params = array(), $content = null) {
    extract(shortcode_atts(array(
        'title' 					=> ''
    ), $params));

    $megamio_brand = '<section class="brand-area pt-100 pb-100">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-2 offset-lg-1">
                    <div class="brand-title mt-25 text-center text-lg-left">
                        <h3 class="title">'.$title.'</h3>
                    </div>
                </div>
                <div class="col-xl-8 offset-xl-1 col-lg-9">
                    <div class="brand-logo">
                        <div class="row justify-content-center">
                        '.do_shortcode($content).'
                        </div> <!-- row -->
                    </div> <!-- brand logo -->
                </div>
            </div> <!-- row -->
        </div> <!-- container -->
    </section>';

    return $megamio_brand;
}

add_shortcode('brand', 'megamio_brand');

function megamio_brand_inner($params = array(), $content = null) {
    extract(shortcode_atts(array(
        'bg_image'					=> '',
    ), $params));

    if (is_numeric($bg_image))
    {
        $bg_image = wp_get_attachment_url($bg_image);
    } else {
        $bg_image = "";
    }


    $megamio_brand_inner = '<div class="col-md-3 col-sm-4 col-6">
                                <div class="single-brand mt-30">
                                    <img src="'.$bg_image.'" alt="Brand">
                                </div> <!-- single brand -->
                            </div>';

    return $megamio_brand_inner;
}

add_shortcode('brand_inner', 'megamio_brand_inner');