<?php

// [blog_posts]

function megamio_blog_posts($params = array(), $content = null) {
    extract(shortcode_atts(array(
        'full_height' 				=> 'yes',
        'custom_height'	 			=> '',
        'title' 					=> 'Latest News',
        'blog_text'                 => 'Our Blog',
        'blog_url'                 => '#',
        "category"                  => '',
    ), $params));
    ob_start();
    ?>

    <section class="latest-blog-area pt-90">
    <div class="container">
    <div class="row">
        <div class="col-lg-12">
            <div class="section-title text-left">
                <h3 class="custom-heading">
                    <?php echo $title; ?>
                    <a href="<?php echo $blog_url; ?>" class="heading-link"><?php echo $blog_text; ?></a>
                </h3>
            </div> <!-- section title -->
        </div>
    </div> <!-- row -->
    <div class="row justify-content-center">

    <?php


    $args = array(
                'post_status' => 'publish',
                'post_type' => 'post',
                'category_name' => $category
            );

    $recentPosts = new WP_Query( $args );

            if ( $recentPosts->have_posts() ) : ?>

                <?php while ( $recentPosts->have_posts() ) : $recentPosts->the_post(); ?>

                   <div class="col-lg-4 col-md-6">
                        <div class="single-blog text-center mt-50">
                            <div class="blog-image">
                                <?php
                                if ( has_post_thumbnail()) {
                                    the_post_thumbnail('megamio-blog-list-medium');
                                }
                                ?>                            </div>
                            <div class="blog-content pt-20">
                                <h4 class="blog-title mb-30"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
                                <a href="<?php the_permalink(); ?>">Read More</a>
                            </div>
                        </div> <!-- single blog -->
                    </div>

                <?php endwhile; // end of the loop. ?>

                <?php

            endif;
            ?>
    </div> <!-- row -->
    </div> <!-- container -->
    </section>

    <?php
	wp_reset_query();
	$content = ob_get_contents();
	ob_end_clean();
	return $content;
}

add_shortcode('blog_posts', 'megamio_blog_posts');