<?php

if (class_exists('WooCommerce')) {

    function megamio_trending_items( $atts ) {

        extract( shortcode_atts( array(
            'posts'       => '',
            'category'    => '',
            'order'       => 'asc'
        ), $atts ) );

        $args = array(
            'post_type'      => 'product',
            'posts_per_page' => $posts,
            'product_cat'    => $category
        );

        $trending_items = new WP_Query( $args );

        $shop_url = get_permalink( wc_get_page_id( 'shop' ) );

        ob_start();

        if ( $trending_items->have_posts() ) : ?>

            <?php while ( $trending_items->have_posts() ) : $trending_items->the_post(); ?>
                <?php
                $product = wc_get_product( get_the_ID() );
                $image = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), array(220,220) );
                ?>
                <div class="col-lg-3">
                    <div class="products-card-one">
                        <div class="card-image">
                            <img src="<?php echo $image[0]; ?>" alt="Card">
                            <div class="products-icon">
                                <ul>
                                    <li>
                                        <?php do_action( 'woocommerce_after_shop_loop_item_title' ); ?>
                                    </li>
                                    <li>
                                        <?php do_action( 'woocommerce_after_shop_loop_item' ); ?>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="card-cont pt-10">
                            <h6 class="products-title"><a href="<?php the_permalink(); ?>"><?php echo $product->get_name(); ?></a></h6>
                            <span class="price">$<?php echo $product->get_sale_price(); ?></span>
                        </div>
                    </div> <!-- products card one -->
                </div>
            <?php endwhile; // end of the loop. ?>

            <?php

             endif;
        ?>
        <?php

        wp_reset_query();

        return '<section class="trending_items pt-95 pb-95">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="section-title text-center pb-80">
                        <h3>Trending Items</h3>
                    </div> <!-- section title -->
                </div>
            </div> <!-- row -->
            <div class="row justify-content-center">
                <div class="col-xl-10 col-md-11">
                    <div class="row trending-active">' . ob_get_clean() . '</div> <!-- row -->
                </div>
            </div> <!-- row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="trending_more pt-60 text-center">
                        <a href="'.$shop_url.'">Browse All Products</a>
                    </div> <!-- trending more -->
                </div>
            </div> <!-- row -->
        </div> <!-- container -->
    </section>';
    }

    add_shortcode("trending_items", "megamio_trending_items");

}