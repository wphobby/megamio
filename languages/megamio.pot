# Copyright (C) 2019 megamio
# This file is distributed under the same license as the megamio package.
msgid ""
msgstr ""
"Project-Id-Version: megamio\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language-Team: support@wphobby.com\n"
"Report-Msgid-Bugs-To: support@wphobby.com\n"
"X-Poedit-Basepath: ..\n"
"X-Poedit-KeywordsList: __;_e;_ex:1,2c;_n:1,2;_n_noop:1,2;_nx:1,2,4c;_nx_noop:1,2,3c;_x:1,2c;esc_attr__;esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c\n"
"X-Poedit-SearchPath-0: .\n"
"X-Poedit-SearchPathExcluded-0: *.js\n"
"X-Poedit-SourceCharset: UTF-8\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: comments.php:44
msgid "Comments are closed."
msgstr ""

#: search.php:18
msgid "Search Results for: %s"
msgstr ""

#: inc/widgets/widget-featured-post-grid.php:21
msgid "Add Widget to Display Featured Post."
msgstr ""

#: inc/widgets/widget-featured-post-grid.php:23
msgid "HS: Featured Grid"
msgstr ""

#: inc/widgets/widget-featured-post-grid.php:35
msgid "Post Category:"
msgstr ""

#: inc/widgets/widget-featured-post-grid.php:42
msgid "From Recent Post"
msgstr ""

#: inc/widgets/widget-featured-post-main.php:35
msgid "Post Category:"
msgstr ""

#: inc/widgets/widget-featured-post-main.php:21
msgid "Add Widget to Display Featured Main Post."
msgstr ""

#: inc/widgets/widget-featured-post-main.php:23
msgid "HS: Featured Main Post"
msgstr ""

#: inc/widgets/widget-featured-post-main.php:42
msgid "From Recent Post"
msgstr ""

#: inc/widgets/widget-featured-post-news.php:35
msgid "Post Category:"
msgstr ""

#: inc/widgets/widget-featured-post-news.php:21
msgid "Add Widget to Display Featured Post."
msgstr ""

#: inc/widgets/widget-featured-post-news.php:23
msgid "HS: Featured News"
msgstr ""

#: inc/widgets/widget-featured-post-news.php:42
msgid "From Recent Post"
msgstr ""

#: inc/widgets/widget-featured-post-prime.php:35
msgid "Post Category:"
msgstr ""

#: inc/widgets/widget-featured-post-prime.php:21
msgid "Add Widget to Display Featured Main Post."
msgstr ""

#: inc/widgets/widget-featured-post-prime.php:23
msgid "HS: Featured Prime Post"
msgstr ""

#: inc/widgets/widget-featured-post-prime.php:42
msgid "From Recent Post"
msgstr ""

#: inc/widgets/widget-latest-post.php:21
msgid "Add Widget to Display Latest Post."
msgstr ""

#: inc/widgets/widget-latest-post.php:23
msgid "HS: Latest Post"
msgstr ""

#: inc/widgets/widget-latest-post.php:35
msgid "Post Category:"
msgstr ""

#: inc/widgets/widget-latest-post.php:42
msgid "From Recent Post"
msgstr ""

#: inc/widgets/widget-masonry-post.php:21
msgid "Add Widget to Display Masonry Post."
msgstr ""

#: inc/widgets/widget-masonry-post.php:23
msgid "HS: Masonry Post"
msgstr ""

#: inc/widgets/widget-masonry-post.php:35
msgid "Post Category:"
msgstr ""

#: inc/widgets/widget-masonry-post.php:42
msgid "From Recent Post"
msgstr ""

#: inc/widgets/widget-popular-post.php:21
msgid "Add Widget to Display Popular Post."
msgstr ""

#: inc/widgets/widget-popular-post.php:23
msgid "HS: Popular Post"
msgstr ""

#: inc/widgets/widget-popular-post.php:35
msgid "Post Category:"
msgstr ""

#: inc/widgets/widget-popular-post.php:42
msgid "From Recent Post"
msgstr ""

#: inc/widgets/widget-trending-post.php:21
msgid "Add Widget to Display Trending Post."
msgstr ""

#: inc/widgets/widget-trending-post.php:23
msgid "HS: Trending Post"
msgstr ""

#: inc/widgets/widget-trending-post.php:35
msgid "Post Category:"
msgstr ""

#: inc/widgets/widget-trending-post.php:42
msgid "From Recent Post"
msgstr ""

#: template-parts/content-none.php:14
msgid "Nothing Found"
msgstr ""

#: template-parts/content-none.php:14
msgid "Sorry, but nothing matched your search terms. Please try again with some different keywords."
msgstr ""

#: template-parts/content-none.php:14
msgid "It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps searching can help."
msgstr ""

#: template-parts/content-page.php:15
msgid "Pages:"
msgstr ""

#: template-parts/content.php:19
msgid " - in"
msgstr ""



#: template-parts/pagination/pagination.php:17
msgid "Posts Navigation"
msgstr ""

#: template-parts/pagination/pagination.php:27
msgid "Page"
msgstr ""

#: template-parts/pagination/pagination.php:28
msgid "Next"
msgstr ""

#: template-parts/pagination/pagination.php:29
msgid "Previous"
msgstr ""

#: template-parts/post/layout-5.php:19
msgid " - in"
msgstr ""


#: inc/customizer/options-footer.php:62
msgid "White"
msgstr ""

#: inc/customizer/options-footer.php:63
msgid "Gray"
msgstr ""

#: inc/customizer/options-post-slider.php:29
msgid "Latest Post"
msgstr ""

#: inc/customizer/options-post-slider.php:30
msgid "Category"
msgstr ""

#: inc/customizer/options-typography.php:13
msgid "Body"
msgstr ""

#: inc/customizer/options-typography.php:45
msgid "Heading"
msgstr ""

#: inc/customizer/options-breadcrumb.php:13
msgid "Enable Breadcrumb"
msgstr ""

#: inc/customizer/options-color.php:13
msgid "Primary Color"
msgstr ""

#: inc/customizer/options-footer.php:13
msgid "Enable Scroll To Top"
msgstr ""

#: inc/customizer/options-footer.php:24
msgid "Contact Phone Number"
msgstr ""

#: inc/customizer/options-footer.php:35
msgid "Contact Email"
msgstr ""

#: inc/customizer/options-footer.php:46
msgid "Contact Address"
msgstr ""

#: inc/customizer/options-footer.php:58
msgid "Category Select"
msgstr ""

#: inc/customizer/options-header.php:13
msgid "Header Layout"
msgstr ""

#: inc/customizer/options-layout.php:13
msgid "Body Layout"
msgstr ""

#: inc/customizer/options-loader.php:13
msgid "Enable Preloader"
msgstr ""

#: inc/customizer/options-post-slider.php:13
msgid "Enable post slider"
msgstr ""

#: inc/customizer/options-post-slider.php:25
msgid "Category Select"
msgstr ""

#: inc/customizer/options-post-slider.php:52
msgid "Categories"
msgstr ""

#: inc/customizer/options-single.php:13
msgid "Single Post Layout"
msgstr ""

#: inc/customizer/options-single.php:28
msgid "Enable Author Box"
msgstr ""

#: inc/customizer/options-social.php:11
msgid "Add Social Profile"
msgstr ""

#: inc/customizer/options-social.php:12
msgid "Drag & Drop items to re-arrange the order"
msgstr ""

#: inc/customizer/options-social.php:17
msgid "Social Profile"
msgstr ""

#: inc/customizer/options-social.php:22
msgid "Social Icon"
msgstr ""

#: inc/customizer/options-social.php:26
msgid "Google Plus"
msgstr ""

#: inc/customizer/options-social.php:27
msgid "Pinterest"
msgstr ""

#: inc/customizer/options-social.php:28
msgid "Twitter"
msgstr ""

#: inc/customizer/options-social.php:29
msgid "Friends"
msgstr ""

#: inc/customizer/options-social.php:30
msgid "Paper Plane"
msgstr ""

#: inc/customizer/options-social.php:31
msgid "Streaming"
msgstr ""

#: inc/customizer/options-social.php:36
msgid "Social Link URL"
msgstr ""

#: inc/customizer/kirki-customizer.php:32
msgid "Theme Options"
msgstr ""

#: inc/customizer/kirki-customizer.php:40
msgid "Header"
msgstr ""

#: inc/customizer/kirki-customizer.php:49
msgid "Layout"
msgstr ""

#: inc/customizer/kirki-customizer.php:59
msgid "Footer"
msgstr ""

#: inc/customizer/kirki-customizer.php:68
msgid "Loader"
msgstr ""

#: inc/customizer/kirki-customizer.php:77
msgid "Post Slider"
msgstr ""

#: inc/customizer/kirki-customizer.php:86
msgid "Breadcrumb"
msgstr ""

#: inc/customizer/kirki-customizer.php:95
msgid "Single Post"
msgstr ""

#: inc/customizer/kirki-customizer.php:104
msgid "Social"
msgstr ""

#: inc/customizer/kirki-customizer.php:113
msgid "Color"
msgstr ""

#: inc/customizer/kirki-customizer.php:122
msgid "Typography"
msgstr ""

#: inc/functions/function-setup.php:90
msgid "Sidebar"
msgstr ""

#: inc/functions/function-setup.php:92
msgid "Add widgets here."
msgstr ""

#: inc/functions/function-setup.php:101
msgid "Featured Post News"
msgstr ""

#: inc/functions/function-setup.php:103
msgid "News Widget Below Header Section."
msgstr ""

#: inc/functions/function-setup.php:112
msgid "Featured Post Grid"
msgstr ""

#: inc/functions/function-setup.php:114
msgid "Grid Widget Below Header Section."
msgstr ""

#: inc/functions/function-setup.php:123
msgid "Popular Latest Post News"
msgstr ""

#: inc/functions/function-setup.php:125
msgid "News Widget Below Post News."
msgstr ""

#: inc/functions/function-setup.php:134
msgid "Popular Latest Post Grid"
msgstr ""

#: inc/functions/function-setup.php:136
msgid "News Widget Below Post News."
msgstr ""

#: inc/functions/function-setup.php:145
msgid "Masonry Post"
msgstr ""

#: inc/functions/function-setup.php:147
msgid "'Masonry Post."
msgstr ""

#: inc/functions/function-setup.php:156
msgid "Footer 1"
msgstr ""

#: inc/functions/function-setup.php:158
msgid "Add widgets here to show on footer 1."
msgstr ""

#: inc/functions/function-setup.php:167
msgid "Footer 2"
msgstr ""

#: inc/functions/function-setup.php:169
msgid "Add widgets here to show on footer 2."
msgstr ""

#: inc/functions/function-setup.php:178
msgid "Footer 3"
msgstr ""

#: inc/functions/function-setup.php:180
msgid "Add widgets here to show on footer 3."
msgstr ""

#: inc/functions/function-setup.php:189
msgid "Footer 4"
msgstr ""

#: inc/functions/function-setup.php:191
msgid "Add widgets here to show on footer 4."
msgstr ""


