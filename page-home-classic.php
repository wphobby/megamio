<?php
/**
 * Template Name: Homepage Classic
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package megamio
 */
get_header();
?>

    <section class="latest-posts pb-60">
        <div class="container">
            <div class="row">
                <div class="col-lg-8">
                    <?php
                    if ( is_active_sidebar( 'popular-latest-post-grid') ) {
                        dynamic_sidebar('popular-latest-post-grid');
                    }
                    ?>
                </div>
                <?php get_sidebar(); ?>
            </div> <!-- row -->
        </div> <!-- container -->
    </section>
<?php
get_footer();
?>