<?php
/**
 * The sidebar containing the main widget area
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package megamio
 */
global $megamio_options;

if ( ! is_active_sidebar( 'sidebar-1' ) || 'layout-3' === $megamio_options['layout_blog'] || 'layout-4' === $megamio_options['layout_blog'] || 'layout-5' === $megamio_options['layout_blog'] ) {
return;
}
?>

<div class="col-lg-4">
    <div class="right-sidebar pt-10">
            <?php dynamic_sidebar( 'sidebar-1' ); ?>
    </div> <!-- right sidebar -->
</div>

