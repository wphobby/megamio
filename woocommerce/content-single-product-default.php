<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

global $post, $product, $megamio_options;

$layout_class = 'single-product-layout-' . $megamio_options['single_product_layout'];

$product_name = $product->get_title();

/*
 * Remove when publish
 */
switch ( $product_name )
{
	case "Cotton Shirt":
		$layout_class = 'single-product-layout-1';
		set_theme_mod('single_product_layout', 1);
		break;
	case "Women Cotton Shirt":
		$layout_class = 'single-product-layout-2';
		set_theme_mod('single_product_layout', 2);
		break;
	case "Hooded Sweat Shirt":
		$layout_class = 'single-product-layout-3';
		set_theme_mod('single_product_layout', 3);
		break;
	case "Long-sleeved Blouse":
		$layout_class = 'single-product-layout-4';
		set_theme_mod('single_product_layout', 4);
		break;
}




//woocommerce_single_product_summary
add_action( 'woocommerce_single_product_summary_single_title', 'woocommerce_template_single_title', 5 );
add_action( 'woocommerce_single_product_summary_single_rating', 'woocommerce_template_single_rating', 10 );
add_action( 'woocommerce_single_product_summary_single_price', 'woocommerce_template_single_price', 10 );
add_action( 'woocommerce_single_product_summary_single_excerpt', 'woocommerce_template_single_excerpt', 20 );
add_action( 'woocommerce_single_product_summary_single_add_to_cart', 'woocommerce_template_single_add_to_cart', 30 );
add_action( 'woocommerce_single_product_summary_single_meta', 'woocommerce_template_single_meta', 40 );
add_action( 'woocommerce_single_product_summary_single_sharing', 'woocommerce_template_single_sharing', 50 );
add_action( 'woocommerce_after_single_product_summary_related_products', 'megamio_output_related', 20 );

?>
<div  id="product-<?php the_ID(); ?>" <?php function_exists('wc_product_class')? wc_product_class() : post_class(); ?>>
	<div class="row">
		<div class="col-lg-6 col-md-6">
			<div class="product-images-wrapper <?php echo $layout_class; ?>">
			   <?php  do_action( 'woocommerce_before_single_product_summary' ); ?>
		    </div>
		</div>
		<div class="col-lg-6 col-md-6">
			<div class="shop-details-content mt-25">
				<?php
                //product-title
				do_action( 'woocommerce_single_product_summary_single_title' );

				if ( post_password_required() ) {
					echo get_the_password_form();
					return;
				}

                //product-rating
				do_action( 'woocommerce_single_product_summary_single_rating' );

				//product-price
				do_action( 'woocommerce_single_product_summary_single_price' );

				//product-description
				do_action( 'woocommerce_single_product_summary_single_excerpt' );


				//add-to-cart
				do_action( 'woocommerce_single_product_summary_single_add_to_cart' );

				//product-meta
				do_action( 'woocommerce_single_product_summary_single_meta' );

                //social-sharing
				do_action( 'woocommerce_single_product_summary_single_sharing' );

				?>

			</div> <!-- shop details content -->
		</div>
	</div> <!-- row -->

	<div class="row">
		<?php
		//woocommerce_output_product_data_tabs();
		?>
	</div>

</div><!-- #product-<?php the_ID(); ?> -->


<?php do_action( 'woocommerce_after_single_product_summary_related_products' ); ?>
