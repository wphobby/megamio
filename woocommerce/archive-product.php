<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive.
 *
 * Override this template by copying it to yourtheme/woocommerce/archive-product.php
 *
 * @author      WooThemes
 * @package     WooCommerce/Templates
 * @version     3.4.0
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

get_header('shop'); 

global $megamio_options;

add_action( 'woocommerce_before_shop_loop_result_count', 'woocommerce_result_count', 20 );
add_action( 'woocommerce_before_shop_loop_catalog_ordering', 'woocommerce_catalog_ordering', 30 );
add_action( 'woocommerce_after_shop_loop', 'woocommerce_pagination' );



/*
 * Remove when publish
 */
if(isset($_GET['shop_sidebar']) && $_GET['shop_sidebar'] == 'true'){
	$megamio_options['display_shop_sidebar'] = true;
}

$display_shop_sidebar = false;
if ( is_active_sidebar( 'catalog-widget-area' ) && $megamio_options['display_shop_sidebar'] ) {
	$display_shop_sidebar = true;
}

?>
   	<section class="shop-page">
	   <div class="container">
		   <?php if ( $display_shop_sidebar ) : ?>
			   <div class="row">
				   <div class="shop-sidebar col-lg-3 col-md-4 order-last order-md-first">
					   <?php do_action('woocommerce_sidebar'); ?>
				   </div>
				   <div class="col-lg-9 col-md-8 order-first order-md-last">
					   <div class="shop-page-product">
						   <?php
						   $animateCounter = 0;
						   woocommerce_product_loop_start();

						   if ( wc_get_loop_prop( 'total' ) ) {
							   while ( have_posts() ) {
								   the_post();
								   $animateCounter++;
								   do_action( 'woocommerce_shop_loop' );

								   wc_get_template_part( 'content', 'product-large' );
							   }
						   }

						   woocommerce_product_loop_end();
						   ?>
					   </div> <!-- shop page product -->
					   <div class="woocommerce-after-shop-loop-wrapper">
						   <?php do_action( 'woocommerce_after_shop_loop' ); ?>
					   </div>
				   </div>
			   </div> <!-- row -->
		   <?php else : ?>
			   <div class="row">
				   <div class="col-lg-12">
					   <div class="accordion" id="accordionExample">
						   <div class="card">
							   <div class="row">
								   <div class="col-lg-8 d-none d-lg-block">
									   <div class="shop-price-range mt-30">
										   <?php do_action( 'woocommerce_before_shop_loop_result_count' ); ?>
									   </div> <!-- shop price range -->
								   </div>
								   <div class="col-lg-3 col-sm-6 col-8">
									   <div class="mt-30">
										   <?php do_action( 'woocommerce_before_shop_loop_catalog_ordering' ); ?>
									   </div> <!--catalog-ordering-->
								   </div>
								   <div class="col-lg-1 col-sm-6 col-4">
									   <div class="shop-filter mt-30 text-right" id="headingTwo">
										   <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">Filter</a>
									   </div>
								   </div>
							   </div>  <!-- row -->
							   <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
								   <div class="card-body shop-topbar">
									   <div class="row">
										   <?php
										   $sidebar = 'catalog-filter';
										   if ( is_active_sidebar( $sidebar ) ) {
											   dynamic_sidebar( $sidebar );
										   }
										   ?>
									   </div> <!-- row -->
								   </div> <!-- card-body -->
							   </div>
						   </div> <!-- card -->
					   </div> <!-- accordion -->
				   </div>
			   </div> <!-- row -->
			   <div class="shop-page-product">
					   <?php
					   $animateCounter = 0;
					   woocommerce_product_loop_start();

					   if ( wc_get_loop_prop( 'total' ) ) {
						   while ( have_posts() ) {
							   the_post();
							   $animateCounter++;
							   do_action( 'woocommerce_shop_loop' );

							   wc_get_template_part( 'content', 'product-full-width' );
						   }
					   }

					   woocommerce_product_loop_end();
					   ?>
			   </div> <!-- shop-page-product -->
			   <div class="woocommerce-after-shop-loop-wrapper">
				   <?php do_action( 'woocommerce_after_shop_loop' ); ?>
			   </div>		   <?php endif; ?>
	   </div> <!-- container -->
	</section>

<?php get_footer('shop'); ?>