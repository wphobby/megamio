<?php
/**
 * The template for displaying product content within loops
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;

global $product;

// Ensure visibility.
if ( empty( $product ) || ! $product->is_visible() ) {
	return;
}

//woocommerce_after_shop_loop_item_title
remove_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_price', 10 );

add_action( 'woocommerce_after_shop_loop_item_title_loop_price', 'woocommerce_template_loop_price', 10 );

$attachment_ids = $product->get_gallery_image_ids();
if ( $attachment_ids ) {
	$loop = 0;
	foreach ( $attachment_ids as $attachment_id ) {
		$image_link = wp_get_attachment_url( $attachment_id );
		if (!$image_link) continue;
		$loop++;
		$product_thumbnail_second = wp_get_attachment_image_src($attachment_id, 'shop_catalog');
		if ($loop == 1) break;
	}
}

$style = '';
$class = '';
if (isset($product_thumbnail_second[0])) {
	$style = 'background-image:url(' . $product_thumbnail_second[0] . ')';
	$class = 'with_second_image';
}


?>
<div class="<?php megamio_loop_columns_class(); ?>">
	<div class="products-card-one mt-40">
		<div class="card-image product_thumbnail">
			<a href="<?php the_permalink(); ?>">
				<span class="product_thumbnail_background" style="<?php echo $style; ?>"></span>
			    <?php
			    if ( has_post_thumbnail( $product->get_id() ) ) {
				   echo get_the_post_thumbnail( $product->get_id(), 'shop_catalog');
			    } else {
				   echo apply_filters( 'woocommerce_single_product_image_html', sprintf( '<img src="%s" alt="Placeholder" />', wc_placeholder_img_src() ), $product->get_id() );
			    }
			?>
			</a>
			<div class="products-icon">
				<ul>
					<li>
						<?php do_action( 'woocommerce_after_shop_loop_item_title' ); ?>
					</li>
					<li>
						<?php do_action( 'woocommerce_after_shop_loop_item' ); ?>
					</li>
				</ul>
			</div>
		</div>
		<div class="card-cont pt-10">
			<h6 class="products-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h6>
			<?php do_action( 'woocommerce_after_shop_loop_item_title_loop_price' ); ?>
		</div>
	</div> <!-- products card one -->
</div>

