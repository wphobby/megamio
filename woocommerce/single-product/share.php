<?php
/**
 * Single Product Share
 *
 * Sharing plugins can hook into here or you can add your own code directly.
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/share.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.5.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

global $product, $megamio_options;
$image_id   = $product->get_image_id();
$image_link = '';
if ( $image_id ) {
	$image_link = wp_get_attachment_url( $image_id );
}
$title = $product->get_title();
$link = $product->get_permalink();
$media = $image_link;


	$socials      = $megamio_options['single_product_socials'];
	$socials_html = '';


	if ( $socials ) {
		if ( in_array( 'facebook', $socials ) ) {
			$socials_html .= sprintf(
				'<li><a class="flaticon-facebook-letter-logo" title="%s" href="http://www.facebook.com/sharer.php?u=%s&t=%s" target="_blank"><i class="ion-social-facebook"></i></a></li>',
				esc_attr( $title ),
				urlencode( $link ),
				urlencode( $title )
			);
		}

		if ( in_array( 'twitter', $socials ) ) {
			$socials_html .= sprintf(
				'<li><a class="flaticon-twitter" href="http://twitter.com/share?text=%s&url=%s" title="%s" target="_blank"><i class="ion-social-twitter"></i></a></li>',
				urlencode( $title ),
				urlencode( $link ),
				esc_attr( $title )
			);
		}

		if ( in_array( 'pinterest', $socials ) ) {
			$socials_html .= sprintf(
				'<li><a class="flaticon-pinterest-logo" href="http://pinterest.com/pin/create/button?media=%s&url=%s&description=%s" title="%s" target="_blank"><i class="ion-social-pinterest"></i></a></li>',
				urlencode( $media ),
				urlencode( $link ),
				esc_attr( $title ),
				urlencode( $title )
			);
		}

		if ( in_array( 'google', $socials ) ) {
			$socials_html .= sprintf(
				'<li><a href="https://plus.google.com/share?url=%s&text=%s" title="%s" target="_blank"><i class="flaticon-google-plus-symbol"></i></a></li>',
				urlencode( $link ),
				esc_attr( $title ),
				urlencode( $title )
			);
		}

		if ( in_array( 'linkedin', $socials ) ) {
			$socials_html .= sprintf(
				'<li><a href="http://www.linkedin.com/shareArticle?url=%s&title=%s" title="%s" target="_blank"><i class="flaticon-linkedin-logo"></i></a></li>',
				urlencode( $link ),
				esc_attr( $title ),
				urlencode( $title )
			);
		}

		if ( in_array( 'tumblr', $socials ) ) {
			$socials_html .= sprintf(
				'<li><a href="http://www.tumblr.com/share/link?url=%s" title="%s" target="_blank"><i class="flaticon-tumblr-logo"></i></a></li>',
				urlencode( $link ),
				esc_attr( $title )
			);
		}
	}

	if ( $socials_html ) {
		echo sprintf( '<ul class="megamio-social-share socials-inline">%s</ul>', $socials_html );
	}



/* Omit closing PHP tag at the end of PHP files to avoid "headers already sent" issues. */
