<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
while ( have_posts() ) : the_post();
    global $post, $product;

	add_action( 'woocommerce_before_product_quick_view_summary', 'woocommerce_show_product_images' );
	add_action( 'woocommerce_single_product_summary_single_title', 'woocommerce_template_single_title', 5 );
	add_action( 'woocommerce_single_product_summary_single_rating', 'woocommerce_template_single_rating', 10 );
	add_action( 'woocommerce_single_product_summary_single_price', 'woocommerce_template_single_price', 10 );
	add_action( 'woocommerce_single_product_summary_single_excerpt', 'woocommerce_template_single_excerpt', 20 );
	add_action( 'woocommerce_single_product_summary_single_add_to_cart', 'woocommerce_template_single_add_to_cart', 30 );
	add_action( 'woocommerce_single_product_summary_single_meta', 'woocommerce_template_single_meta', 40 );
	add_action( 'woocommerce_single_product_summary_single_sharing', 'woocommerce_template_single_sharing', 50 );

$image_title 				= esc_attr( get_the_title( get_post_thumbnail_id() ) );
$image_src 					= wp_get_attachment_image_src( get_post_thumbnail_id(), 'shop_thumbnail' );
$image_data_src				= wp_get_attachment_image_src( get_post_thumbnail_id(), 'shop_single' );
$image_data_src_original 	= wp_get_attachment_image_src( get_post_thumbnail_id(), 'full' );
$image_link  				= wp_get_attachment_url( get_post_thumbnail_id() );
$image       				= get_the_post_thumbnail( $post->ID, apply_filters( 'single_product_large_thumbnail_size', 'shop_single' ) );
$image_original				= get_the_post_thumbnail( $post->ID, 'full' );
$attachment_count   		= count( $product->get_gallery_image_ids() );
$catalog_image 				= get_the_post_thumbnail( $post->ID, 'shop_catalog');
?>
<div class="modal-content">
	<div class="modal-body">
		   <div class="row no-gutters">
			<div class="col-lg-6 col-md-5">
				<?php
				/**
				 * Hook: woocommerce_before_product_quick_view_summary
				 *
				 * @hooked woocommerce_show_product_sale_flash - 5
				 * @hooked woocommerce_show_product_images - 10
				 */
				do_action( 'woocommerce_before_product_quick_view_summary' );
				?>
			</div>
			<div class="col-lg-6 col-md-7">
						<div class="modal-products-content">
							<?php
							do_action( 'woocommerce_single_product_summary_single_title' );

							do_action( 'woocommerce_single_product_summary_single_rating' );

							do_action( 'woocommerce_single_product_summary_single_price' );

							do_action( 'woocommerce_single_product_summary_single_excerpt' );

							do_action( 'woocommerce_single_product_summary_single_add_to_cart' );

							do_action( 'woocommerce_single_product_summary_single_meta' );

							do_action( 'woocommerce_single_product_summary_single_sharing' );

							?>
						</div> <!-- modal products content -->
					</div>
		   </div> <!-- row -->
		   <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			   <i class="flaticon-close"></i>
		   </button>
	    </div> <!-- modal body -->
</div> <!-- modal content -->

<?php endwhile; // end of the loop.