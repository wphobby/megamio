<?php
/**
 * The template to display comments.
 *
 * This is the template that displays the area of the page that contains both the current comments
 * and the comment form.
 *
 *
 * @package    megamio
 * @author     wphobby
 * @link       https://wphobby.com
 */
?>
<?php if ( have_comments() ) : ?>
<div class="blog-comment pt-75">
<div class="comment-title">
        <h3 class="title">
            <?php
            printf( _n( 'One reply on "%2$s"', '%1$s replies on "%2$s"', get_comments_number(), 'megamio' ),
                number_format_i18n( get_comments_number() ), '<span>' . get_the_title() . '</span>' );
            ?>
        </h3>
    </div>
    <div class="comment-list">
        <ul>
            <?php
            /* Loop through and list the comments. Tell wp_list_comments()
             * to use megamio_comment() to format the comments.
             * If you want to override this in a child theme, then you can
             * define megamio_comment() and that will be used instead.
             */
            wp_list_comments( array( 'callback' => 'megamio_comment' ) );
            ?>
        </ul>
        <?php the_comments_navigation(); ?>
    </div> <!-- comment list -->
</div>
<?php endif; // have_comments() ?>


<?php
// If comments are closed and there are comments, let's leave a little note, shall we?
if ( ! comments_open() && get_comments_number() && post_type_supports( get_post_type(), 'comments' ) ) :
    ?>
    <p class="no-comments"><?php esc_html_e( 'Comments are closed.', 'megamio' ); ?></p>
<?php endif; ?>

<?php

$commenter 	= wp_get_current_commenter();
$req 		= get_option( 'require_name_email' );
$aria_req 	= ( $req ? " aria-required='true'" : '' );

$megamio_comment_args = array(

    'title_reply' => __( 'Leave a Comment', 'megamio' ),

    'fields' => apply_filters( 'comment_form_default_fields', array(

        'author' 	=> 	'<div class="row"><div class="col-md-6"><div class="single-comment-form">
                         <input type="text" placeholder="Name *" name="author" value=""></div> <!-- single comment form --></div>',
        'email'  	=> 	'<div class="col-md-6"><div class="single-comment-form">
                        <input type="text" placeholder="Email *" name="email" value=""></div> <!-- single comment form --></div></div> <!-- row -->',
    )),

    'comment_field' =>	'<div class="row"><div class="col-md-12"><div class="single-comment-form">
                        <textarea placeholder="Comment" name="comment"></textarea></div> <!-- single comment form --></div>',

    'comment_notes_after'  => '</div> <!-- row -->',

);

echo '<div class="blog-form-box pt-75">';
comment_form($megamio_comment_args);
echo '</div><!-- blog form box -->';
?>
