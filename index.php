<?php
/**
 * The main template file.
 *
 *
 * @package    megamio
 * @author     wphobby
 * @link       https://wphobby.com
 */

get_header();
global $megamio_options;

$page_for_posts = get_option( 'page_for_posts' );
if ($page_for_posts && 'none' != get_post_meta( get_the_ID(), 'page_title_display', true )) { ?>
    <div id="page-title-bar" class="page-title-bar">
        <div class="container">
            <div class="wrap w-100 d-flex align-items-center">
                <div class="page-title-bar-inner d-flex flex-column align-items-center w-100">
                    <div class="breadcrumb">
                        <?php if (function_exists('bcn_display')): ?>
                            <?php bcn_display(); ?>
                        <?php else: ?>
                            <?php the_title( '<h4 class="entry-title">', '</h4>' ); ?>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <header class="entry-header">
        <h4 class="entry-title">
            <?php echo get_queried_object()->post_title; ?>
        </h4>
    </header><!-- .entry-header -->
<?php } ?>
<?php
//Get layout_blog
$layout_blog = 'layout-1';

if(isset($megamio_options['layout_blog'])){
    $layout_blog = $megamio_options['layout_blog'];
}

if(isset($_GET['layout_blog'])){
    $layout_blog = $_GET['layout_blog'];
}


switch ($layout_blog)
{
    case "layout-1":
        get_template_part( 'template-parts/index/layout', '1' );
        break;
    case "layout-2":
        get_template_part( 'template-parts/index/layout', '2' );
        break;
    case "layout-3":
        get_template_part( 'template-parts/index/layout', '3' );
        break;
    case "layout-4":
        get_template_part( 'template-parts/index/layout', '4' );
        break;
    case "layout-5":
        get_template_part( 'template-parts/index/layout', '5' );
        break;
    default:
        get_template_part( 'template-parts/index/layout', '1' );
        break;
}
get_footer();
