<?php
/**
 * Template Name: Homepage News
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package megamio
 */
get_header();
?>
    <?php if ( is_active_sidebar( 'featured-post-news') ) : ?>
    <section class="slider-area">
        <div class="container">
            <div class="row">
            <?php dynamic_sidebar( 'featured-post-news' ); ?>
            </div> <!-- row -->
        </div> <!-- container -->
    </section>
    <?php endif; ?>

    <section class="latest-posts pb-60">
        <div class="container">
            <div class="row">
                <div class="col-lg-8">
                    <?php
                    if ( is_active_sidebar( 'popular-latest-post-news') ) {
                        dynamic_sidebar('popular-latest-post-news');
                    }
                    ?>
                </div>
                <?php get_sidebar(); ?>
            </div> <!-- row -->
        </div> <!-- container -->
    </section>

<?php
get_footer();
?>