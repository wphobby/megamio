<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package megamio
 */
global $megamio_options;
get_header();
if($megamio_options['enable_breadcrumbs']) {
    get_template_part('template-parts/breadcrumb/breadcrumb', 'trail');
}
?>
    <section class="blog-single-page container">
        <?php get_template_part( 'template-parts/single/layout', $megamio_options['single_post_layout'] ); ?>
    </section>
<?php get_footer();
