<?php
/**
 * Template Name: Homepage Masonry
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package megamio
 */
get_header();
get_template_part( 'template-parts/slider/post', 'slider' );
?>
    <section class="masonry-blog-area pt-30 pb-60">
        <div class="container">
            <?php
            if ( is_active_sidebar( 'masonry-post') ) {
                dynamic_sidebar('masonry-post');
            }
            ?>
        </div> <!-- container -->
    </section>
<?php
get_footer();
?>